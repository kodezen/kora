const { ipcMain } = require('electron');
let browserService = require('../../services/browser-service.js');
let ipcService = require('../../services/ipc-service.js');
let eventService = require('../../services/event-service.js');
const to = require('await-to-js').default;

let intialCheckComplete = false;

ipcMain.on('detect-chrome', async (event) => {
    let err, chromeDetected;
    [err, chromeDetected] = await to(browserService.detectChrome());
    if (err) {
        chromeDetected = false;
    } else if (!chromeDetected && !intialCheckComplete) {
        eventService.track(eventService.CHROME_NOT_INSTALLED, {});        
    }
    initialCheckComplete = true;
    ipcService.send('chrome-detected', chromeDetected);    
});
