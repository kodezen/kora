const { ipcMain } = require('electron');
var moment = require('moment');
var ipcService = require('../../services/ipc-service.js');
const userService = require('../../services/user-service.js');
const trialChecker = require('../../services/trial-checker.js');
const trialService = require('../../services/trial-service.js');
const actionService = require('../../services/action-service.js');

ipcMain.on('get-trial-time-remaining', (event) => {
    let trialTimeRemaining = userService.getTrialTimeRemaining();
    ipcService.send('trial-time-remaining', trialTimeRemaining);
});

ipcMain.on('trial-expired', (event) => {
    trialService.onTrialExpired();
    trialChecker.stopCheckerAndNotifyUser();
});

ipcMain.on('get-trial-expired-offer-time-remaining', (event) => {
    let trialExpiredOfferDetails = actionService.queryAction(actionService.INITIATED_TRIAL_EXPIRED_OFFER);
    if (trialExpiredOfferDetails) {
        offerExpirationDT = moment(trialExpiredOfferDetails.offerExpirationDT, 'MM/DD/YYYY HH:mm:ss');
        let now = moment();
        let offerTimeRemaining = moment.duration(offerExpirationDT.diff(now)).asSeconds();
        ipcService.send('trial-expired-offer-time-remaining', offerTimeRemaining);
    }
});