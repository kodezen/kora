const { ipcMain } = require('electron');
const log = require('electron-log');
var database = require('../../database/database.js');
var heroCache = require('../../database/hero-cache.js');
var heroService = require('../../services/hero-service.js');
var actionService = require('../../services/action-service.js');
var uninstallService = require('../../services/uninstall-service.js');
var ipcService = require('../../services/ipc-service.js');
var server = require('../../server/server.js');



ipcMain.on('get-heroes', (event) => {
    let heroes = heroCache.getHeroes();
    ipcService.send('heroes', heroes);
});

ipcMain.on('create-hero', (event, hero) => {
    createHero(hero);
    if (hero.blockType === 'one-time') {
        server.initiateBlockQueryAllTabs();
    }
});

ipcMain.on('update-hero', (event, hero) => {
    updateHero(hero);
});

ipcMain.on('start-hero', (event, hero) => {
    startHero(hero);
    server.initiateBlockQueryAllTabs();
});

ipcMain.on('delete-hero', (event, heroId) => {
    deleteHero(heroId);
});

function createHero(hero) {
    if (hero.blockType === 'one-time') {
        heroService.setOneTimeHeroDateTimes(hero);
    }
    hero.docType = 'hero';
    database.insert(hero, (err, newHero) => {
        if (err) {
            log.error(err);
        } else {
            heroCache.addHero(newHero);
            uninstallService.runActiveHeroCheck();
            ipcService.send('hero-created', newHero);
            if (!actionService.queryAction(actionService.FIRST_BLOCK_CREATED)) {
                actionService.registerAction(actionService.FIRST_BLOCK_CREATED);
                heroService.sendFirstBlockCreatedEvent(newHero);   
            } else {
                heroService.sendBlockCreatedEvent(newHero);   
            }
        }
    });
}

function updateHero(hero) {
    database.update({ _id: hero._id }, hero, (err, numReplaced) => {
        if (err) {
            log.error(err);
        } else {
            heroCache.deleteHero(hero);
            heroCache.addHero(hero);
            uninstallService.runActiveHeroCheck();
            ipcService.send('hero-updated', hero);
            heroService.sendBlockEditedEvent(hero);
        }
    });
}

function startHero(hero) {
    heroService.setOneTimeHeroDateTimes(hero);
    database.update({ _id: hero._id }, hero, (err, numReplaced) => {
        if (err) {
            log.error(err);
        } else {
            heroCache.deleteHero(hero);
            heroCache.addHero(hero);
            uninstallService.runActiveHeroCheck();
            ipcService.send('hero-started');
            heroService.sendBlockStartedEvent(hero);
        }
    });
}

function deleteHero(hero) {
    database.deleteOne({ _id: hero._id }, (err, numRemoved) => {
        if (err) {
            log.error(err);
        } else {
            heroCache.deleteHero(hero);
            uninstallService.runActiveHeroCheck();
            ipcService.send('hero-deleted');
            heroService.sendBlockDeletedEvent(hero);
        }
    });
}

function sendHeroes() {
    heroes = heroCache.getHeroes();
    let oneTimeHeroes = heroService.getOneTimeHeroes(heroes);
    let ongoingHeroes = heroService.getOngoingHeroes(heroes);
    sendOneTimeHeroes(oneTimeHeroes);
    sendOngoingHeroes(ongoingHeroes);
}

function sendOneTimeHeroes(heroes) {
    ipcService.send('one-time-heroes-updated', heroes);
}

function sendOngoingHeroes(heroes) {
    ipcService.send('ongoing-heroes-updated', heroes);
}

exports.sendHeroes = sendHeroes;
exports.sendOneTimeHeroes = sendOneTimeHeroes;
exports.sendOngoingHeroes = sendOngoingHeroes;