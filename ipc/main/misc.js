const { ipcMain } = require('electron');
let electronWindow = require('../../electron-win.js');

ipcMain.on('show-window', (event, code) => {
    electronWindow.show();
});