const { ipcMain } = require('electron');
var timeSavedCache = require('../../database/time-saved-cache.js');
const ipcService = require('../../services/ipc-service.js');

ipcMain.on('get-minutes-saved', (event) => {
    let timeSaved = timeSavedCache.getTimeSaved();
    ipcService.send('minutes-saved', timeSaved.minutesSaved);
});

function sendMinutesSaved(minutesSaved) {
    ipcService.send('minutes-saved', minutesSaved);
}

exports.sendMinutesSaved = sendMinutesSaved;