const { ipcMain } = require('electron');
const userService = require('../../services/user-service.js');
const ipcService = require('../../services/ipc-service.js');

ipcMain.on('get-mixpanel-id', (event) => {
    let mixpanelId = userService.getMixpanelId();
    ipcService.send('mixpanel-id', mixpanelId);
});