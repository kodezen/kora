const { ipcMain } = require('electron');
var moment = require('moment');
const eventService = require('../../services/event-service.js');
const actionService = require('../../services/action-service.js');
const userService = require('../../services/user-service.js');
var ipcService = require('../../services/ipc-service.js');

ipcMain.on('event: first-block-filled-out', (event) => {
    eventService.track(eventService.FIRST_BLOCK_FILLED_OUT, {});
    ipcService.send('event-tracked: first-block-filled-out');
});

ipcMain.on('event: extension-setup-started', (event) => {
    eventService.track(eventService.EXTENSION_SETUP_STARTED, {});
});

ipcMain.on('event: extension-setup-finished', (event) => {
    eventService.track(eventService.EXTENSION_SETUP_FINISHED, {});
    ipcService.send('event-tracked: extension-setup-finished');
});

ipcMain.on('event: setup-finished', (event) => {
    eventService.track(eventService.SETUP_FINISHED, {});
    ipcService.send('event-tracked: setup-finished');
});

ipcMain.on('event: first-block-created', (event) => {
    eventService.track(eventService.FIRST_BLOCK_CREATED, {});
});

ipcMain.on('event: upgrade-plans-viewed', (event) => {
    eventService.track(eventService.UPGRADE_PLANS_VIEWED, {});
});

ipcMain.on('event: upgrade-activated-code', (event) => {
    let trialTimeRemainingSeconds = userService.getTrialTimeRemaining();
    let trialRemainingDuration = moment.duration(trialTimeRemainingSeconds, 'seconds');
    let eventDetails = {
        trialTimeRemaining: trialRemainingDuration.format('DD:HH:mm:ss')
    }
    eventService.track(eventService.UPGRADE_ACTIVATED_CODE, eventDetails);
    ipcService.send('event-tracked: upgrade-activated-code');
});

ipcMain.on('event: trial-offer-clicked', (event) => {
    let trialTimeRemainingSeconds = userService.getTrialTimeRemaining();
    let trialRemainingDuration = moment.duration(trialTimeRemainingSeconds, 'seconds');
    let eventDetails = {
        trialTimeRemaining: trialRemainingDuration.format('DD:HH:mm:ss')
    }
    let firstTrialOfferClick = !actionService.queryAction(actionService.TRIAL_OFFER_FIRST_CLICK);
    if (firstTrialOfferClick) {
        actionService.registerAction(actionService.TRIAL_OFFER_FIRST_CLICK);
        eventDetails.firstClick = true;
    } 
    eventService.track(eventService.TRIAL_OFFER_CLICKED, eventDetails);
    ipcService.send('event-tracked: trial-offer-clicked');
});

ipcMain.on('event: trial-offer-plans-viewed', (event) => {
    eventService.track(eventService.TRIAL_OFFER_PLANS_VIEWED, {});
});

ipcMain.on('event: trial-offer-activated-code', (event) => {
    let trialTimeRemainingSeconds = userService.getTrialTimeRemaining();
    let trialRemainingDuration = moment.duration(trialTimeRemainingSeconds, 'seconds');
    let eventDetails = {
        trialTimeRemaining: trialRemainingDuration.format('DD:HH:mm:ss')
    }
    eventService.track(eventService.TRIAL_OFFER_ACTIVATED_CODE, eventDetails);
    ipcService.send('event-tracked: trial-offer-activated-code');
});

ipcMain.on('event: trial-expiring-reminder-clicked-code', (event) => {
    eventService.track(eventService.TRIAL_EXPIRING_REMINDER_CLICKED_CODE, {});
    ipcService.send('event-tracked: trial-expiring-reminder-clicked-code');
});

ipcMain.on('event: trial-expiring-reminder-clicked-dashboard', (event) => {
    eventService.track(eventService.TRIAL_EXPIRING_REMINDER_CLICKED_DASHBOARD, {});
    ipcService.send('event-tracked: trial-expiring-reminder-clicked-dashboard');
});

ipcMain.on('event: trial-expired-plans-viewed', (event) => {
    eventService.track(eventService.TRIAL_EXPIRED_PLANS_VIEWED, {});
});

ipcMain.on('event: trial-expired-activated-code', (event) => {
    eventService.track(eventService.TRIAL_EXPIRED_ACTIVATED_CODE, {});
    ipcService.send('event-tracked: trial-expired-activated-code');
});