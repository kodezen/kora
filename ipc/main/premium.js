const { ipcMain } = require('electron');
var premiumService = require('../../services/premium-service.js');

ipcMain.on('premium-activated', (event, code) => {
    premiumService.onPremiumActivated(code);
});