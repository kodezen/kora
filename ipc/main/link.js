const { ipcMain, shell } = require('electron');
const programFiles86 = process.env['ProgramFiles(x86)'];
const chromePath86 = `${programFiles86}\\Google\\Chrome\\Application\\chrome.exe`;
const childProcess = require('child_process');

ipcMain.on('install-chrome-browser', (event) => {
    shell.openExternal('https://www.google.com/chrome/');
});

ipcMain.on('install-chrome-extension', (event) => {
    childProcess.exec(`"${chromePath86}" https://chrome.google.com/webstore/detail/kora/pjmdcnieenpgbfbmchohjdgmecpjkjol`, (err, stdout, stderr) => {});
});

ipcMain.on('view-kora-plans', (event) => {
    childProcess.exec(`"${chromePath86}" https://trykora.com/plans`, (err, stdout, stderr) => {});
});