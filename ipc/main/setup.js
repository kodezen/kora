const { ipcMain } = require('electron');
const ipcService = require('../../services/ipc-service.js');

var setupCompleteSender;

ipcMain.on('setup-complete-received', (event) => {
    clearInterval(setupCompleteSender);
});

function sendSetupComplete(startupBundle) {
    let channel = 'setup-complete';
    send(channel, startupBundle);
    setupCompleteSender = setInterval(send, 500, channel, startupBundle);
}

function send(channel, data) {
    ipcService.send(channel, data);
}

exports.sendSetupComplete = sendSetupComplete;