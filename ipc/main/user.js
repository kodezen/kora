const { ipcMain } = require('electron');
const to = require('await-to-js').default;
const log = require('electron-log');
var ipcService = require('../../services/ipc-service.js');
const registerService = require('../../services/register-service.js');
const userService = require('../../services/user-service.js');
const eventService = require('../../services/event-service.js');
const trialService = require('../../services/trial-service.js');

ipcMain.on('tracking-permission-granted', async (event) => {
    let err, user;
    [ err, user ] = await to(registerService.registerUser());
    if (err) {
        log.error(err);
        return;
    }
    let mixpanelId = user.mixpanelId;
    userService.storeMixpanelId(mixpanelId);
    eventService.setPermission(true);
    eventService.setMixpanelId(mixpanelId);
    eventService.createPeopleProfile();
});

ipcMain.on('free-trial-user-query', (event) => {
    let freeTrialUserInfo = {};
    freeTrialUserInfo.freeTrialUser = userService.isFreeTrialUser();
    freeTrialUserInfo.freeTrialActive = trialService.isTrialActive();
    ipcService.send('free-trial-user', freeTrialUserInfo);
});
