const { ipcMain } = require('electron');
const supportService = require('../../services/support-service.js');
const heroService = require('../../services/hero-service.js');
const ipcService = require('../../services/ipc-service.js');    

ipcMain.on('support-code', async (event, code) => {
    let validCode = await supportService.verifySupportCode(code);
    ipcService.send('support-code-checked', validCode);
});

ipcMain.on('extension-support-code', async (event, code) => {
    let validCode = await supportService.verifySupportCode(code);
    if (validCode) {
        await heroService.deleteHeroes();
    }
    ipcService.send('extension-support-code-checked', validCode);
});