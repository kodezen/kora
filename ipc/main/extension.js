const { ipcMain } = require('electron');
let userService = require('../../services/user-service.js');
let extensionService = require('../../services/extension-service.js');
let ipcService = require('../../services/ipc-service.js');
let electronWindow = require('../../electron-win.js');

ipcMain.on('check-chrome-extension-connected', (event) => {
    if (extensionService.isExtensionConnected()) {
        ipcService.send('chrome-extension-connected')
        electronWindow.show();
    }
});

// not currently used anywhere but might be useful  in the future
ipcMain.on('check-chrome-setup-completed', (event) => {
    let chromeSetupStatus = userService.isChromeSetupCompleted();
    ipcService.send('chrome-setup-completed-status', chromeSetupStatus);
    if (chromeSetupStatus) {
        electronWindow.show();
    }
});