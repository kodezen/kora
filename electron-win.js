win = null;

function getWindow() {
    return win;
}

function setWindow(window) {
    win = window;
}

function show() {
    if (win != null) {
        win.setAlwaysOnTop(true);
        win.show();
        setTimeout(() => {
            win.setAlwaysOnTop(false);
        }, 1000)
    }
}

exports.getWindow = getWindow;
exports.setWindow = setWindow;
exports.show = show;