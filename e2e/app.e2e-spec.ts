import { CarbonPage } from './app.po';

describe('carbon App', function() {
  let page: CarbonPage;

  beforeEach(() => {
    page = new CarbonPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
