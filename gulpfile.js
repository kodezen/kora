const gulp = require('gulp');
const spawn = require('cross-spawn');
const replace = require('gulp-replace');
const inject = require('gulp-inject');
const bump = require('gulp-bump');

/*** PRODUCTION ***/

gulp.task('ng-build', (done) => {
    spawn.sync('ng', ['build', '--prod=true'], { stdio: 'inherit'});
    done();
});

gulp.task('change-href', () => {
    let target = gulp.src('./distAngular/index.html');
    return target
        .pipe(replace('<base href="/">', '<base href="./">'))
        .pipe(gulp.dest('./distAngular'));
});

gulp.task('insert-prod-headers', () => {
    let target = gulp.src('./distAngular/index.html');
    let source = gulp.src('./src/prod-headers.html');
    return target.pipe(inject(source, {
        starttag: '<!-- inject:prod-headers -->',
        transform: function (filePath, file) {
            return file.contents.toString('utf8')
        }
    }))
    .pipe(gulp.dest('./distAngular'));
});

gulp.task('production-mode', (done) => {
    let target = gulp.src('./main.js');
    target
        .pipe(replace('let productionMode = false;', 'let productionMode = true;'))
        .pipe(gulp.dest('./'));
    done();
});

gulp.task('prod-config', (done) => {
    let packageTarget = gulp.src('./package.json');
    packageTarget
        .pipe(replace('koradevinstall', 'korainstall'))
        .pipe(replace('"kora-dev"', '"kora"'))
        .pipe(gulp.dest('./'));

    let portTarget = gulp.src('./server/server.js');
    portTarget
        .pipe(replace('9010', '9009'))
        .pipe(gulp.dest('./server'));

    let schedTaskXMLTarget = gulp.src('./schedTaskFiles/KoraMonitor.txt');
    schedTaskXMLTarget
        .pipe(replace('<URI>KoraMonitorDev</URI>', '<URI>KoraMonitor</URI>'))
        .pipe(replace('\\kora-dev\\', '\\kora\\'))
        .pipe(gulp.dest('./schedTaskFiles'));

    let batTarget = gulp.src('./schedTaskFiles/StartKora.bat');
    batTarget
        .pipe(replace('kora-dev.exe', 'kora.exe'))
        .pipe(replace('\\kora-dev\\', '\\kora\\'))
        .pipe(gulp.dest('./schedTaskFiles'));

    let vbsTarget = gulp.src('./schedTaskFiles/StartKora.vbs');
    vbsTarget
        .pipe(replace('\\kora-dev\\', '\\kora\\'))
        .pipe(gulp.dest('./schedTaskFiles'));

    let installerTarget = gulp.src('./build/installer.nsh');
    installerTarget
        .pipe(replace('\\createScheduledTask-dev-win.exe', '\\createScheduledTask-win.exe'))
        .pipe(replace('\\kora-dev\\', '\\kora\\'))
        .pipe(replace('"KoraMonitorDev"', '"KoraMonitor"'))
        .pipe(gulp.dest('./build'));

    done();
});

gulp.task('code-sign-options', () => {
    let packageTarget = gulp.src('./package.json');
    return packageTarget
        .pipe(replace('"target": "nsis",', '"target": "nsis",\n\t\t\t"certificateSubjectName": "Red Koi Labs, LLC",'))
        .pipe(gulp.dest('./'));
});

gulp.task('prod', gulp.series('code-sign-options', 'prod-config', 'production-mode'));

/*** DEVELOPMENT ***/

gulp.task('development-mode', (done) => {
    let target = gulp.src('./main.js');
    target
        .pipe(replace('let productionMode = true;', 'let productionMode = false;'))
        .pipe(gulp.dest('./'));
    done();
});

gulp.task('dev-config', (done) => {
    let packageTarget = gulp.src('./package.json');
    packageTarget
        .pipe(replace('korainstall', 'koradevinstall'))
        .pipe(replace('"kora"', '"kora-dev"'))
        .pipe(replace('\n\t\t\t"certificateSubjectName": "Red Koi Labs, LLC",', ''))
        .pipe(gulp.dest('./'));

    let portTarget = gulp.src('./server/server.js');
    portTarget
        .pipe(replace('9009', '9010'))
        .pipe(gulp.dest('./server'));

    let schedTaskXMLTarget = gulp.src('./schedTaskFiles/KoraMonitor.txt');
    schedTaskXMLTarget
        .pipe(replace('<URI>KoraMonitor</URI>', '<URI>KoraMonitorDev</URI>'))
        .pipe(replace('\\kora\\', '\\kora-dev\\'))
        .pipe(gulp.dest('./schedTaskFiles'));

    let batTarget = gulp.src('./schedTaskFiles/StartKora.bat');
    batTarget
        .pipe(replace('kora.exe', 'kora-dev.exe'))
        .pipe(replace('\\kora\\', '\\kora-dev\\'))
        .pipe(gulp.dest('./schedTaskFiles'));

    let vbsTarget = gulp.src('./schedTaskFiles/StartKora.vbs');
    vbsTarget
        .pipe(replace('\\kora\\', '\\kora-dev\\'))
        .pipe(gulp.dest('./schedTaskFiles'));

    let installerTarget = gulp.src('./build/installer.nsh');
    installerTarget
        .pipe(replace('\\createScheduledTask-win.exe', '\\createScheduledTask-dev-win.exe'))
        .pipe(replace('\\kora\\', '\\kora-dev\\'))
        .pipe(replace('"KoraMonitor"', '"KoraMonitorDev"'))
        .pipe(gulp.dest('./build'));

    done();
})

gulp.task('dev', gulp.series('dev-config', 'development-mode'), (done) => {
    done();
});

/*** BUILD ***/

gulp.task('build-angular', gulp.series('production-mode', 'ng-build', 'change-href', 'insert-prod-headers'), (done) => {
    done();
});

gulp.task('dist', (done) => {
    spawn.sync('yarn', ['dist'], { stdio: 'inherit'});
    done();
});

/*** RELEASE ***/

gulp.task('release', (done) => {
    spawn.sync('yarn', ['release'], { stdio: 'inherit'});
    done();
});

gulp.task('version-patch', () => {
    let target = gulp.src('./package.json');
    return target.pipe(bump())
    .pipe(gulp.dest('./'));
});

gulp.task('version-minor', () => {
    let target = gulp.src('./package.json');
    return target.pipe(bump({type: 'minor'}))
    .pipe(gulp.dest('./'));
});

gulp.task('version-major', () => {
    let target = gulp.src('./package.json');
    return target.pipe(bump({type: 'major'}))
    .pipe(gulp.dest('./'));
});
