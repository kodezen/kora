import { Component, OnInit, OnDestroy } from '@angular/core';
import { IpcService } from '../services/ipc.service';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.component.html',
  styleUrls: ['./privacy.component.css']
})
export class PrivacyComponent implements OnInit, OnDestroy {
  mixpanelId: string = '';
  
  constructor(private ipcService: IpcService) { }

  ngOnInit() {
    this.ipcService.send('get-mixpanel-id');
    window.addEventListener('message', this.messageListener);
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      if (message.channel === 'mixpanel-id') {
        if (message.data) {
          this.mixpanelId = message.data;
        }
      }
    }
  };

}
