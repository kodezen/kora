import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReinstallExtensionComponent } from './reinstall-extension.component';

describe('ReinstallExtensionComponent', () => {
  let component: ReinstallExtensionComponent;
  let fixture: ComponentFixture<ReinstallExtensionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReinstallExtensionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReinstallExtensionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
