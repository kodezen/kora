import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { IpcService } from '../services/ipc.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-reinstall-extension',
  templateUrl: './reinstall-extension.component.html',
  styleUrls: ['./reinstall-extension.component.css']
})
export class ReinstallExtensionComponent implements OnInit, OnDestroy {

  constructor(private ipcService: IpcService, private zone: NgZone, private router: Router) { }

  ngOnInit() {
    window.addEventListener('message', this.messageListener);
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      let data = message.data;
      switch (message.channel) {
        case 'extension-reinstalled':
          this.dash();
          break;
      }
    }
  };

  dash() {
    this.zone.run(() => this.router.navigate(['/dashboard']));
  }

  onAddExtension() {
    this.ipcService.send('install-chrome-extension');
  }

}
