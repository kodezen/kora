import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { PremiumCodeService } from '../services/premium-code.service';
import { IpcService } from '../services/ipc.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-upgrade',
  templateUrl: './upgrade.component.html',
  styleUrls: ['./upgrade.component.css']
})
export class UpgradeComponent implements OnInit, OnDestroy {
  code: string = '';
  error: string = '';

  constructor(private premiumCodeService: PremiumCodeService, private ipcService: IpcService, private router: Router, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    window.addEventListener('message', this.messageListener);
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      let data = message.data;
      switch (message.channel) {
        case 'event-tracked: upgrade-activated-code':
          this.router.navigate(['/premium-activated']);
          break;
      }
    }
  };

  onViewPlans() {
    this.ipcService.send('view-kora-plans');
    this.ipcService.send('event: upgrade-plans-viewed');
  }

  submitCode() {
    if (this.code.length === 0) {
      this.error = 'Please enter a premium code.';
      return;
    }
    this.spinner.show();
    this.code = this.code.trim();
    this.premiumCodeService.submitPremiumCode(this.code).subscribe(result => {
      this.spinner.hide();
      let response = <any>result;
      if (response.type === 'SUCCESS') {
        let validInfo = response.data;
        if (!validInfo.valid) {
          this.error = validInfo.reason;
        } else {
          this.ipcService.send('premium-activated', this.code);
          this.ipcService.send('event: upgrade-activated-code');
        }
      } else {
        this.error = response.message;
      }
    },
      error => {
        this.spinner.hide();
        this.error = 'Internal server error. Please contact support: korablocker@gmail.com';
      });
  }

  onKeydown(event) {
    this.error = '';
    if (event.key === 'Enter') {
      this.submitCode();
    }
  }

}
