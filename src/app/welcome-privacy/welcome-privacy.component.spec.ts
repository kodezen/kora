import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WelcomePrivacyComponent } from './welcome-privacy.component';

describe('WelcomePrivacyComponent', () => {
  let component: WelcomePrivacyComponent;
  let fixture: ComponentFixture<WelcomePrivacyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WelcomePrivacyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomePrivacyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
