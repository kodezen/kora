import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IpcService } from '../services/ipc.service';

@Component({
  selector: 'app-welcome-privacy',
  templateUrl: './welcome-privacy.component.html',
  styleUrls: ['./welcome-privacy.component.css']
})
export class WelcomePrivacyComponent implements OnInit {
  permission: boolean = false;

  constructor(private ipcService: IpcService, private router: Router) { }

  ngOnInit() {
  }

  onNextClicked() {
    if (this.permission) {
      this.ipcService.send('tracking-permission-granted');
    }
    this.router.navigate(['/supported-browser']);
  }
}
