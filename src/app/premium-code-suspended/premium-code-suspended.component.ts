import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-premium-code-suspended',
  templateUrl: './premium-code-suspended.component.html',
  styleUrls: ['./premium-code-suspended.component.css']
})
export class PremiumCodeSuspendedComponent implements OnInit {

  constructor(private router: Router) { }


  ngOnInit() {
  }

  onSubmitNewCode() {
    this.router.navigate(['/submit-premium-code']);
  }

}
