import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PremiumCodeSuspendedComponent } from './premium-code-suspended.component';

describe('PremiumCodeSuspendedComponent', () => {
  let component: PremiumCodeSuspendedComponent;
  let fixture: ComponentFixture<PremiumCodeSuspendedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PremiumCodeSuspendedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PremiumCodeSuspendedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
