import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { IpcService } from '../services/ipc.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-update-available',
  templateUrl: './update-available.component.html',
  styleUrls: ['./update-available.component.css']
})
export class UpdateAvailableComponent implements OnInit, OnDestroy {
  downloadingUpdate: boolean = false;
  error: string = '';

  constructor(private ipcService: IpcService, private router: Router, private zone: NgZone, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    window.addEventListener('message', this.messageListener);
  }

  ngOnDestroy() {
    window.addEventListener('message', this.messageListener);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      switch (message.channel) {
        case 'setup-complete':
          this.ipcService.send('setup-complete-received');
          break;
        case 'error-downloading-update':
          this.zone.run(() => {
            this.router.navigate(['/dashboard']);
          });
          break;
      }
    }
  };

  onUpdatePressed() {
    this.downloadingUpdate = true;
    this.ipcService.send('update');
    this.spinner.show();
  }
}
