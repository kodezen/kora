import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsedInvalidBrowserComponent } from './used-invalid-browser.component';

describe('UsedInvalidBrowserComponent', () => {
  let component: UsedInvalidBrowserComponent;
  let fixture: ComponentFixture<UsedInvalidBrowserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsedInvalidBrowserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsedInvalidBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
