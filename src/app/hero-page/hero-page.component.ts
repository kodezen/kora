import { Component, OnInit, OnDestroy, ChangeDetectorRef, NgZone } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HeroService } from '../services/hero.service';
import { SupportService } from '../services/support.service';
import { IpcService } from '../services/ipc.service';
import { ScheduledHeroService } from '../services/scheduled-hero.service';
import * as jquery from 'jquery';


@Component({
  selector: 'app-hero-page',
  templateUrl: './hero-page.component.html',
  styleUrls: ['./hero-page.component.css', '../common/hero.css']
})
export class HeroPageComponent implements OnInit, OnDestroy {

  hero: any;
  editing: boolean = false;
  sitesLocked: boolean = false;
  blockDuration: number = 30;
  sliderDurations: any = this.createSliderDurations();
  sliderConfig: any = this.createSliderConfig();
  displaySave: boolean = false;
  name: string;
  blockedSites: string[];
  exceptions: string[];
  days: any[];
  scheduledDays: any[];

  constructor(private heroService: HeroService, private supportService: SupportService, private ipcService: IpcService, private scheduledHeroService: ScheduledHeroService, private route: ActivatedRoute, private router: Router, private zone: NgZone, private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    this.route.data.subscribe((data: { hero: any }) => {
      let hero = data.hero;
      this.hero = hero;
      this.setValues(hero);
      window.addEventListener('message', this.messageListener);
    });
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      let data = message.data;
      switch (message.channel) {
        case 'hero-started':
          this.onHeroStarted();
          break;
        case 'hero-updated':
          this.updateHero(data);
        case 'one-time-heroes-updated':
          let oneTimeHeroActive = ((this.hero.blockType === 'one-time') && (this.hero.status === 'active'));
          if (oneTimeHeroActive) {
            this.onOneTimeHeroesUpdated(data);
          }
          break;
        case 'hero-deleted':
          this.zone.run(() => {
            this.router.navigate(['/dashboard']);
          });
      }
    }
  };

  setValues(hero: any) {
    let heroCopy = JSON.parse(JSON.stringify(hero));
    this.name = heroCopy.name;
    this.exceptions = heroCopy.exceptions;
    if (hero.heroType === 'blocker') {
      this.blockedSites = heroCopy.blockedSites;
    }
    if (hero.blockType === 'ongoing') {
      this.days = this.createDays(heroCopy);
      this.scheduledDays = this.createScheduledDays(heroCopy);
    }
  }

  createDays(hero) {
    let days = [];
    let blockDays = hero.schedule.blockDays;
    Object.keys(blockDays).forEach(function (key) {
      let dayOfWeek = key;
      days.push(dayOfWeek);
    });
    return days;
  }

  createScheduledDays(hero) {
    let scheduledDays = [];
    let blockDays = hero.schedule.blockDays;
    Object.keys(blockDays).forEach(function (key) {
      let blockDay = blockDays[key];
      scheduledDays.push(blockDay);
    });
    this.scheduledHeroService.sortScheduledDays(scheduledDays);
    return scheduledDays;
  }

  onSliderChanged(value: number) {
    this.blockDuration = this.sliderDurations[value.toString()];
  }

  onDaysChanged(info: any) {
    if (info.change === 'added') {
      let scheduledDay = this.scheduledHeroService.createScheduledDay(info.dayOfWeek);
      this.scheduledDays.push(scheduledDay);
      this.scheduledHeroService.sortScheduledDays(this.scheduledDays);
    } else if (info.change === 'removed') {
      this.scheduledHeroService.removeScheduledDay(this.scheduledDays, info.dayOfWeek);
    }
    this.scheduledDays = this.scheduledDays.map(e => ({ ... e }));
  }

  onScheduleStatusUpdated(status: string) {
    if (this.editing) {
      if (status === 'valid') {
        this.displaySave = true;
      } else if (status === 'invalid') {
        this.displaySave = false;
      }
      this.cdRef.detectChanges();  
    }
  }

  onStartHero() {
    this.hero.blockDuration = this.blockDuration;
    this.heroService.startHero(this.hero);
  }

  createBlockDurationLabels() {
    let blockDurationLabels = {
      '1': '30 Minutes',
      '2': '1 Hour',
      '3': '2 Hours',
      '4': '3 Hours',
      '5': '4 Hours',
      '6': '5 Hours',
      '7': '6 Hours',
      '8': '7 Hours',
      '9': '8 Hours',
      '10': '9 Hours',
      '11': '10 Hours',
      '12': '11 Hours',
      '13': '12 Hours',
      '14': '1 Day',
      '15': '2 Days',
      '16': '3 Days',
      '17': '4 Days',
      '18': '5 Days',
      '19': '6 Days',
      '20': '7 Days'
    }
    return blockDurationLabels;
  }

  createSliderDurations() {
    let sliderDurations = {
      '1': 30,
      '2': 60,
      '3': 120,
      '4': 180,
      '5': 240,
      '6': 300,
      '7': 360,
      '8': 420,
      '9': 480,
      '10': 540,
      '11': 600,
      '12': 660,
      '13': 720,
      '14': 1440,
      '15': 2880,
      '16': 4320,
      '17': 5760,
      '18': 7200,
      '19': 8640,
      '20': 10080
    }
    return sliderDurations;
  }

  createSliderConfig() {
    let sliderConfig = {
      sliderLabel: 'How long should I block for?',
      durationLabels: this.createBlockDurationLabels(),
      startLabel: '30 Min',
      endLabel: '7 Days',
      min: 1,
      max: 20,
      value: 1
    };
    return sliderConfig;
  }

  showDeleteModal() {
    let overlay = jquery('.modal-overlay');
    let modal = jquery('.delete-modal');
    overlay.show();
    modal.show();
  }

  hideDeleteModal() {
    let overlay = jquery('.modal-overlay');
    let modal = jquery('.delete-modal');
    overlay.hide();
    modal.hide();
  }

  showDeleteButton() {
    if (this.hero.status !== 'active') {
      return true;
    } else {
      let supportModeOn = this.supportService.getSupportModeOn();
      return supportModeOn;
    }
  }
  
  onDeleteRequest() {
    this.showDeleteModal();
  }

  onDeleteConfirmed() {
    this.heroService.deleteHero(this.hero);
  }

  onEditPressed() {
    if (this.hero.status === 'active') {
      let supportModeOn = this.supportService.getSupportModeOn();
      if (!supportModeOn) {
        this.sitesLocked = true;
      }
    }
    this.editing = true;
    this.displaySave = true;
  }

  onCancelPressed() {
    this.setValues(this.hero);
    this.editing = false;
    this.displaySave = false;
    this.sitesLocked = false;
  }

  onSavePressed() {
    let hero = this.hero;
    hero.name = this.name;
    hero.exceptions = this.exceptions;
    if (hero.blockType === 'ongoing') {
      hero.schedule = this.scheduledHeroService.prepareSchedule(this.scheduledDays);
    }
    if (hero.heroType === 'blocker') {
      hero.blockedSites = this.blockedSites;
    }
    this.heroService.updateHero(hero);
    this.editing = false;
    this.displaySave = false;
  }

  onNameChanged(name: string) {
    this.name = name;
  }

  onHeroStarted = () => {
    this.zone.run(() => {
      this.router.navigate(['/dashboard']);
    });
  }
  
  onOneTimeHeroesUpdated = (heroes: any) => {
    console.log('on oneTimeHeroesUpdated');
    for (var i = 0; i < heroes.length; i++) {
      let candidateHero = heroes[i];
      let isMatchingHero = (candidateHero._id === this.hero._id);
      if (isMatchingHero) {
        this.updateHero(candidateHero);
      }
    }
  }
  
  updateHero(updatedHero: any) {
    let status = updatedHero.status;
    this.hero.status = status;
    if (status === 'active') {
      this.hero.timeRemaining = updatedHero.timeRemaining;
    }
    this.cdRef.detectChanges();  
  }

  onBackPressed() {
    this.router.navigate(['/dashboard']);  
  }

}
