import { Component, OnInit, OnChanges, SimpleChanges, Input, Output, EventEmitter} from '@angular/core';
import { AmazingTimePickerService } from 'amazing-time-picker';
import * as moment from 'moment';

@Component({
  selector: 'app-ongoing-schedule',
  templateUrl: './ongoing-schedule.component.html',
  styleUrls: ['./ongoing-schedule.component.css', '../hero.css']
})
export class OngoingScheduleComponent implements OnInit, OnChanges {

  @Input() scheduledDays: any[];
  @Input() editing: boolean;
  @Input() lockCurrentScheduledDay: boolean;
  @Output() scheduleStatusUpdated: EventEmitter<string> = new EventEmitter<string>();
  validSchedule: boolean;
  daysOfWeek: any = {'1': 'Monday', '2': 'Tuesday', '3': 'Wednesday', '4': 'Thursday', '5': 'Friday',
                          '6': 'Saturday', '7': 'Sunday'};

  constructor(private atp: AmazingTimePickerService) { }

  ngOnInit() {
    this.updateScheduledDays();
    let validSchedule = this.validateSchedule();
    this.updateScheduleValidity(validSchedule);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.updateScheduledDays();
    let validSchedule = this.validateSchedule();
    this.updateScheduleValidity(validSchedule);
  }

  updateScheduledDays() {
    let now = moment();
    let scheduledDays = this.scheduledDays;
    for (var i = 0; i < scheduledDays.length; i++) {
      let scheduledDay = scheduledDays[i];
      this.updateTimeDisplays(scheduledDay);
      this.updateLength(scheduledDay);
      console.log(scheduledDay);
      if (this.lockCurrentScheduledDay) {
        let isScheduledDayActive = this.checkScheduledDayActive(scheduledDay, now);
        if (isScheduledDayActive) {
          scheduledDay.editLocked = true;
        }
      }
    }
  }

  checkScheduledDayActive(scheduledDay: any, now: any) {
    let dayOfWeek = now.isoWeekday().toString();
    if (dayOfWeek !== scheduledDay.dayOfWeek) {
      return false;
    }
    let start = moment(scheduledDay.startTime, 'HH:mm');
    let end = moment(scheduledDay.endTime, 'HH:mm');
    if (scheduledDay.endTime === '00:00') {
      end.add(1, 'days');
    }
    if (now.isBetween(start, end, null, '[)')) {
      return true;
    } else {
      return false;
    }
  }

  openTimePicker(type: string, scheduledDay: any) {
    let time = '';
    if (type === 'start') {
      time = scheduledDay.startTime;
    } else if (type === 'end') {
      time = scheduledDay.endTime;
    }
    const amazingTimePicker = this.atp.open({
      time: time,
      theme: 'material-green',
      changeToMinutes: true
    });
    amazingTimePicker.afterClose().subscribe(setTime => {
      this.onTimeUpdated(type, scheduledDay, setTime);
    });
  }

  onTimeUpdated(type: string, scheduledDay: any, setTime: string) {
    this.updateTime(type, scheduledDay, setTime);
    let isTimesValid = this.validateTimes(scheduledDay);
    scheduledDay.validTimes = isTimesValid;
    if (isTimesValid) {
      this.updateLength(scheduledDay);
      if (!this.validSchedule) {
        let validSchedule = this.validateSchedule();
        if (validSchedule) {
          this.updateScheduleValidity(true);
        }
      } 
    } else {
      this.updateScheduleValidity(false); 
    }
  }

  updateTime(type: string, scheduledDay: any, setTime: string) {
    if (type === 'start') {
      scheduledDay.startTime = setTime;
      scheduledDay.startTimeDisplay = this.formatTimeDisplay(setTime);
    } else if (type === 'end') {
      scheduledDay.endTime = setTime;
      scheduledDay.endTimeDisplay = this.formatTimeDisplay(setTime);
    }
  }

  formatTimeDisplay(time: string) {
    let timeMoment = moment(time, 'HH:mm');
    let displayTime = timeMoment.format('hh:mmA').replace("AM", "am").replace("PM","pm");
    return displayTime;
  }

  validateTimes(scheduledDay: any) {
    var startTime = scheduledDay.startTime;
    var endTime = scheduledDay.endTime;
    let momentStartTime = moment(startTime, 'hh:mm');
    let momentEndTime = moment(endTime, 'hh:mm');
    if (momentStartTime.isAfter(momentEndTime)) {
      if (endTime == '00:00') {
        return true;
      } else {
        return false;
      }
    } else {
      return true;
    }
  }

  validateSchedule() {
    var validSchedule = true;
    let scheduledDays = this.scheduledDays;
    for (var i = 0; i < scheduledDays.length; i++) {
      let scheduledDay = scheduledDays[i];
      let invalidTimes = !scheduledDay.validTimes;
      if(invalidTimes) {
        validSchedule = false;
      }
    }
    return validSchedule;
  }

  updateScheduleValidity(validSchedule: boolean) {
    this.validSchedule = validSchedule;
    if (validSchedule) {
      this.scheduleStatusUpdated.emit('valid');
    } else {
      this.scheduleStatusUpdated.emit('invalid');
    }
  }

  updateTimeDisplays(scheduledDay: any) {
    scheduledDay.startTimeDisplay = this.formatTimeDisplay(scheduledDay.startTime);
    scheduledDay.endTimeDisplay = this.formatTimeDisplay(scheduledDay.endTime);
  }

  updateLength(scheduledDay: any) {
    var startTime = moment(scheduledDay.startTime, 'HH:mm');
    var endTime;
    if (scheduledDay.endTime === '00:00') {
      if (scheduledDay.startTime === '00:00') {
        scheduledDay.lengthHours = 24;
        scheduledDay.lengthMinutes = 0;
        return;
      } else {
        endTime = moment('24:00', 'HH:mm');
      }
    } else {
      endTime = moment(scheduledDay.endTime, 'HH:mm');
    }
    let differenceInMinutes = endTime.diff(startTime, 'minutes');
    let lengthHours = Math.floor(differenceInMinutes / 60);
    let lengthMinutes = differenceInMinutes % 60;
    scheduledDay.lengthHours = lengthHours;
    scheduledDay.lengthMinutes = lengthMinutes;
  }

  // getScheduledDay(dayOfWeek: string) {
  //   let scheduledDay;
  //   let scheduledDays = this.scheduledDays;
  //   for (var i =  0; i < this.scheduledDays.length; i++) {
  //     let scheduledDayCandidate = scheduledDays[i];
  //     if (scheduledDayCandidate.dayOfWeek === dayOfWeek) {
  //       scheduledDay = scheduledDayCandidate;
  //     }
  //   }
  //   return scheduledDay;
  // }

}
