import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-name',
  templateUrl: './name.component.html',
  styleUrls: ['./name.component.css', '../hero.css']
})
export class NameComponent implements OnInit {

  @Input() label: string;
  @Input() name: string;
  @Input() editable: boolean;
  @Output() nameChanged: EventEmitter<string> = new EventEmitter<string>();
  @Output() nameEntered: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {

  }

  onNameChanged(newValue: string) {
    this.name = newValue;
    this.nameChanged.emit(newValue);
  }

  onKeydown(event) {
    if (event.key === 'Enter') {
      this.nameEntered.emit(this.name);    
    }
  }

}
