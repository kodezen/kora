import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'app-days',
  templateUrl: './days.component.html',
  styleUrls: ['./days.component.css', '../hero.css']
})
export class DaysComponent implements OnInit {

  @Input() days: string[];
  @Input() editing: boolean;
  @Input() scheduledDays: any;
  @Output() daysChanged: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {
  }

  onDayOfWeekClick(dayOfWeek: string) {
    let dayNotYetAdded = !this.dayAdded(dayOfWeek);
    if (dayNotYetAdded) {
      this.addDay(dayOfWeek);
    } else {
      if (this.editing) {
        let scheduledDay = this.getScheduledDay(dayOfWeek);
        let now = moment();
        let isScheduledDayNotActive = !this.checkScheduledDayActive(scheduledDay, now);
        if (isScheduledDayNotActive) {
          this.removeDay(dayOfWeek);
        }
      } else {
        this.removeDay(dayOfWeek);
      }
    }
  }

  dayAdded(dayOfWeek: string) {
    for (var i = 0; i < this.days.length; i++) {
      let dayOfWeekCandidate = this.days[i];
      if (dayOfWeekCandidate === dayOfWeek) {
        return true;
      }
    }
    return false;
  }

  addDay(dayOfWeek: string) {
    this.days.push(dayOfWeek);
    this.days.sort(function(a, b) {
      return parseInt(a, 10) - parseInt(b, 10);
    });
    this.daysChanged.emit({
      change: 'added',
      dayOfWeek: dayOfWeek
    });
  }

  removeDay(dayOfWeek: string) {
    let index = this.days.indexOf(dayOfWeek);
    this.days.splice(index, 1);
    this.daysChanged.emit({
      change: 'removed',
      dayOfWeek: dayOfWeek
    });
  }

  getScheduledDay(dayOfWeek: string) {
    let scheduledDays = this.scheduledDays;
    for (var i = 0; i < scheduledDays.length; i++) {
      let scheduledDayCandidate = scheduledDays[i];
      if (scheduledDayCandidate.dayOfWeek === dayOfWeek) {
        return scheduledDayCandidate;
      }
    }
  }

  checkScheduledDayActive(scheduledDay: any, now: any) {
    let dayOfWeek = now.isoWeekday().toString();
    if (dayOfWeek !== scheduledDay.dayOfWeek) {
      return false;
    }
    let start = moment(scheduledDay.startTime, 'HH:mm');
    let end = moment(scheduledDay.endTime, 'HH:mm');
    let midnight = moment('00:00', 'HH:mm');
    if (end.isSame(midnight)) {
      end.add('1', 'days');
    }
    if (now.isBetween(start, end, null, '[)')) {
      return true;
    } else {
      return false;
    }
  }

}

