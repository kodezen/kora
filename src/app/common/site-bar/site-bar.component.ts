import { Component, OnInit, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-site-bar',
  templateUrl: './site-bar.component.html',
  styleUrls: ['./site-bar.component.css']
})
export class SiteBarComponent implements OnInit {
  site: string = '';
  @Output() addedSite: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {}

  addSite(){
    if (this.site.length > 0){
      this.site = this.site.trim();
      this.site = this.site.toLowerCase();
      this.addedSite.emit(this.site);
      this.clearSite();
    }
  }

  clearSite() {
    this.site = '';
  }

  onKeydown(event) {
    if (event.key === 'Enter') {
      this.addSite();        
    }
  }

}
