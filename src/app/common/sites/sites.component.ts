import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-sites',
  templateUrl: './sites.component.html',
  styleUrls: ['./sites.component.css']
})
export class SitesComponent implements OnInit {
  @Input() blockedSites: string[];
  @Input() editable: boolean;
  @Output() removeSite:EventEmitter<string> = new EventEmitter<string>();
  
  constructor() { }

  ngOnInit() {
  }

  passUpRemoveSite(site: string){
    this.removeSite.emit(site);
  }
}
