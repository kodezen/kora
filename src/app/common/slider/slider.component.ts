import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css', '../hero.css']
})
export class SliderComponent implements OnInit {

  @Input() sliderConfig: any;
  @Output() sliderChanged: EventEmitter<number> = new EventEmitter<number>();
  durationDisplay: string = '';

  constructor() { }

  ngOnInit() {
    let initialValue = this.sliderConfig.value;
    this.durationDisplay = this.sliderConfig.durationLabels[initialValue.toString()];
  }

  onSliderChange(event: any) {
    let value = event.value;
    this.durationDisplay = this.sliderConfig.durationLabels[value.toString()];
    this.sliderChanged.emit(value);
  }

}
