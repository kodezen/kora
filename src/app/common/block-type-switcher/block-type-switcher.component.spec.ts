import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockTypeSwitcherComponent } from './block-type-switcher.component';

describe('HeroTypeSwitcherComponent', () => {
  let component: BlockTypeSwitcherComponent;
  let fixture: ComponentFixture<BlockTypeSwitcherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockTypeSwitcherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockTypeSwitcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
