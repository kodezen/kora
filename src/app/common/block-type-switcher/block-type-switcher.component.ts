import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-block-type-switcher',
  templateUrl: './block-type-switcher.component.html',
  styleUrls: ['./block-type-switcher.component.css', '../hero.css']
})
export class BlockTypeSwitcherComponent implements OnInit {

  @Input() blockType: string;
  @Input() editable: boolean;
  @Output() blockTypeSelected: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {

  }

  onBlockTypeSelected(blockType: string) {
    this.blockType = blockType;
    this.blockTypeSelected.emit(blockType);
  }

}
