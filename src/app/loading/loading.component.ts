import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { Router,  } from '@angular/router';
import { IpcService } from '../services/ipc.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit, OnDestroy {

  waitingOnSetupComplete: boolean = true;

  constructor(private ipcService: IpcService, private zone: NgZone, private router: Router) { }

  ngOnInit() {
    window.addEventListener('message', this.messageListener);
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      if (message.channel === 'setup-complete') {
        this.onSetupComplete(message.data);
      }
    }
  };

  onSetupComplete = (startupBundle: any) => {
    if (this.waitingOnSetupComplete) {
      this.waitingOnSetupComplete = false;
      this.ipcService.send('setup-complete-received');
      if (startupBundle.newUser) {
        this.router.navigate(['/introduction']);
      } else if (!startupBundle.firstBlockCreated) {
        this.router.navigate(['/blocker'], {
          state: { firstBlock: true }
        });
      } else if (startupBundle.freeTrialExpired) {
        this.router.navigate(['/trial-expired']);
      } else {
        this.router.navigate(['/dashboard']);
      }     
    }
  }
}
