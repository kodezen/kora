import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { IpcService } from '../services/ipc.service';
import { TrialService } from '../services/trial.service';

@Component({
  selector: 'app-trial-expiring-reminder',
  templateUrl: './trial-expiring-reminder.component.html',
  styleUrls: ['./trial-expiring-reminder.component.css']
})
export class TrialExpiringReminderComponent implements OnInit, OnDestroy {
  trialTimeRemaining: any = {
    days: 2,
    hours: 0,
    minutes: 0,
    seconds: 0
  };
  trialSecondsInterval: any;

  constructor(private trialService: TrialService, private ipcService: IpcService, private router: Router) { }

  ngOnInit() {
    window.addEventListener('message', this.messageListener);
    this.startTrialTimeSync();
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
    clearInterval(this.trialSecondsInterval);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      let data = message.data;
      switch (message.channel) {
        case 'trial-time-remaining':
          this.onTrialTimeSyncResponse(data);
          break;
        case 'event-tracked: trial-expiring-reminder-clicked-code':
          this.router.navigate(['/trial-offer']);
          break;
        case 'event-tracked: trial-expiring-reminder-clicked-dashboard':
          this.router.navigate(['/dashboard']);
          break;
      }
    }
  };

  onGetCodeClicked() {
    this.ipcService.send('event: trial-expiring-reminder-clicked-code')
  }

  onDashboardClicked() {
    this.ipcService.send('event: trial-expiring-reminder-clicked-dashboard');
  }

  startTrialTimeSync() {
    this.ipcService.send('get-trial-time-remaining');
  }

  onTrialTimeSyncResponse(trialTimeRemainingSeconds) {
    let trialExpired = (trialTimeRemainingSeconds <= 0);
    if (trialExpired) {
      this.ipcService.send('trial-expired');
      return;
    } 
    let timeRemainingDuration = moment.duration(trialTimeRemainingSeconds, 'seconds');
    this.trialService.updateTrialTimeRemaining(this.trialTimeRemaining, timeRemainingDuration);
    this.startTrialSecondsInterval();
  }

  startTrialSecondsInterval() {
    this.trialSecondsInterval = setInterval(() => {
      let seconds = parseInt(this.trialTimeRemaining.seconds);
      let timeToSync = (seconds <= 0);
      if (timeToSync) {
        clearInterval(this.trialSecondsInterval);
        this.startTrialTimeSync();
      } else {
        seconds--;
        this.trialTimeRemaining.seconds = this.trialService.format(seconds);
      }
    }, 1000);
  }

}
