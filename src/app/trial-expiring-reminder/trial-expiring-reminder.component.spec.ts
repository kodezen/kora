import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrialExpiringReminderComponent } from './trial-expiring-reminder.component';

describe('TrialExpiringReminderComponent', () => {
  let component: TrialExpiringReminderComponent;
  let fixture: ComponentFixture<TrialExpiringReminderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrialExpiringReminderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrialExpiringReminderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
