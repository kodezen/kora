import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { IpcService } from '../services/ipc.service';
import { HeroService } from '../services/hero.service';

@Component({
  selector: 'app-setup-finished',
  templateUrl: './setup-finished.component.html',
  styleUrls: ['./setup-finished.component.css']
})
export class SetupFinishedComponent implements OnInit, OnDestroy {

  constructor(private heroService: HeroService, private ipcService: IpcService, private router: Router) { }

  ngOnInit() {
    window.addEventListener('message', this.messageListener);
    this.ipcService.send('event: setup-finished');
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      let data = message.data;
      switch (message.channel) {
        case 'event-tracked: setup-finished':
            this.heroService.createHero(window.history.state.firstBlock);
            break;
        case 'hero-created':
            setTimeout(() => {
              this.router.navigate(['/dashboard'], {
                state: { firstBlock: window.history.state.firstBlock }
              });
            }, 5000)
            break;
      }
    }
  };

}
