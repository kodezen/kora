import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PremiumActivatedComponent } from './premium-activated.component';

describe('PremiumActivatedComponent', () => {
  let component: PremiumActivatedComponent;
  let fixture: ComponentFixture<PremiumActivatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PremiumActivatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PremiumActivatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
