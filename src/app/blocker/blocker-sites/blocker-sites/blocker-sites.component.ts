import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-blocker-sites',
  templateUrl: './blocker-sites.component.html',
  styleUrls: ['./blocker-sites.component.css', '../../../common/hero.css']
})
export class BlockerSitesComponent implements OnInit {

  @Input() blockedSites: string[];
  @Input() exceptions: string[];
  @Input() editable: boolean;
  @Input() sitesLocked: boolean;
  @Input() displayMode: boolean;
  switcherChoice: string = 'blocked sites';
  sitesAdded: boolean = false;

  constructor() { }

  ngOnInit() {}

  addSite(site: string) {
    if (this.switcherChoice === 'blocked sites') {
      this.blockedSites.push(site);
    } else {
      this.exceptions.push(site);
    }
  }

  onSwitcherChange(switcherChoice: string) {
    this.switcherChoice = switcherChoice;
  }
}
