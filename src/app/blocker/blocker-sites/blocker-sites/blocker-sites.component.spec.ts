import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockerSitesComponent } from './blocker-sites.component';

describe('BlockerSitesComponent', () => {
  let component: BlockerSitesComponent;
  let fixture: ComponentFixture<BlockerSitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockerSitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockerSitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
