import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockerAddSitesComponent } from './blocker-add-sites.component';

describe('BlockerAddSitesComponent', () => {
  let component: BlockerAddSitesComponent;
  let fixture: ComponentFixture<BlockerAddSitesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockerAddSitesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockerAddSitesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
