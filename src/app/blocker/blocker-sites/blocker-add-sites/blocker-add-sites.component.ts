import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-blocker-add-sites',
  templateUrl: './blocker-add-sites.component.html',
  styleUrls: ['./blocker-add-sites.component.css', '../../../common/hero.css']
})
export class BlockerAddSitesComponent implements OnInit {

  @Input() switcherChoice: string;
  @Output() siteAdded: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {

  }

  addSite(site) {
    this.siteAdded.emit(site);
  }



}
