import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockedSitesExceptionsComponent } from './blocked-sites-exceptions.component';

describe('BlockedSitesExceptionsComponent', () => {
  let component: BlockedSitesExceptionsComponent;
  let fixture: ComponentFixture<BlockedSitesExceptionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockedSitesExceptionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockedSitesExceptionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
