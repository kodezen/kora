import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

@Component({
  selector: 'app-blocked-sites-exceptions',
  templateUrl: './blocked-sites-exceptions.component.html',
  styleUrls: ['./blocked-sites-exceptions.component.css', '../../../common/hero.css']
})
export class BlockedSitesExceptionsComponent implements OnInit {

  @Input() blockedSites: string[];
  @Input() exceptions: any;
  @Input() editable: boolean;
  @Input() sitesLocked: boolean;
  @Input() displayMode: boolean;
  @Input() switcherChoice: string;
  @Output() switcherChange: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {}

  removeBlockedSite(site: string) {
    var index = this.blockedSites.indexOf(site);
    if (index !== -1) {
      this.blockedSites.splice(index, 1);
    }
  }

  removeException(site: string) {
    var index = this.exceptions.indexOf(site);
    if (index !== -1) {
      this.exceptions.splice(index, 1);
    }
  }

  onSwitcherChange(switcherChoice: string) {
    this.switcherChoice = switcherChoice;
    this.switcherChange.emit(switcherChoice);
  }

  showBlockedSites() {
    return (this.blockedSites.length > 0) && (this.switcherChoice === 'blocked sites');
  }

  showExceptions() {
    return (this.exceptions.length > 0) && (this.switcherChoice === 'exceptions');
  }

}
