import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BlockedSitesExceptionsSwitcherComponent } from './blocked-sites-exceptions-switcher.component';

describe('BlockedSitesExceptionsSwitcherComponent', () => {
  let component: BlockedSitesExceptionsSwitcherComponent;
  let fixture: ComponentFixture<BlockedSitesExceptionsSwitcherComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BlockedSitesExceptionsSwitcherComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BlockedSitesExceptionsSwitcherComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
