import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-blocked-sites-exceptions-switcher',
  templateUrl: './blocked-sites-exceptions-switcher.component.html',
  styleUrls: ['./blocked-sites-exceptions-switcher.component.css', '../../../common/hero.css']
})
export class BlockedSitesExceptionsSwitcherComponent implements OnInit {

  @Input() display: boolean;
  @Input() switcherChoice: string;
  @Output() switcherChange: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  onSwitcherChange(switcherChoice: string) {
    this.switcherChoice = switcherChoice;
    this.switcherChange.emit(switcherChoice);
  }

}
