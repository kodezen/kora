import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { IpcService } from '../../services/ipc.service';
import { HeroService } from '../../services/hero.service';
import { ScheduledHeroService } from '../../services/scheduled-hero.service';

@Component({
  selector: 'app-blocker',
  templateUrl: './blocker.component.html',
  styleUrls: ['./blocker.component.css', '../../common/hero.css']
})
export class BlockerComponent implements OnInit, OnDestroy {
  hideBackButton: boolean = false;
  currentStep: string = 'sites';
  blockType: string = 'one-time';
  name: string = '';
  blockedSites: string[] = [];
  exceptions: string[] = [];
  sliderConfig: any = this.genSliderConfig();
  sliderDurations: any = this.genSliderDurations();
  blockDuration: number = 30;
  days: string[] = [];
  scheduledDays: any[] = [];
  validSchedule: boolean = true;

  constructor( private heroService: HeroService, private ipcService: IpcService, private scheduledHeroService: ScheduledHeroService, private zone: NgZone, private router: Router) { }

  ngOnInit() {
    if (window.history.state.firstBlock) {
      this.hideBackButton = true;
    }
    window.addEventListener('message', this.messageListener);
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      switch (message.channel) {        
        case 'hero-created':
          this.zone.run(() => {
            this.router.navigate(['/dashboard']);
          });
          break;
        case 'event-tracked: first-block-filled-out':
          let hero = this.prepareHeroObj();
          this.router.navigate(['/supported-browser'], {
            state: { firstBlock: hero }
          });
          break;
      }      
    }
  };

  onBlockTypeSelected(blockType: string) {
    this.blockType = blockType;
    if (blockType === 'one-time') {
      this.currentStep = 'block-duration-slider';
    } else if (blockType === 'ongoing') {
      this.currentStep = 'select-days';
    }
  }

  continue(stepToAdvanceTo: string) {
    if (stepToAdvanceTo === 'finish') {
      this.onFinish();
    }
    if (stepToAdvanceTo === 'name' && window.history.state.firstBlock) {
      this.name = 'My First Block';
      this.continue('finish');
      return;
    }
    this.currentStep = stepToAdvanceTo;
  }

  onBackPressed() {
    if (this.currentStep === 'sites') {
      this.router.navigate(['/dashboard']);
    } else if (this.currentStep === 'block-type') {
      this.currentStep = 'sites';
    } else if (this.currentStep === 'block-duration-slider') {
      this.currentStep = 'block-type';
    } else if (this.currentStep === 'select-days') {
      this.currentStep = 'block-type';
    } else if (this.currentStep === 'set-schedule') {
      this.currentStep = 'select-days';
    } else if (this.currentStep === 'name') {
      if (this.blockType === 'one-time') {
        this.currentStep = 'block-duration-slider';
      } else if (this.blockType === 'ongoing') {
        this.currentStep = 'set-schedule';
      }
    }
  }

  onSliderChanged(value: number) {
    this.blockDuration = this.sliderDurations[value.toString()];
  }

  onDaysChanged(info: any) {
    if (info.change === 'added') {
      let scheduledDay = this.scheduledHeroService.createScheduledDay(info.dayOfWeek);
      this.scheduledDays.push(scheduledDay);
      this.scheduledHeroService.sortScheduledDays(this.scheduledDays);
    } else if (info.change === 'removed') {
      this.scheduledHeroService.removeScheduledDay(this.scheduledDays, info.dayOfWeek);
    }
  }

  onScheduleStatusUpdated(scheduleStatus: string) {
    if (scheduleStatus === 'valid') {
      this.validSchedule = true;
    } else if (scheduleStatus === 'invalid') {
      this.validSchedule = false;
    }
  }

  onNameChanged(name: string) {
    this.name = name;
  }

  onNameEntered(name: string) {
    this.name = name;
    this.onFinish();
  }

  onFinish() {
    if (window.history.state.firstBlock) {
      this.ipcService.send('event: first-block-filled-out');
    } else {
      let hero = this.prepareHeroObj();
      this.heroService.createHero(hero);
    }
  }

  prepareHeroObj() {
    let hero: any = {
      heroType: 'blocker',
      blockType: this.blockType,
      name: this.name,
      blockedSites: this.blockedSites,
      exceptions: this.exceptions
    };
    if (this.blockType === 'one-time') {
      hero.blockDuration = this.blockDuration;
    } else if (this.blockType === 'ongoing') {
      hero.schedule = this.scheduledHeroService.prepareSchedule(this.scheduledDays);
    }
    return hero;
  }

  genSliderDurations() {
    let sliderDurations = {
      '1': 30,
      '2': 60,
      '3': 120,
      '4': 180,
      '5': 240,
      '6': 300,
      '7': 360,
      '8': 420,
      '9': 480,
      '10': 540,
      '11': 600,
      '12': 660,
      '13': 720,
      '14': 1440,
      '15': 2880,
      '16': 4320,
      '17': 5760,
      '18': 7200,
      '19': 8640,
      '20': 10080
    }
    return sliderDurations;
  }

  genSliderConfig() {
    let sliderConfig = {
      sliderLabel: 'How long should I block for?',
      durationLabels: this.createBlockDurationLabels(),
      startLabel: '30 Min',
      endLabel: '7 Days',
      min: 1,
      max: 20,
      value: 1
    };
    return sliderConfig;
  }

  createBlockDurationLabels() {
    let blockDurationLabels = {
      '1': '30 Minutes',
      '2': '1 Hour',
      '3': '2 Hours',
      '4': '3 Hours',
      '5': '4 Hours',
      '6': '5 Hours',
      '7': '6 Hours',
      '8': '7 Hours',
      '9': '8 Hours',
      '10': '9 Hours',
      '11': '10 Hours',
      '12': '11 Hours',
      '13': '12 Hours',
      '14': '1 Day',
      '15': '2 Days',
      '16': '3 Days',
      '17': '4 Days',
      '18': '5 Days',
      '19': '6 Days',
      '20': '7 Days'
    }
    return blockDurationLabels;
  }


}
