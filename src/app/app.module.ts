import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatSliderModule } from '@angular/material/slider';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatExpansionModule } from '@angular/material/expansion';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AmazingTimePickerModule } from 'amazing-time-picker';

import { AppComponent } from './app.component';
import { SiteBarComponent } from './common/site-bar/site-bar.component';
import { SitesComponent } from './common/sites/sites.component';
import { SiteComponent } from './common/site/site.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { BlockerComponent } from './blocker/blocker/blocker.component';
import { BlockerSitesComponent } from './blocker/blocker-sites/blocker-sites/blocker-sites.component';
import { BlockedSitesExceptionsSwitcherComponent } from './blocker/blocker-sites/blocked-sites-exceptions-switcher/blocked-sites-exceptions-switcher.component';
import { BlockedSitesExceptionsComponent } from './blocker/blocker-sites/blocked-sites-exceptions/blocked-sites-exceptions.component';
import { BlockerAddSitesComponent } from './blocker/blocker-sites/blocker-add-sites/blocker-add-sites.component';
import { LaserComponent } from './laser/laser/laser.component';
import { LaserAddSitesComponent } from './laser/laser-sites/laser-add-sites/laser-add-sites.component';
import { LaserSitesComponent } from './laser/laser-sites/laser-sites/laser-sites.component';
import { BlackoutComponent } from './blackout/blackout.component';
import { SliderComponent } from './common/slider/slider.component';
import { BlockTypeSwitcherComponent } from './common/block-type-switcher/block-type-switcher.component';
import { DaysComponent } from './common/days/days.component';
import { OngoingScheduleComponent } from './common/ongoing-schedule/ongoing-schedule.component';
import { NameComponent } from './common/name/name.component';
import { HeroPageComponent } from './hero-page/hero-page.component';
import { HeroResolverService } from './services/hero-resolver.service';
import { AlertComponent } from './alert/alert.component';
import { LoadingComponent } from './loading/loading.component';
import { SettingsComponent } from './settings/settings.component';
import { SelectHeroComponent } from './select-hero/select-hero.component';
import { SupportComponent } from './support/support.component';
import { SupportModeComponent } from './support-mode/support-mode.component';
import { SupportExtensionComponent } from './support-extension/support-extension.component';
import { SupportedBrowserComponent } from './supported-browser/supported-browser.component';
import { UpdateAvailableComponent } from './update-available/update-available.component';
import { PrivacyComponent } from './privacy/privacy.component';
import { TrialExpiredComponent } from './trial-expired/trial-expired.component';
import { PremiumActivatedComponent } from './premium-activated/premium-activated.component';
import { PremiumCodeCanceledComponent } from './premium-code-canceled/premium-code-canceled.component';
import { PremiumCodeSuspendedComponent } from './premium-code-suspended/premium-code-suspended.component';
import { SubmitPremiumCodeComponent } from './submit-premium-code/submit-premium-code.component';
import { ReinstallExtensionComponent } from './reinstall-extension/reinstall-extension.component';
import { IntroductionComponent } from './introduction/introduction.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { WelcomePrivacyComponent } from './welcome-privacy/welcome-privacy.component';
import { WelcomeExtensionComponent } from './welcome-extension/welcome-extension.component';
import { UsedInvalidBrowserComponent } from './used-invalid-browser/used-invalid-browser.component';
import { OnboardingCompleteComponent } from './onboarding-complete/onboarding-complete.component';
import { SetupFinishedComponent } from './setup-finished/setup-finished.component';
import { TrialOfferComponent } from './trial-offer/trial-offer.component';
import { TrialExpiringReminderComponent } from './trial-expiring-reminder/trial-expiring-reminder.component';
import { UpgradeComponent } from './upgrade/upgrade.component';


const appRoutes: Routes = [
    { path: 'loading', component: LoadingComponent },
    { path: 'dashboard',  component: DashboardComponent },
    { path: 'blocker', component: BlockerComponent },
    { path: 'laser', component: LaserComponent },
    { path: 'blackout', component: BlackoutComponent },
    { path: 'hero', resolve: { hero: HeroResolverService }, component: HeroPageComponent },
    { path: 'settings', component: SettingsComponent },
    { path: 'select-hero', component: SelectHeroComponent },
    { path: 'privacy', component: PrivacyComponent },
    { path: 'support', component: SupportComponent },
    { path: 'upgrade', component: UpgradeComponent },
    { path: 'support-mode', component: SupportModeComponent },
    { path: 'support-extension', component: SupportExtensionComponent },
    { path: 'supported-browser', component: SupportedBrowserComponent },
    { path: 'update-available', component: UpdateAvailableComponent },
    { path: 'trial-expiring-reminder', component: TrialExpiringReminderComponent },
    { path: 'trial-expired', component: TrialExpiredComponent },
    { path: 'trial-offer', component: TrialOfferComponent },
    { path: 'premium-activated', component: PremiumActivatedComponent },
    { path: 'premium-code-canceled', component: PremiumCodeCanceledComponent },
    { path: 'premium-code-suspended', component: PremiumCodeSuspendedComponent },
    { path: 'submit-premium-code', component: SubmitPremiumCodeComponent },
    { path: 'used-invalid-browser', component: UsedInvalidBrowserComponent },
    { path: 'reinstall-extension', component: ReinstallExtensionComponent},
    { path: 'welcome', component: WelcomeComponent },
    { path: 'welcome-privacy', component: WelcomePrivacyComponent },
    { path: 'welcome-extension', component: WelcomeExtensionComponent },
    { path: 'introduction', component: IntroductionComponent },
    { path: 'setup-finished', component: SetupFinishedComponent },
    { path: 'onboarding-complete', component: OnboardingCompleteComponent },
    { path: '', redirectTo: '/loading', pathMatch: 'full' }
]

@NgModule({
  declarations: [
    AppComponent,
    SiteBarComponent,
    SitesComponent,
    SiteComponent,
    DashboardComponent,
    BlockerComponent,
    BlockerSitesComponent,
    BlockedSitesExceptionsSwitcherComponent,
    BlockedSitesExceptionsComponent,
    BlockerAddSitesComponent,
    LaserComponent,
    LaserAddSitesComponent,
    LaserSitesComponent,
    BlackoutComponent,
    SliderComponent,
    BlockTypeSwitcherComponent,
    DaysComponent,
    OngoingScheduleComponent,
    NameComponent,
    HeroPageComponent,
    AlertComponent,
    LoadingComponent,
    SettingsComponent,
    SelectHeroComponent,
    SupportComponent,
    SupportModeComponent,
    SupportExtensionComponent, 
    SupportedBrowserComponent,
    UpdateAvailableComponent,
    PrivacyComponent,
    TrialExpiredComponent,
    PremiumActivatedComponent,
    PremiumCodeCanceledComponent,
    PremiumCodeSuspendedComponent,
    SubmitPremiumCodeComponent,
    ReinstallExtensionComponent,
    IntroductionComponent,
    WelcomeComponent,
    WelcomePrivacyComponent,
    WelcomeExtensionComponent,
    UsedInvalidBrowserComponent,
    OnboardingCompleteComponent,
    SetupFinishedComponent,
    TrialOfferComponent,
    TrialExpiringReminderComponent,
    UpgradeComponent
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatExpansionModule,
    NgxSpinnerModule,
    AmazingTimePickerModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
