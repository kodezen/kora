import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupportedBrowserComponent } from './supported-browser.component';

describe('SupportedBrowserComponent', () => {
  let component: SupportedBrowserComponent;
  let fixture: ComponentFixture<SupportedBrowserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupportedBrowserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupportedBrowserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
