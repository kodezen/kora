import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { IpcService } from '../services/ipc.service';

@Component({
  selector: 'app-supported-browser',
  templateUrl: './supported-browser.component.html',
  styleUrls: ['./supported-browser.component.css']
})
export class SupportedBrowserComponent implements OnInit, OnDestroy {
  timeout: any;

  constructor(private ipcService: IpcService, private zone: NgZone, private router: Router) { }

  ngOnInit() {
    window.addEventListener('message', this.messageListener);
    this.ipcService.send('detect-chrome');
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      let data = message.data;
      if (message.channel === 'chrome-detected') {
        this.onChromeDetected(data);
      }
    }
  }

  onChromeDetected = (chromeDetected: boolean) => {
    if (chromeDetected) {
      this.zone.run(() => {
        this.router.navigate(['/welcome-extension'], {
          state: { firstBlock: window.history.state.firstBlock }
        });
      });
    } else {
      this.timeout = setTimeout(() => {
        this.ipcService.send('detect-chrome');
      }, 5000);
    }
  }

  installChrome() {
    this.ipcService.send('install-chrome-browser');
  }
}
