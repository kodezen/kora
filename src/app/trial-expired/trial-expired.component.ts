import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { PremiumCodeService } from '../services/premium-code.service';
import { TrialService } from '../services/trial.service';
import { IpcService } from '../services/ipc.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-trial-expired',
  templateUrl: './trial-expired.component.html',
  styleUrls: ['./trial-expired.component.css']
})
export class TrialExpiredComponent implements OnInit, OnDestroy {
  offerExpired: boolean = true;
  offerTimeRemaining: any = {
    hours: 4,
    minutes: 0,
    seconds: 0
  };
  offerSecondsInterval: any;
  code: string = '';
  error: string = '';

  constructor(private trialService: TrialService, private premiumCodeService: PremiumCodeService, private ipcService: IpcService, private router: Router, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    window.addEventListener('message', this.messageListener);
    this.ipcService.send('get-trial-expired-offer-time-remaining');
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
    clearInterval(this.offerSecondsInterval);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      let data = message.data;
      switch (message.channel) {
        case 'trial-expired-offer-time-remaining':
          this.onOfferTimeRemainingResponse(data);
          break;
        case 'event-tracked: trial-expired-activated-code':
          this.router.navigate(['/premium-activated']);
          break;
      }
    }
  };

  onOfferTimeRemainingResponse(offerTimeRemainingInSeconds) {
    this.offerExpired = (offerTimeRemainingInSeconds <= 0);
    if (this.offerExpired) {
      return;
    }
    let timeRemainingDuration = moment.duration(offerTimeRemainingInSeconds, 'seconds');
    this.trialService.updateTrialExpiredOfferTimeRemaining(this.offerTimeRemaining, timeRemainingDuration);
    this.startOfferSecondsInterval();
  }

  startOfferSecondsInterval() {
    this.offerSecondsInterval = setInterval(() => {
      let seconds = parseInt(this.offerTimeRemaining.seconds);
      let timeToSync = (seconds <= 0);
      if (timeToSync) {
        clearInterval(this.offerSecondsInterval);
        this.startOfferTimeSync();
      } else {
        seconds--;
        this.offerTimeRemaining.seconds = this.trialService.format(seconds);
      }
    }, 1000);
  }

  startOfferTimeSync() {
    this.ipcService.send('get-trial-expired-offer-time-remaining');
  }

  onViewPlans() {
    this.ipcService.send('view-kora-plans');
    this.ipcService.send('event: trial-expired-plans-viewed');
  }

  submitCode() {
    if (this.code.length === 0) {
      this.error = 'Please enter a premium code.';
      return;
    }
    this.spinner.show();
    this.code = this.code.trim();
    this.premiumCodeService.submitPremiumCode(this.code).subscribe(result => {
      this.spinner.hide();
      let response = <any> result;
      if (response.type === 'SUCCESS') {
        let validInfo = response.data;
        if (!validInfo.valid) {
          this.error = validInfo.reason;
        } else {
          this.ipcService.send('premium-activated', this.code);
          this.ipcService.send('event: trial-expired-activated-code');
        }
      } else {
        this.error = response.message;
      }
    }, 
    error => {
      this.spinner.hide();
      this.error = 'Internal server error. Please contact support: korablocker@gmail.com';
    });
  }

  onKeydown(event) {
    this.error = '';
    if (event.key === 'Enter') {
      this.submitCode();
    }
  }

}
