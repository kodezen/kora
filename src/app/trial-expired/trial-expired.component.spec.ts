import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrialExpiredComponent } from './trial-expired.component';

describe('TrialExpiredComponent', () => {
  let component: TrialExpiredComponent;
  let fixture: ComponentFixture<TrialExpiredComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrialExpiredComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrialExpiredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
