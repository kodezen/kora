import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root'
})
export class IpcService {
  private FRONT_END: string = 'kora-front-end';
  private BACK_END: string = 'kora-back-end';

  constructor() { }

  get KORA_FRONT_END() {
    return this.FRONT_END;
  }

  get KORA_BACK_END() {
    return this.BACK_END;
  }

  send(channel: string, data?: any) {
    let message: any = {
      type: this.FRONT_END,
      channel: channel
    };
    if (data != null) {
      message.data = data;
    }
    postMessage(message, '*');
  }
}
