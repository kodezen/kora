import { Injectable } from '@angular/core';
import { IpcService } from './ipc.service';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  constructor(private ipcService: IpcService) { }

  removeHero(heroes, hero): void {
    let index = this.getHeroIndex(heroes, hero);
    heroes.splice(index, 1);
  }

  replaceHero(heroes, hero): void {
    let index = this.getHeroIndex(heroes, hero);
    heroes[index] = hero;
  }

  getHeroIndex(heroes, hero): number {
    let id = hero._id;
    for (var i = 0; i < heroes.length; i++) {
      let candidateHero = heroes[i];
      if (candidateHero._id === id) {
        return i;
      }
    }
    return -1;
  }

  getLastActiveHeroIndex(heroes): number {
    let lastActiveHeroIndex = -1;
    for (var i = 0; i < heroes.length; i++) {
      let hero = heroes[i];
      if (hero.status === 'active') {
        lastActiveHeroIndex = i;
      }
    }
    return lastActiveHeroIndex;
  }

  statusEqual(hero1, hero2): boolean {
    return (hero1.status === hero2.status);
  }

  createHero(hero): void {
    this.ipcService.send('create-hero', hero);
  }

  updateHero(hero): void {
    this.ipcService.send('update-hero', hero);
  }
  
  startHero(hero): void {
    this.ipcService.send('start-hero', hero);
  }

  deleteHero(hero): void {
    this.ipcService.send('delete-hero', hero);
  }
}
