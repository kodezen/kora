import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TrialService {

  constructor() { }

  updateTrialTimeRemaining(timeRemainingObj: any, timeRemainingDuration: any) {
    var days = Math.floor(timeRemainingDuration.asDays());
    var hours = Math.floor(timeRemainingDuration.asHours() % 24);
    var minutes = Math.floor(timeRemainingDuration.asMinutes() % 60);
    var seconds = Math.floor(timeRemainingDuration.asSeconds() % 60);
    if (minutes == 60) {
        hours++;
        minutes = 0;
    }
    if (hours == 24) {
        days++;
        hours = 0;
    }
    timeRemainingObj.days = this.format(days);
    timeRemainingObj.hours = this.format(hours);
    timeRemainingObj.minutes = this.format(minutes);
    timeRemainingObj.seconds = this.format(seconds);
  }

  updateTrialExpiredOfferTimeRemaining(timeRemainingObj: any, timeRemainingDuration: any) {
    var hours = Math.floor(timeRemainingDuration.asHours() % 24);
    var minutes = Math.floor(timeRemainingDuration.asMinutes() % 60);
    var seconds = Math.floor(timeRemainingDuration.asSeconds() % 60);
    if (minutes == 60) {
        hours++;
        minutes = 0;
    }
    timeRemainingObj.hours = this.format(hours);
    timeRemainingObj.minutes = this.format(minutes);
    timeRemainingObj.seconds = this.format(seconds);
  }

  format(number) {
    let formattedNumber = '';
    if (number < 10) {
      formattedNumber = '0';
    }
    formattedNumber += number;
    return formattedNumber;
  }
}


