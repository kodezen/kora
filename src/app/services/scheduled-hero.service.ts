import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ScheduledHeroService {

  constructor() { }

  createScheduledDay(dayOfWeek: string) {
    let scheduledDay = {
      dayOfWeek: dayOfWeek,
      startTime: '00:00',
      endTime: '00:00',
      validTimes: true
    }
    return scheduledDay;
  }

  sortScheduledDays(scheduledDays: any) {
    scheduledDays.sort(function(a, b) {
      return parseInt(a.dayOfWeek, 10) - parseInt(b.dayOfWeek, 10);
    });
  }

  removeScheduledDay(scheduledDays: any, dayOfWeek: string) {
    let index = this.getScheduledDayIndex(scheduledDays, dayOfWeek);
    scheduledDays.splice(index, 1);
  }

  getScheduledDayIndex(scheduledDays: any, dayOfWeek: string) {
    for (var i = 0; i < scheduledDays.length; i++) {
      let scheduledDay = scheduledDays[i];
      if (scheduledDay.dayOfWeek === dayOfWeek) {
        return i;
      }
    }
  }

  prepareSchedule(scheduledDays: any) {
    let blockDays = {};
    for (var i = 0; i < scheduledDays.length; i++) {
      let scheduledDay = scheduledDays[i];
      let dayOfWeek = scheduledDay.dayOfWeek;
      let blockDay = this.createBlockDay(scheduledDay);
      blockDays[dayOfWeek] = blockDay;
    }
    let schedule = {
      blockDays: blockDays,
      validSchedule: true
    }
    return schedule;
  }

  createBlockDay(scheduledDay: any) {
    let blockDay = {
      dayOfWeek: scheduledDay.dayOfWeek,
      startTime: scheduledDay.startTime,
      endTime: scheduledDay.endTime,
      validTimes: scheduledDay.validTimes
    }
    return blockDay;
  }
}
