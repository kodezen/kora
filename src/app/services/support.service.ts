import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SupportService {

  supportModeOn: boolean = false;

  constructor() { }

  getSupportModeOn() {
    return this.supportModeOn;
  }

  turnOnSupportMode() {
    this.supportModeOn = true;
    setTimeout(() => { this.supportModeOn = false; }, 15 * 60000);
  }

}
