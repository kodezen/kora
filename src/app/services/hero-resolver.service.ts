import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { HeroDataService } from './hero-data.service';


@Injectable({
  providedIn: 'root'
})
export class HeroResolverService implements Resolve<any> {

  constructor(private heroDataService: HeroDataService, private router: Router) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<any> | Observable<never> {
    let hero = this.heroDataService.getHero();
    if (hero) {
      return of(hero);
    } else {
      this.router.navigate(['/dashaboard']);
      return EMPTY;
    }
  }
  
}
