import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PremiumCodeService {

  constructor(private http: HttpClient) { }

  submitPremiumCode(code: string) {
    let body = {
      code: code
    };
    return this.http.post('https://evening-scrubland-10274.herokuapp.com/premium/submit-code', body);
  }
}
