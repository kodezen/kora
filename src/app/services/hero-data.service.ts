import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeroDataService {

  hero: any;

  constructor() { }

  setHero(hero: any): void {
    this.hero = hero;
    localStorage.setItem('hero', JSON.stringify(hero));
  }

  getHero(): any {
    if (!this.hero) {
      this.hero = JSON.parse(localStorage.getItem('hero'));
    }
    return this.hero;
  }
}
