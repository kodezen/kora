import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { HeroService } from '../services/hero.service';
import { IpcService } from '../services/ipc.service';

@Component({
  selector: 'app-onboarding-complete',
  templateUrl: './onboarding-complete.component.html',
  styleUrls: ['./onboarding-complete.component.css']
})
export class OnboardingCompleteComponent implements OnInit, OnDestroy {

  constructor(private heroService: HeroService, private ipcService: IpcService, private router: Router, private zone: NgZone) { }

  ngOnInit() {
    window.addEventListener('message', this.messageListener);
    // on video end listener
    document.getElementById('video').addEventListener('ended', (e) => {
      this.ipcService.send('event: onboarding-video-finished');
      this.ipcService.send('event: onboarding-finished');
    }, false);
    // handle first block creation
    if (window.history.state.firstBlock) {
      this.heroService.createHero(window.history.state.firstBlock);
      this.ipcService.send('event: first-block-created');
    }
    // initiate navigation after 4 minutes in case they don't watch the video
    setTimeout(() => {
      this.ipcService.send('event: onboarding-finished');
    }, 4 * 60000)
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      let data = message.data;
      switch (message.channel) {
        case 'event-tracked: onboarding-finished':
          this.ipcService.send('show-window');
          this.router.navigate(['/dashboard']);
          break;
      }
    }
  };
}