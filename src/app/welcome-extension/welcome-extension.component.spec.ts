import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WelcomeExtensionComponent } from './welcome-extension.component';

describe('WelcomeExtensionComponent', () => {
  let component: WelcomeExtensionComponent;
  let fixture: ComponentFixture<WelcomeExtensionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WelcomeExtensionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeExtensionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
