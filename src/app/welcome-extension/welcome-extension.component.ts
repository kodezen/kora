import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { IpcService } from '../services/ipc.service';
import { HeroService } from '../services/hero.service';

@Component({
  selector: 'app-welcome-extension',
  templateUrl: './welcome-extension.component.html',
  styleUrls: ['./welcome-extension.component.css']
})
export class WelcomeExtensionComponent implements OnInit, OnDestroy {
  setupInterval: any;

  constructor(private zone: NgZone, private heroService: HeroService, private ipcService: IpcService, private router: Router) { }

  ngOnInit() {
    window.addEventListener('message', this.messageListener);
    this.ipcService.send('event: extension-setup-started');
    this.ipcService.send('check-chrome-extension-connected');
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      let data = message.data;
      switch (message.channel) {
        case 'chrome-extension-setup-complete':
          this.onChromeExtensionSetupComplete();
          break;
        case 'chrome-extension-connected':
          this.onChromeExtensionSetupComplete();
          break;
        case 'event-tracked: extension-setup-finished':
          this.onExtensionSetupFinished();
          break;
      }
    }
  };

  installChromeExtension() {
    this.ipcService.send('install-chrome-extension');
  }

  onChromeExtensionSetupComplete() {
    this.ipcService.send('event: extension-setup-finished');
  }

  onExtensionSetupFinished() {
    this.router.navigate(['/setup-finished'], {
      state: { firstBlock: window.history.state.firstBlock }
    });
  }
}