import { Component, OnInit, OnDestroy } from '@angular/core';
import { IpcService } from '../services/ipc.service';


@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit, OnDestroy {
  freeTrialUser: boolean = false;

  constructor(private ipcService: IpcService) { }

  ngOnInit() {
    window.addEventListener('message', this.messageListener);
    this.ipcService.send('free-trial-user-query');
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      let data = message.data;
      switch (message.channel) {
        case 'free-trial-user':
          this.freeTrialUser = data.freeTrialUser;
          break;
      }
    }
  };

}
