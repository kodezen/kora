import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { SupportService } from '../services/support.service';
import { IpcService } from '../services/ipc.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.css']
})
export class SupportComponent implements OnInit, OnDestroy {

  code: string;
  error: string = '';

  constructor(private supportService: SupportService, private ipcService: IpcService, private router: Router, private zone: NgZone, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    window.addEventListener('message', this.messageListener);
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      let data = message.data;
      if (message.channel === 'support-code-checked') {
        this.onSupportCodeChecked(data);
      }
    }
  }

  onCodeSubmit() {
    if (this.code.length > 0) {
      this.spinner.show();
      this.ipcService.send('support-code', this.code);
    } else {
      this.error = 'Please enter a support code.';
    }
  }

  onSupportCodeChecked = (validCode: boolean) => {
    this.zone.run(() => {
      this.spinner.hide();
      if (validCode) {
        this.supportService.turnOnSupportMode();
        this.router.navigate(['/support-mode']);
      } else {
        this.error = 'Invaild support code.';
      }
    });
  }

}
