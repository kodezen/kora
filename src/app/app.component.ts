import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { fadeAnimation } from './animations';
import { IpcService } from './services/ipc.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [fadeAnimation]
})
export class AppComponent implements OnInit, OnDestroy {
  
  constructor(private ipcService: IpcService, private router: Router) {}

  ngOnInit() {
    window.addEventListener('message', this.messageListener);
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) { 
      switch (message.channel) {
        case 'update-available':
          this.ipcService.send('update-available-received');
          this.router.navigate(['/update-available']);
          break;
        case 'used-invalid-browser':
          this.router.navigate(['/used-invalid-browser']);
          break;
        case 'reinstall-extension':
          this.router.navigate(['/reinstall-extension']);
          break;
        case 'show-trial-expiring-reminder':
          this.ipcService.send('show-trial-expiring-reminder-received');
          this.router.navigate(['/trial-expiring-reminder']);
          break;
        case 'trial-expired':
          this.ipcService.send('trial-expired-received');
          this.router.navigate(['/trial-expired']);
          break;
        case 'premium-code-inactive':
          this.ipcService.send('premium-code-inactive-received');
          let validInfo = message.data;
          var route;
          switch (validInfo.status) {
            case 'suspended':
              route = 'premium-code-suspended';
              break;
            case 'canceled':
              route = 'premium-code-canceled';
              break;
            case 'refunded':
              route = 'premium-code-canceled';
              break;
          }
          this.router.navigate([route]);
          break;
      }
    }
  };
}
