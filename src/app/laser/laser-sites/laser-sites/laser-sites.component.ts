import { Component, OnInit, OnChanges, SimpleChanges, Input, ChangeDetectorRef } from '@angular/core';

@Component({
  selector: 'app-laser-sites',
  templateUrl: './laser-sites.component.html',
  styleUrls: ['./laser-sites.component.css', '../../../common/hero.css']
})
export class LaserSitesComponent implements OnInit, OnChanges {

  @Input() exceptions: string[];
  @Input() editing: boolean;
  @Input() exceptionsLocked: boolean;

  constructor(private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
  }

  ngOnChanges(SimpleChanges: SimpleChanges) {
    this.cdRef.detectChanges();
  }

  addException(site: string) {
    this.exceptions.push(site);
  }

  removeException(site: string) {
    var index = this.exceptions.indexOf(site);
    if (index !== -1) {
      this.exceptions.splice(index, 1);
    }
  }

}
