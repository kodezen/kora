import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-laser-add-sites',
  templateUrl: './laser-add-sites.component.html',
  styleUrls: ['./laser-add-sites.component.css', '../../../common/hero.css']
})
export class LaserAddSitesComponent implements OnInit {

  @Output() exceptionAdded: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit() {
  }

  addException(site) {
    this.exceptionAdded.emit(site);
  }

}
