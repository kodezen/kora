import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitPremiumCodeComponent } from './submit-premium-code.component';

describe('SubmitPremiumCodeComponent', () => {
  let component: SubmitPremiumCodeComponent;
  let fixture: ComponentFixture<SubmitPremiumCodeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SubmitPremiumCodeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitPremiumCodeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
