import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PremiumCodeService } from '../services/premium-code.service';
import { IpcService } from '../services/ipc.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-submit-premium-code',
  templateUrl: './submit-premium-code.component.html',
  styleUrls: ['./submit-premium-code.component.css']
})
export class SubmitPremiumCodeComponent implements OnInit {

  code: string = '';
  error: string = '';

  constructor(private premiumCodeService: PremiumCodeService, private ipcService: IpcService, private router: Router, private spinner: NgxSpinnerService) { }

  ngOnInit() {
  }

  onViewPlans() {
    this.ipcService.send('view-kora-plans');
  }

  submitCode() {
    if (this.code.length === 0) {
      this.error = 'Please enter a premium code.';
      return;
    }
    this.spinner.show();
    this.code = this.code.trim();
    this.premiumCodeService.submitPremiumCode(this.code).subscribe(result => {
      this.spinner.hide();
      let response = <any> result;
      if (response.type === 'SUCCESS') {
        let validInfo = response.data;
        if (!validInfo.valid) {
          this.error = validInfo.reason;
        } else {
          this.ipcService.send('premium-activated', this.code);
          this.router.navigate(['/premium-activated']);
        }
      } else {
        this.error = response.message;
      }
    },  
    error => {
      this.spinner.hide();
      this.error = 'Internal server error. Please contact support: korablocker@gmail.com';
    });
  }

  onKeydown(event) {
    this.error = '';
    if (event.key === 'Enter') {
      this.submitCode();
    }
  }

}
