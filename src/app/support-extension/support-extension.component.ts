import { Component, OnInit, OnDestroy, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { IpcService } from '../services/ipc.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-support-extension',
  templateUrl: './support-extension.component.html',
  styleUrls: ['./support-extension.component.css']
})
export class SupportExtensionComponent implements OnInit, OnDestroy {

  code: string;
  error: string = '';

  constructor(private ipcService: IpcService, private router: Router, private zone: NgZone, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    window.addEventListener('message', this.messageListener);
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      let data = message.data;
      if (message.channel === 'extension-support-code-checked') {
        this.onSupportCodeChecked(data);
      }
    }
  }

  onCodeSubmit() {
    if (this.code.length > 0) {
      this.spinner.show();
      this.ipcService.send('extension-support-code', this.code);
    } else {
      this.error = 'Please enter a support code.';
    }
  }

  onSupportCodeChecked = (validCode: boolean) => {
    this.zone.run(() => {
      this.spinner.hide();
      if (validCode) {
        this.router.navigate(['/dashboard']);
      } else {
        this.error = 'Invaild support code.';
      }
    });
  }

}
