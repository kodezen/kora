import { Component, OnInit, OnDestroy, ChangeDetectorRef, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { IpcService } from '../services/ipc.service';
import { HeroService } from '../services/hero.service';
import { HeroDataService } from '../services/hero-data.service';
import { TrialService } from '../services/trial.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit, OnDestroy {
  freeTrialUser: boolean = false;
  freeTrialActive: boolean = false;
  displayTrialOffer: boolean = false;
  heroes: any = [];
  oneTimeHeroes: any = [];
  ongoingHeroes: any = [];
  trialTimeRemaining: any = {
    days: 7,
    hours: 0,
    minutes: 0,
    seconds: 0
  };
  trialSecondsInterval: any;

  constructor(private heroService: HeroService,
    private heroDataService: HeroDataService, private trialService: TrialService, private ipcService: IpcService, private router: Router, private zone: NgZone, private cdRef: ChangeDetectorRef) { }

  ngOnInit() {
    window.addEventListener('message', this.messageListener);
    this.ipcService.send('get-heroes');
    this.ipcService.send('free-trial-user-query');
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
    clearInterval(this.trialSecondsInterval);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      let data = message.data;
      switch (message.channel) {
        case 'free-trial-user':
          this.onFreeTrialUserResponse(data);
          break;
        case 'trial-time-remaining':
          this.onTrialTimeSyncResponse(data);
          break;
        case 'heroes':
          this.populateHeroes(data);
          break;
        case 'one-time-heroes-updated':
          this.updateOneTimeHeroes(data);
          break;
        case 'ongoing-heroes-updated':
          this.updateOngoingHeroes(data);
          break;
        case 'hero-updated':
          this.onHeroUpdate(data);
          break;
        case 'event-tracked: trial-offer-clicked':
          this.router.navigate(['/trial-offer']);
          break;
      }
    }
  };

  onFreeTrialUserResponse(freeTrialUserInfo) {
    this.freeTrialUser = freeTrialUserInfo.freeTrialUser;
    this.freeTrialActive = freeTrialUserInfo.freeTrialActive;
    if (this.freeTrialUser && this.freeTrialActive) {
      if (window.history.state.firstBlock) {
        setTimeout(() => {
          this.displayTrialOffer = true;
          this.startTrialTimeSync();
        }, 2 * 60000);
      } else {
          this.displayTrialOffer = true;
          this.startTrialTimeSync();
      }
    }
  }

  startTrialTimeSync() {
    this.ipcService.send('get-trial-time-remaining');
  }
  
  onTrialTimeSyncResponse(trialTimeRemainingSeconds) {
    let trialExpired = (trialTimeRemainingSeconds <= 0);
    if (trialExpired) {
      this.freeTrialActive = false;
      this.ipcService.send('trial-expired');
      return;
    } 
    let timeRemainingDuration = moment.duration(trialTimeRemainingSeconds, 'seconds');
    this.trialService.updateTrialTimeRemaining(this.trialTimeRemaining, timeRemainingDuration);
    this.startTrialSecondsInterval();
  }

  startTrialSecondsInterval() {
    this.trialSecondsInterval = setInterval(() => {
      let seconds = parseInt(this.trialTimeRemaining.seconds);
      let timeToSync = (seconds <= 0);
      if (timeToSync) {
        clearInterval(this.trialSecondsInterval);
        this.startTrialTimeSync();
      } else {
        seconds--;
        this.trialTimeRemaining.seconds = this.trialService.format(seconds);
      }
    }, 1000);
  }

  populateHeroes = (heroes: any) => {
    this.heroes.push(...heroes);
    for (var i = 0; i < heroes.length; i++) {
      let hero = heroes[i];
      if (hero.blockType === 'one-time') {
        this.oneTimeHeroes.push(hero);
      } else if (hero.blockType === 'ongoing') {
        this.ongoingHeroes.push(hero);
      }
    }
    if (!this.cdRef['destroyed']) {
      this.cdRef.detectChanges();
    }
  }

  updateOneTimeHeroes = (heroes: any) => {
    // special case of updating sending before get all heroes returning, ignore call or get duplicates
    if (this.heroes.length == 0) {
      return;
    }
    for (var i = 0; i < heroes.length; i++) {
      let hero = heroes[i];
      let newIndex = i;
      let oldIndex = this.heroService.getHeroIndex(this.oneTimeHeroes, hero);
      let positionChanged = (newIndex != oldIndex);
      if (positionChanged) {
        let heroToSwapWith = this.oneTimeHeroes[newIndex];
        this.oneTimeHeroes[newIndex] = hero;
        this.oneTimeHeroes[oldIndex] = heroToSwapWith;
      } else {
        this.oneTimeHeroes[newIndex] = hero;
      }
    }
    if (!this.cdRef['destroyed']) {
      this.cdRef.detectChanges();
    }
  }

  updateOngoingHeroes = (heroes: any) => {
    // special case of updating sending before get all heroes returning, ignore call or get duplicates
    if (this.heroes.length == 0) {
      return;
    }
    for (var i = 0; i < heroes.length; i++) {
      let hero = heroes[i];
      let newIndex = i;
      let oldIndex = this.heroService.getHeroIndex(this.ongoingHeroes, hero);
      let positionChanged = (newIndex != oldIndex);
      if (positionChanged) {
        let heroToSwapWith = this.ongoingHeroes[newIndex];
        this.ongoingHeroes[newIndex] = hero;
        this.ongoingHeroes[oldIndex] = heroToSwapWith;
      } else {
        this.ongoingHeroes[newIndex] = hero;
      }
    }
    if (!this.cdRef['destroyed']) {
      this.cdRef.detectChanges();
    }
  }

  onHeroUpdate = (updatedHero: any) => {
    var heroesList;
    if (updatedHero.blockType === 'one-time') {
      heroesList = this.oneTimeHeroes;
    } else if (updatedHero.blockType === 'ongoing') {
      heroesList = this.ongoingHeroes;
    }
    this.heroService.removeHero(heroesList, updatedHero);
    let indexToInsertAt = this.heroService.getLastActiveHeroIndex(heroesList) + 1;
    heroesList.splice(indexToInsertAt, 0, updatedHero);
    if (!this.cdRef['destroyed']) {
      this.cdRef.detectChanges();
    }
  }

  onHeroClicked(hero) {
    this.heroDataService.setHero(hero);
    this.zone.run(() => {
      this.router.navigate(['/hero']);
    })
  }

  onTrialOfferClick() {
    this.ipcService.send('event: trial-offer-clicked');
  }
}
