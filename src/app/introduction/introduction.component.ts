import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { IpcService } from '../services/ipc.service';

@Component({
  selector: 'app-introduction',
  templateUrl: './introduction.component.html',
  styleUrls: ['./introduction.component.css']
})
export class IntroductionComponent implements OnInit {

  constructor(private ipcService: IpcService, private router: Router) { }

  ngOnInit() {
    setTimeout(() => {
      this.router.navigate(['/blocker'], {
        state: { firstBlock: true }
      });
    }, 18000);
  }

}
