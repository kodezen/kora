import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { PremiumCodeService } from '../services/premium-code.service';
import { IpcService } from '../services/ipc.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { TrialService } from '../services/trial.service';

@Component({
  selector: 'app-trial-offer',
  templateUrl: './trial-offer.component.html',
  styleUrls: ['./trial-offer.component.css']
})
export class TrialOfferComponent implements OnInit, OnDestroy {
  trialTimeRemaining: any = {
    days: 7,
    hours: 0,
    minutes: 0,
    seconds: 0
  };
  trialSecondsInterval: any;
  code: string = '';
  error: string = '';

  constructor(private trialService: TrialService, private premiumCodeService: PremiumCodeService, private ipcService: IpcService, private router: Router, private spinner: NgxSpinnerService) { }

  ngOnInit() {
    window.addEventListener('message', this.messageListener);
    this.startTrialTimeSync();
  }

  ngOnDestroy() {
    window.removeEventListener('message', this.messageListener);
    clearInterval(this.trialSecondsInterval);
  }

  messageListener = ({ data }) => {
    let message = data;
    let messageFromBackEnd = (message.type === this.ipcService.KORA_BACK_END);
    if (messageFromBackEnd) {
      let data = message.data;
      switch (message.channel) {
        case 'trial-time-remaining':
          this.onTrialTimeSyncResponse(data);
          break;
        case 'event-tracked: trial-offer-activated-code':
          this.router.navigate(['/premium-activated']);
          break;
      }
    }
  };

  startTrialTimeSync() {
    this.ipcService.send('get-trial-time-remaining');
  }

  onTrialTimeSyncResponse(trialTimeRemainingSeconds) {
    let trialExpired = (trialTimeRemainingSeconds <= 0);
    if (trialExpired) {
      this.ipcService.send('trial-expired');
      return;
    } 
    let timeRemainingDuration = moment.duration(trialTimeRemainingSeconds, 'seconds');
    this.trialService.updateTrialTimeRemaining(this.trialTimeRemaining, timeRemainingDuration);
    this.startTrialSecondsInterval();
  }

  startTrialSecondsInterval() {
    this.trialSecondsInterval = setInterval(() => {
      let seconds = parseInt(this.trialTimeRemaining.seconds);
      let timeToSync = (seconds <= 0);
      if (timeToSync) {
        clearInterval(this.trialSecondsInterval);
        this.startTrialTimeSync();
      } else {
        seconds--;
        this.trialTimeRemaining.seconds = this.trialService.format(seconds);
      }
    }, 1000);
  }

  onViewPlans() {
    this.ipcService.send('view-kora-plans');
    this.ipcService.send('event: trial-offer-plans-viewed');
  }

  submitCode() {
    if (this.code.length === 0) {
      this.error = 'Please enter a premium code.';
      return;
    }
    this.spinner.show();
    this.code = this.code.trim();
    this.premiumCodeService.submitPremiumCode(this.code).subscribe(result => {
      this.spinner.hide();
      let response = <any> result;
      if (response.type === 'SUCCESS') {
        let validInfo = response.data;
        if (!validInfo.valid) {
          this.error = validInfo.reason;
        } else {
          this.ipcService.send('premium-activated', this.code);
          this.ipcService.send('event: trial-offer-activated-code');
        }
      } else {
        this.error = response.message;
      }
    }, 
    error => {
      this.spinner.hide();
      this.error = 'Internal server error. Please contact support: korablocker@gmail.com';
    });
  }

  onKeydown(event) {
    this.error = '';
    if (event.key === 'Enter') {
      this.submitCode();
    }
  }

}
