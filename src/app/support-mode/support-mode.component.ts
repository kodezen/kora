import { Component, OnInit } from '@angular/core';
import { SupportService } from '../services/support.service';

@Component({
  selector: 'app-support-mode',
  templateUrl: './support-mode.component.html',
  styleUrls: ['./support-mode.component.css']
})
export class SupportModeComponent implements OnInit {

  constructor(private supportService: SupportService) { }

  ngOnInit() {
    this.supportService.turnOnSupportMode();
  }

}
