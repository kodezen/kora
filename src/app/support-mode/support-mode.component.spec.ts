import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupportModeComponent } from './support-mode.component';

describe('SupportModeComponent', () => {
  let component: SupportModeComponent;
  let fixture: ComponentFixture<SupportModeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupportModeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupportModeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
