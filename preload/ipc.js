const { ipcRenderer } = require('electron');

window.addEventListener('message', ({ data }) => {
    let message = data;
    let messageFromFrontEnd = (message.type === 'kora-front-end');
    if (messageFromFrontEnd) {
        ipcRenderer.send(message.channel, message.data);
    }
});

ipcRenderer.on('kora-back-end', (event, backEndMessage) => {
    let message = {
        type: 'kora-back-end',
        channel: backEndMessage.channel,
        data: backEndMessage.data
    }
    postMessage(message, '*');
});
