var heroService = require('../services/hero-service.js');
var moment = require('moment');
const log = require('electron-log');

var activeHeroes = [];
var scheduledHeroes = [];
var restingHeroes = [];

function populateCache(heroes, now) {
    sortHeroes(heroes, now);
}

function reSortHeroes(now) {
    unmarkHeroes();
    let heroes = [];
    heroes.push(...activeHeroes);
    heroes.push(...scheduledHeroes);
    heroes.push(...restingHeroes);
    clearCache();
    sortHeroes(heroes, now);
}

function sortHeroes(heroes, now) {
    try {
        now.seconds(0);
        now.milliseconds(0);
        log.info('Populating block cache: ' + now.format('HH:mm:ss'));
        for (var i = 0; i < heroes.length; i++) {
            let hero = heroes[i];
            let heroIsActive = heroService.isHeroActive(hero, now);
            if (heroIsActive) {
                log.info('Active block: ' + hero.name);
                addActiveHero(hero, now);
            } else {
                let heroIsScheduled = heroService.isHeroScheduled(hero, now);
                if (heroIsScheduled) {
                    log.info('Scheduled block: ' + hero.name);
                    addScheduledHero(hero, now);
                } else {
                    log.info('Resting block: ' + hero.name);
                    addRestingHero(hero);
                }
            }
        }
    } catch (err) {
        log.error(err);
    }
}

function getHeroes() {
    let heroes = [];
    heroes.push(...activeHeroes);
    heroes.push(...scheduledHeroes);
    heroes.push(...restingHeroes);
    return heroes;
}

function deleteHero(hero) {
    let heroIndex = heroService.getHeroIndex(activeHeroes, hero);
    if (heroIndex != -1)  {
        activeHeroes.splice(heroIndex, 1);
        return;
    }
    heroIndex = heroService.getHeroIndex(scheduledHeroes, hero);
    if (heroIndex != -1) {
        scheduledHeroes.splice(heroIndex, 1);
        return;
    }
    heroIndex = heroService.getHeroIndex(restingHeroes, hero);
    restingHeroes.splice(heroIndex, 1);
}

function getActiveHeroes() {
    return activeHeroes;
}

function getRestingHeroes() {
    return restingHeroes;
}

function getScheduledHeroes() {
    return scheduledHeroes;
}

function addHero(hero) {
    let now = moment();
    let heroIsActive = heroService.isHeroActive(hero, now);
    if (heroIsActive) {
        addActiveHero(hero, now);
    } else {
        let heroIsScheduled = heroService.isHeroScheduled(hero, now);
        if (heroIsScheduled) {
            addScheduledHero(hero, now);
        } else {
            addRestingHero(hero);
        }
    }
}

function addActiveHero(hero, now) {
    heroService.markHeroActive(hero, now);
    activeHeroes.push(hero);
    heroService.sendBlockActiveEvent(hero);
}

function removeActiveHero(heroToRemove) {
    heroService.unmarkHeroActive(heroToRemove);
    for (var i = 0; i < activeHeroes.length; i++) {
        let hero = activeHeroes[i];
        if (hero.name === heroToRemove.name) {
            activeHeroes.splice(i, 1);
            return;
        }
    }
}

function addScheduledHero(hero, now) {
    heroService.markHeroScheduled(hero, now);
    scheduledHeroes.push(hero);
}

function removeScheduledHero(heroToRemove) {
    heroService.unmarkHeroScheduled(heroToRemove);
    for (var i = 0; i < scheduledHeroes.length; i++) {
        let hero = scheduledHeroes[i];
        if (hero.name === heroToRemove.name) {
            scheduledHeroes.splice(i, 1);
            return;
        }
    }
}

function removeRestingHero(heroToRemove) {
    heroService.unmarkHeroResting(heroToRemove);
    for (var i = 0; i < restingHeroes.length; i++) {
        let hero = restingHeroes[i];
        if (hero.name === heroToRemove.name) {
            restingHeroes.splice(i, 1);
            return;
        }
    }
}

function addRestingHero(hero) {
    heroService.markHeroResting(hero);
    restingHeroes.push(hero);
}

function clearCache() {
    activeHeroes.length = 0;
    scheduledHeroes.length = 0;
    restingHeroes.length = 0;
}

function unmarkHeroes() {
    unmarkActiveHeroes();
    unmarkScheduledHeroes();
    unmarkRestingHeroes();
}

function unmarkActiveHeroes() {
    for (var i = 0; i < activeHeroes.length; i++) {
        let hero = activeHeroes[i];
        heroService.unmarkHeroActive(hero);
    }
}

function unmarkScheduledHeroes() {
    for (var i = 0; i < scheduledHeroes.length; i++) {
        let hero = scheduledHeroes[i];
        heroService.unmarkHeroScheduled(hero);
    }
}

function unmarkRestingHeroes() {
    for (var i = 0; i < restingHeroes.length; i++) {
        let hero = restingHeroes[i];
        heroService.unmarkHeroResting(hero);
    }
}


exports.populateCache = populateCache;
exports.reSortHeroes = reSortHeroes;
exports.getHeroes = getHeroes;
exports.deleteHero = deleteHero;
exports.getActiveHeroes = getActiveHeroes;
exports.getRestingHeroes = getRestingHeroes;
exports.getScheduledHeroes = getScheduledHeroes;
exports.addHero = addHero;
exports.addActiveHero = addActiveHero;
exports.removeActiveHero = removeActiveHero;
exports.addScheduledHero = addScheduledHero;
exports.removeScheduledHero = removeScheduledHero;
exports.removeRestingHero = removeRestingHero;
exports.addRestingHero = addRestingHero;
exports.clearCache = clearCache;
