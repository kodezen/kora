var timeSaved;

function populateCache(timeSavedDoc) {
    timeSaved = timeSavedDoc;
}

function getTimeSaved() {
    return timeSaved;
}

function setTimeSaved(updatedTimeSaved) {
    timeSaved = updatedTimeSaved;
}

exports.populateCache = populateCache;
exports.getTimeSaved = getTimeSaved;
exports.setTimeSaved = setTimeSaved;