var crypto = require('crypto');
var Datastore = require('nedb');
const appDataPaths = require('../services/app-data-paths.js');
const log = require('electron-log');

const filePath = appDataPaths.databaseFilePath;
const encryption = {
    algorithm: 'aes-256-cbc',
    key: 'cD4pBZGnVOcVRjbT9lxOvAV8biVs57bd',
    iv: '22RiW3UwAypKpfFf'
};

var db;

function get() {
    return db;
}

function connect() {
    try {
        db = new Datastore({
            filename: filePath,
            autoload: true,
            afterSerialization: function (doc) {
                var encryptedDoc = doc;
                var cipher = crypto.createCipheriv(encryption.algorithm, encryption.key, encryption.iv);
                encryptedDoc = cipher.update(JSON.stringify(doc), 'utf8', 'hex') + cipher.final('hex');
                return encryptedDoc;
            },
            beforeDeserialization: function (doc) {
                var decryptedDoc = doc;
                var decipher = crypto.createDecipheriv(encryption.algorithm, encryption.key, encryption.iv);
                decryptedDoc = decipher.update(doc, 'hex', 'utf8') + decipher.final('utf8');
                return JSON.parse(decryptedDoc);
            }
        });
    } catch (err) {
        log.error(err);
    }
} 

function get(filter, callback) {
    db.find(filter, (err, docs) => {
        callback(err, docs);
    });
}

function getOne(filter, callback) {
    db.findOne(filter, (err, doc) => {
        callback(err, doc);
    });
}

function update(filter, newData, callback) {
    db.update(filter, newData, { upsert: true }, (err, numReplaced) => {
        callback(err, numReplaced);
    });
}

function insert(dataToInsert, callback) {
    db.insert(dataToInsert, (err, newDoc) => {
        callback(err, newDoc);
    });
}

function deleteOne(filter, callback) {
    db.remove(filter, (err, numRemoved) => {
        callback(err, numRemoved);
    });
}

function deleteAll(filter, callback) {
    db.remove(filter, {multi: true}, (err, numRemoved) => {
        callback(err, numRemoved);
    });
}

exports.get = get;
exports.getOne = getOne;
exports.update = update;
exports.insert = insert;
exports.deleteOne = deleteOne;
exports.deleteAll = deleteAll;
exports.connect = connect;