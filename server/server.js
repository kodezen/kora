const app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
const log = require('electron-log');
var heroCache = require('../database/hero-cache.js');
let userService = require('../services/user-service.js');
const extensionService = require('../services/extension-service.js');
const extensionBypassService = require('../services/extension-bypass-service.js');
const eventService = require('../services/event-service.js');
const ipcService = require('../services/ipc-service.js');
let electronWindow = require('../electron-win.js');
let safeSites = ['trykora.com', 'google.com/chrome', 'chrome.google.com/webstore', 'mozilla.org', 'addons.mozilla.org'];
let activeSocketConnections = [];

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', (socket) => {
    activeSocketConnections.push(socket);
    console.log('Added connection:', activeSocketConnections.length);
    extensionService.setExtensionConnected(true);

    socket.on('extension-version', (versionObj) => {
        console.log(versionObj);
        extensionService.setExtensionVersion(versionObj.version);
    });

    socket.on('block-query', (tabIdUrlPair) => {
        let response = isSiteBlocked(tabIdUrlPair.url);
        if (response.blocked) {
            eventService.track(eventService.WEBSITE_BLOCKED, {
                blockType: response.blockType
            });
        }
        response.tabId = tabIdUrlPair.tabId;
        io.emit('block-query-response', response);
    });

    socket.on('chrome-incognito-enabled', (msgFromClient) => {
        console.log('chrome-incognito-enabled');
        if (msgFromClient === 'chrome-incognito-enabled') {
            if (!userService.isChromeSetupCompleted()) {
                userService.storeChromeSetupCompleted(true);
                electronWindow.show();
                ipcService.send('chrome-extension-setup-complete');
            }
            // TODO: only uncomment after fixing extension bypass user experience
            // if (extensionBypassService.isReinstallingExtension()) {
            //     extensionBypassService.onExtensionReinstalled();
            // }
        }
    });

    socket.on('chrome-incognito-disabled', (msgFromClient) => {
        console.log('chrome-incognito-disabled');
        // TODO: only uncomment after fixing extension bypass user experience
        // if (userService.isChromeSetupCompleted()) {
        //     extensionBypassService.onIncognitoDisabled();
        // }
    });

    socket.on('disconnect', function (data) {
        log.info('socket disconnect: ', data);
        let disregardedDisconnect = (data === 'client error' || data === 'ping timeout');
        let indexToRemove = activeSocketConnections.indexOf(socket);
        if (indexToRemove > -1) {
            activeSocketConnections.splice(indexToRemove, 1);
            console.log('Removed connection:', activeSocketConnections.length);
        }
        let extensionDisconnected = (!disregardedDisconnect && activeSocketConnections.length === 0);
        if (extensionDisconnected) {
            extensionService.setExtensionConnected(false);
        }
    });
});

function isSiteBlocked(visitedUrl) {
    let response = {
        blocked: false
    };
    let isSafeSite = safeSiteCheck(visitedUrl);
    if (isSafeSite) {
        return response;
    }
    let activeHeroes = heroCache.getActiveHeroes();
    for (var i = 0; i < activeHeroes.length; i++) {
        let hero = activeHeroes[i];
        let blocked = doesHeroBlockSite(hero, visitedUrl);
        if (blocked) {
            response.blocked = true;
            response.blockType = hero.heroType;
            console.log('BLOCKED under: ' + hero.name);
            return response;
        }
    }
    return response;
}

function doesHeroBlockSite(hero, visitedUrl) {
    var blocked = false;
    switch (hero.heroType) {
        case 'blocker':
            blocked = doesBlockerBlockSite(hero, visitedUrl);
            break;
        case 'laser':
            blocked = doesLaserBlockSite(hero, visitedUrl);
            break;
        case 'blackout':
            blocked = doesBlackoutBlockSite(hero, visitedUrl);
            break;
    }
    return blocked;
}

function safeSiteCheck(visitedUrl) {
    let visitedHost = extractHostSansExtension(visitedUrl);
    for (var i = 0; i < safeSites.length; i++) {
        let safeSiteUrl = safeSites[i];
        let safeSiteHost = extractHostSansExtension(safeSiteUrl);
        let hostsMatch = doHostsMatch(visitedHost, safeSiteHost);
        if (hostsMatch) {
            let isSafeSite = includesUrl(visitedUrl, safeSiteUrl);
            if (isSafeSite) {
                return true;
            }
        }
    }
    return false;
}

function doesBlockerBlockSite(hero, visitedUrl) {
    let visitedHost = extractHostSansExtension(visitedUrl);
    let blockedSites = hero.blockedSites;
    for (var i = 0; i < blockedSites.length; i++) {
        let blockedUrl = blockedSites[i];
        let blockedHost = extractHostSansExtension(blockedUrl);
        let hostsMatch = doHostsMatch(visitedHost, blockedHost);
        if (hostsMatch) {
            let possibleBlock = includesUrl(visitedUrl, blockedUrl);
            if (possibleBlock) {
                let blocked = !isException(hero, visitedUrl);
                if (blocked) {
                    return true;
                }
            }
        }
    }
    return false;
}

function doesLaserBlockSite(hero, visitedUrl) {
    let blocked = !isException(hero, visitedUrl);
    return blocked;
}

function doesBlackoutBlockSite(hero, visitedUrl) {
    return true;
}
function doHostsMatch(visitedHost, blockedHost) {
    let hostsMatch = (visitedHost === blockedHost);
    if (!hostsMatch) {
        let visitedHostContainsSubdomain = (visitedHost.indexOf('.') !== -1);
        if (!visitedHostContainsSubdomain) {
            return false;
        }
        let visitedHostSansSubdomain = removeSubdomain(visitedHost);
        hostsMatch = (visitedHostSansSubdomain === blockedHost);
        return hostsMatch;
    }
    return true;
}

function isException(hero, visitedUrl) {
    let visitedHost = extractHostSansExtension(visitedUrl);
    let exceptions = hero.exceptions;
    for (var i = 0; i < exceptions.length; i++) {
        let exceptionUrl = exceptions[i];
        let exceptedHost = extractHostSansExtension(exceptionUrl);
        let hostsMatch = doHostsMatch(visitedHost, exceptedHost);
        if (hostsMatch) {
            let excepted = includesUrl(visitedUrl, exceptionUrl);
            if (excepted) {
                return true;
            }
        }
    }
    return false;
}

function extractHostSansExtension(url) {
    let host = removeProtocolAndStrip(url);
    host = removeExtension(host);
    return host;
}

function includesUrl(visitedUrl, blockedUrl) {
    let visitedUrlStripped = removeProtocolAndStrip(visitedUrl);
    let blockedUrlStripped = removeProtocolAndStrip(blockedUrl);
    visitedUrlStripped = removeTrailingSlash(visitedUrlStripped);
    blockedUrlStripped = removeTrailingSlash(blockedUrlStripped);
    let includesUrl = (visitedUrlStripped.includes(blockedUrlStripped));
    return includesUrl;
}

// here Strip refers to removing the www. in a domain, or making the domain "naked"
function removeProtocolAndStrip(url) {
    url = url.replace(/^(?:https?:\/\/)?(?:www\.)?/i, '');
    return url;
}

function removeTrailingSlash(url) {
    url = url.replace(/\/+$/, '');
    return url;
}

function removeExtension(host) {
    let hostContainsSubdomain = containsSubdomain(host);
    var extensionPeriodIndex;
    if (hostContainsSubdomain) {
        let subdomainPeriodIndex = host.indexOf('.');
        extensionPeriodIndex = host.indexOf('.', subdomainPeriodIndex + 1);
    } else {
        extensionPeriodIndex = host.indexOf('.');
    }
    let hostMinusExtension = host.substr(0, extensionPeriodIndex);
    return hostMinusExtension;
}

function containsSubdomain(host) {
    let numPeriods = (host.match(/\./g) || []).length;
    if (numPeriods == 2) {
        return true;
    } else {
        return false;
    }
}

function removeSubdomain(host) {
    let subdomainPeriodIndex = host.indexOf('.');
    let hostSansSubdomain = host.substr(subdomainPeriodIndex + 1, host.length - 1);
    return hostSansSubdomain;
}

function initiateBlockQueryAllTabs() {
    io.emit('initiate-block-query-all-tabs');
}

exports.server = server;
exports.port = 9010;
exports.activeSocketConnections = activeSocketConnections;
exports.initiateBlockQueryAllTabs = initiateBlockQueryAllTabs;
