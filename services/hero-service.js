var moment = require('moment');
const log = require('electron-log');
var database = require('../database/database.js');
var heroCache = require('../database/hero-cache.js');
const uninstallService = require('./uninstall-service.js');
const eventService = require('./event-service.js');

function getHeroesFromDb() {
    return new Promise(function(resolve, reject) {
        database.get({ docType: 'hero' }, (err, heroDocs) => {
            if (err) {
                log.error(err);
                reject(err);
            } else {
                resolve(heroDocs);
            }
        });
    });
}

function isHeroActive(hero, now) {
    if (hero.frozen) {
        return false;
    }
    if (hero.blockType === 'one-time') {
        let start = moment(hero.startDateTime, 'MM/DD/YYYY HH:mm:ss');
        start.seconds(0);
        start.milliseconds(0);
        let end = moment(hero.endDateTime, 'MM/DD/YYYY HH:mm:ss');
        if (now.isBetween(start, end, null, '[)')) {
            return true;
        } else {
            return false;
        }
    } else if (hero.blockType === 'ongoing') {
        let dayOfWeek = now.isoWeekday().toString();
        let blockDays = hero.schedule.blockDays;
        if (blockDays[dayOfWeek] != undefined) {
            let blockDay = blockDays[dayOfWeek];
            let start = moment(blockDay.startTime, 'HH:mm');
            let end = moment(blockDay.endTime, 'HH:mm');
            let midnight = moment('00:00', 'HH:mm');
            if (end.isSame(midnight)) {
                end.add('1', 'days');
            }
            if (now.isBetween(start, end, null, '[)')) {
                return true;
            }
        }
    }
    return false;
}

function isHeroScheduled(hero, now) {
    if (hero.blockType === 'ongoing') {
        let heroNotActive = !isHeroActive(hero, now);
        if (heroNotActive) {
            let heroNotResting = !isHeroResting(hero);
            if (heroNotResting) {
                return true;
            }            
        }
    } 
    return false;
}

function isHeroResting(hero) {
    return hero.status === 'resting';
}

function getActiveHeroes(heroes, now) {
    let activeHeroes = [];
    for (var i = 0; i < heroes.length; i++) {
        let hero = heroes[i];
        let heroIsActive = isHeroActive(hero, now);
        if (heroIsActive) {
            activeHeroes.push(hero);
        }
    }
    return activeHeroes;
}

function getScheduledHeroes(heroes, now) {
    let scheduledHeroes = [];
    for (var i = 0; i < heroes.length; i++) {
        let hero = heroes[i];
        let heroIsScheduled = isHeroScheduled(hero, now);
        if (heroIsScheduled) {
            scheduledHeroes.push(hero);
        }
    }
    return scheduledHeroes;
}

function getNonActiveHeroes(heroes, now) {
    let nonActiveHeroes = [];
    for (var i = 0; i < heroes.length; i++) {
        let hero = heroes[i];
        let heroNoLongerActive = !isHeroActive(hero, now);
        if (heroNoLongerActive) {
            nonActiveHeroes.push(hero);
        }
    }
    return nonActiveHeroes;
}

function updateTimeRemaining(hero, now) {
    var end;
    if (hero.blockType === 'one-time') {
        end = moment(hero.endDateTime, 'MM/DD/YYYY HH:mm:ss');
    } else if (hero.blockType === 'ongoing') {
        let today = now.isoWeekday();
        let blockDay = hero.schedule.blockDays[today.toString()];
        let scheduledNow = now.clone();
        let blockDayEndTime = blockDay.endTime;
        if (blockDayEndTime === '00:00') {
            scheduledNow.add(1, 'days');
        }
        let endDateTime = scheduledNow.format('MM/DD/YYYY') + ' ' + blockDayEndTime;
        end = moment(endDateTime, 'MM/DD/YYYY HH:mm');
    }
    let duration = moment.duration(end.diff(now));
    var days = Math.floor(duration.asDays());
    var hours = Math.floor(duration.asHours() % 24);
    var minutes = Math.floor(duration.asMinutes() % 60);
    var seconds = Math.floor(duration.asSeconds() % 60);
    if (seconds > 0) {
        minutes++;
    }
    if (minutes == 60) {
        hours++;
        minutes = 0;
    }
    if (hours == 24) {
        days++;
        hours = 0;
    }
    hero.timeRemaining = {
        timeRemainingInMinutes: duration.asMinutes(),
        days: days,
        hours: hours,
        minutes: minutes
    }
}

function markHeroActive(hero, now) {
    hero.status = 'active';
    updateTimeRemaining(hero, now);
}

function unmarkHeroActive(hero) {
    delete hero.status;
    delete hero.timeRemaining;
}

function markHeroScheduled(hero, now) {
    hero.status = 'scheduled';
}

function unmarkHeroScheduled(hero) {
    delete hero.status;
    delete hero.scheduledForDay;
    delete hero.scheduledForStartTime;
    delete hero.scheduledForEndTime;
}

function markHeroResting(hero) {
    hero.status = 'resting';
}

function unmarkHeroResting(hero) {
    delete hero.status;
}

function findNextScheduledBlockDayOfWeek(hero, now) {
    let today = now.isoWeekday();
    let blockDays = hero.schedule.blockDays;
    // check if next block day is this week
    for (var dayOfWeek = today; dayOfWeek <= 7; dayOfWeek++) {
        let dayOfWeekString = dayOfWeek.toString();
        let blockDayExists = (blockDays[dayOfWeekString] != undefined);
        if (blockDayExists) {
            let blockDay = blockDays[dayOfWeekString];
            if (dayOfWeek == today) {
                let blockDayStartTime = moment(blockDay.startTime, 'HH:mm');
                if (now.isBefore(blockDayStartTime)) {
                    return dayOfWeek;
                }
            } else {
                return dayOfWeek;
            }
        }
    }
    // check if next block day is next week
    for (var dayOfWeek = 0; dayOfWeek <= today; dayOfWeek++) {
        let dayOfWeekString = dayOfWeek.toString();
        let blockDayExists = (blockDays[dayOfWeekString] != undefined);
        if (blockDayExists) {
            return dayOfWeek;
        }
    }
}

function getHeroIndex(heroes, hero) {
    let id = hero._id;
    for (var i = 0; i < heroes.length; i++) {
        let candidateHero = heroes[i];
        if (candidateHero._id === id) {
            return i;
        }
    }
    return -1;
}

function containsOngoingHero(heroes) {
    for (var i = 0; i < heroes.length; i++) {
        let hero = heroes[i];
        if (hero.blockType === 'ongoing') {
            return true;
        }
    }
    return false;
}

function containsHeroActiveForAMinute(heroes, now) {
    for (var i = 0; i < heroes.length; i++) {
        let hero = heroes[i];
        let start = moment(hero.startDateTime, 'MM/DD/YYYY HH:mm:ss');
        let timeElapsed = now.diff(start, 'minutes');
        let activeForAtLeastAMinute = timeElapsed > 0;
        if (activeForAtLeastAMinute) {
            return true;
        }
    }
    return false;
}

function getOneTimeHeroes(heroes) {
    let oneTimeHeroes = [];
    for (var i = 0; i < heroes.length; i++) {
        let hero = heroes[i];
        if (hero.blockType === 'one-time') {
            oneTimeHeroes.push(hero);
        }
    }
    return oneTimeHeroes;
}

function getOngoingHeroes(heroes) {
    let ongoingHeroes = [];
    for (var i = 0; i < heroes.length; i++) {
        let hero = heroes[i];
        if (hero.blockType === 'ongoing') {
            ongoingHeroes.push(hero);
        }
    }
    return ongoingHeroes;
}

function setOneTimeHeroDateTimes(hero) {
    let now = moment();
    hero.startDateTime = now.format('MM/DD/YYYY HH:mm:ss');
    now.add(hero.blockDuration, 'minutes');
    hero.endDateTime = now.format('MM/DD/YYYY HH:mm:ss');
}

function freezeHeroes() {
    let heroes = heroCache.getHeroes();
    for (var i = 0; i < heroes.length; i++) {
        let hero = heroes[i];
        hero.frozen = true;
        heroCache.deleteHero(hero);
        heroCache.addHero(hero);
        database.update({ _id: hero._id }, hero, (err, numReplaced) => {
            if (err) {
                log.error(err);
            }
        });
    }
}

function unfreezeHeroes() {
    let heroes = heroCache.getHeroes();
    for (var i = 0; i < heroes.length; i++) {
        let hero = heroes[i];
        delete hero.frozen;
        heroCache.deleteHero(hero);
        heroCache.addHero(hero);
        database.update({ _id: hero._id }, hero, (err, numReplaced) => {
            if (err) {
                log.error(err);
            }
        });
    }
}

function createBlockEvent(hero) {
    let blockEvent = {
        _id: hero._id,
        type: hero.heroType,
        frequency: hero.blockType
    }
    if (hero.blockType === 'one-time') {
        blockEvent.blockDuration = hero.blockDuration;
    } else if (hero.blockType === 'ongoing') {
        let blockDays = hero.schedule.blockDays;
        for (var dayOfWeek in blockDays) {
            let blockDay = blockDays[dayOfWeek];
            blockEvent[dayOfWeek] = blockDay;
        }
    }
    return blockEvent;
}

function deleteHeroes() {
    return new Promise((resolve, reject) => {
        database.deleteAll({ docType: 'hero' }, (err, numRemoved) => {
            if (err) {
                log.error(err);
                reject(err);
            } else {
                heroCache.clearCache();
                uninstallService.runActiveHeroCheck();
                resolve(true);
            }
        });
    });
}

function sendFirstBlockCreatedEvent(hero) {
    let blockEvent = createBlockEvent(hero);
    eventService.track(eventService.FIRST_BLOCK_CREATED, blockEvent);
}

function sendBlockCreatedEvent(hero) {
    let blockEvent = createBlockEvent(hero);
    eventService.track(eventService.BLOCK_CREATED, blockEvent);
}

function sendBlockActiveEvent(hero) {
    let blockEvent = createBlockEvent(hero);
    eventService.track(eventService.BLOCK_ACTIVE, blockEvent);
}

function sendBlockStartedEvent(hero) {
    let blockEvent = createBlockEvent(hero);
    eventService.track(eventService.BLOCK_STARTED, blockEvent);
}

function sendBlockEditedEvent(hero) {
    let blockEvent = createBlockEvent(hero);
    eventService.track(eventService.BLOCK_EDITED, blockEvent);
}

function sendBlockDeletedEvent(hero) {
    let blockEvent = createBlockEvent(hero);
    eventService.track(eventService.BLOCK_DELETED, blockEvent);
}

exports.getHeroesFromDb = getHeroesFromDb;
exports.isHeroActive = isHeroActive;
exports.isHeroScheduled = isHeroScheduled;
exports.getActiveHeroes = getActiveHeroes;
exports.getScheduledHeroes = getScheduledHeroes;
exports.getNonActiveHeroes = getNonActiveHeroes;
exports.markHeroActive = markHeroActive;
exports.updateTimeRemaining = updateTimeRemaining;
exports.unmarkHeroActive = unmarkHeroActive;
exports.markHeroScheduled = markHeroScheduled;
exports.unmarkHeroScheduled = unmarkHeroScheduled;
exports.markHeroResting = markHeroResting;
exports.unmarkHeroResting = unmarkHeroResting;
exports.getHeroIndex = getHeroIndex;
exports.containsOngoingHero = containsOngoingHero;
exports.containsHeroActiveForAMinute = containsHeroActiveForAMinute;
exports.getOneTimeHeroes = getOneTimeHeroes;
exports.getOngoingHeroes = getOngoingHeroes;
exports.setOneTimeHeroDateTimes = setOneTimeHeroDateTimes;
exports.freezeHeroes = freezeHeroes;
exports.unfreezeHeroes = unfreezeHeroes;
exports.deleteHeroes = deleteHeroes;
exports.sendFirstBlockCreatedEvent = sendFirstBlockCreatedEvent;
exports.sendBlockCreatedEvent = sendBlockCreatedEvent;
exports.sendBlockActiveEvent = sendBlockActiveEvent;
exports.sendBlockStartedEvent = sendBlockStartedEvent;
exports.sendBlockEditedEvent = sendBlockEditedEvent;
exports.sendBlockDeletedEvent = sendBlockDeletedEvent;