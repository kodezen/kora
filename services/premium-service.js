const { ipcMain } = require('electron');
const got = require('got');
const to = require('await-to-js').default;
const moment = require('moment');
const log = require('electron-log');
var userService = require('./user-service.js');
var heroService = require('./hero-service.js');
var uninstallService = require('./uninstall-service.js');
var ipcService = require('./ipc-service.js');
var eventService = require('./event-service.js');


var lastActiveCodeCheckDT;
var premiumCodeInactiveSender;

ipcMain.on('premium-code-inactive-received', (event) => {
    clearInterval(premiumCodeInactiveSender);
});

function onPremiumActivated(code) {
    userService.storePremiumCode(code);
    heroService.unfreezeHeroes();
    uninstallService.runActiveHeroCheck();
    eventService.updatePeopleProfile({ plan: 'subscription' });
}

function startCodeChecker() {
    verifyCodeActive();
    setInterval(activeCodeChecker, 60 * 60000);
}

function activeCodeChecker() {
    let now = moment();
    let activeCodeCheckRequired = (now.diff(lastActiveCodeCheckDT, 'weeks') > 0);
    if (activeCodeCheckRequired) {
        verifyCodeActive();
    }
}

async function verifyCodeActive() {
    lastActiveCodeCheckDT = moment();
    let code = userService.getPremiumCode();
    let options = {
        headers: { 'Content-type': 'application/json' },
        body: JSON.stringify({code: code})
    };
    let err, response;
    [ err, response ] = await to(got('https://evening-scrubland-10274.herokuapp.com/premium/verify-code', options));
    if (err) {
        log.error(err);
        return;
    }
    let message = JSON.parse(response.body);
    if (message.type === 'SUCCESS') {
        let validInfo = message.data;
        if (!validInfo.valid) {
            log.info('Inactive premium code');
            handleInactiveCode(validInfo)
        }
        else {
            log.info('Premium code active');
        }
    }
}

function handleInactiveCode(validInfo) {
    heroService.freezeHeroes();
    uninstallService.allowUninstall();
    sendCodeInactive(validInfo);
}

function sendCodeInactive(validInfo) {
    let channel = 'premium-code-inactive';
    send(channel);
    premiumCodeInactiveSender = setInterval(send, 500, channel, validInfo);
}

function send(channel, data) {
    ipcService.send(channel, data);
}

exports.onPremiumActivated = onPremiumActivated;
exports.startCodeChecker = startCodeChecker;