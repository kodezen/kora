const fs = require('fs');
const childProcess = require('child_process');

var path = 'C:\\ProgramData\\zKora';

function createDataFolder() {
    return new Promise(function (resolve, reject) {
        fs.mkdir(path, {}, (err) => {
            if (err) {
                console.log(err);
            }
            console.log('Folder created: now hiding...');
            childProcess.spawn('attrib', ['+s', '+h', path]);
        });
    });
}

exports.createDataFolder = createDataFolder;