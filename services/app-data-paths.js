var { app } = require('electron');
const appDataPath = app.getPath('appData');
const dataFolderPath = app.getPath('userData');
const databaseFilePath = `${dataFolderPath}\\Binary`;
const uninstallFilePath =  `${dataFolderPath}\\Versioning-binary`;
const updateFilePath =  `${dataFolderPath}\\update-in-progress`;
const trialExpiredFilePath = `${appDataPath}\\LAU7_020`;

exports.dataFolderPath = dataFolderPath;
exports.databaseFilePath = databaseFilePath;
exports.uninstallFilePath = uninstallFilePath;
exports.trialExpiredFilePath = trialExpiredFilePath;
exports.updateFilePath = updateFilePath;