var database = require('../database/database.js');
const log = require('electron-log');

let supportCodes = ['VV1BCOMXL1', 'GVI1UV2XZE', 'HO29DH3YCH', 'KE8NSBGWO0', '550J3Q4QJA', 'NR32L2KUDY', '6CH0H4GDVA', 'ESTH0CLZ9V', 'E7MY6NFKCI', 'YNNS5BLKPG', '1NZ1D1TWIY', 'QUDL6R05HV', 'SFWWBTXXVW', '8JB35G40TC', 'PRIJ0UPHBS', 'H0JMWTLLI7', 'CYEML171E1', 'ZYLN4KYUFC', 'OWCQ5U49QE', 'Z09C45G3QD', '1FK8882R2Y', '1W9OH51NTV', 'R5BVZDOZKF', '1LFHR4GWJC', '2P9SQXH62F'];

function verifySupportCode(code) {
    return new Promise(function (resolve, reject) {
        let invalidSupportCode = !(supportCodes.includes(code));
        if (invalidSupportCode) {
            return resolve(false);
        }
        database.getOne({ code: code }, (err, supportCodeDoc) => {
            if (err) {
                log.error(err);
                return reject(err);
            }
            let supportCodeUsed = (supportCodeDoc != null);
            if (supportCodeUsed) {
                return resolve(false);
            }
            let newSupportCodeDoc = {
                docType: 'support-code',
                code: code
            };
            database.insert(newSupportCodeDoc, (err, insertedSupportCodeDoc) => {
                if (err) {
                    log.error(err);
                    reject(err);
                } else {
                    resolve(true);
                }
            });
        });
    });
}


exports.verifySupportCode = verifySupportCode;