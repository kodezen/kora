const fs = require('fs');
var moment = require('moment');
const userService = require('./user-service.js');
const heroService = require('./hero-service.js');
const uninstallService = require('./uninstall-service.js');
const eventService = require('./event-service.js');
const actionService = require('./action-service.js');

const appDataPaths = require('./app-data-paths.js');
var filePath = appDataPaths.trialExpiredFilePath;

function checkFreeTrialExpiredFileExists() {
    let fileExists = fs.existsSync(filePath);
    return fileExists;
}

function isTrialActive() {
    let trialExpirationDT = userService.getTrialExpirationDT();
    trialExpirationDT = moment(trialExpirationDT, 'MM/DD/YYYY HH:mm:ss');
    let now = moment();
    let trialActive = now.isBefore(trialExpirationDT);
    return trialActive;
}

function onTrialExpired() {
    eventService.track(eventService.TRIAL_EXPIRED, {});
    let actionDetails = {
        offerExpirationDT: moment().add(4, 'hours').format('MM/DD/YYYY HH:mm:ss')
    }
    actionService.registerAction(actionService.INITIATED_TRIAL_EXPIRED_OFFER, actionDetails);
    createTrialExpiredFile();
    handleExpiredTrial();
}

function handleExpiredTrial() {
    heroService.freezeHeroes();
    uninstallService.allowUninstall();
}

function createTrialExpiredFile() {
    fs.exists(filePath, (exists) => {
        if (!exists) {
            fs.writeFile(filePath, 'true', (err) => {
                if (err) {
                    log.error(err);
                }
            });
        }
    });
}

exports.checkFreeTrialExpiredFileExists = checkFreeTrialExpiredFileExists;
exports.isTrialActive = isTrialActive;
exports.onTrialExpired = onTrialExpired;
exports.handleExpiredTrial = handleExpiredTrial;
exports.createTrialExpiredFile = createTrialExpiredFile;