const heroCache = require('../database/hero-cache.js');
const ipcService = require('./ipc-service.js');
const to = require('await-to-js').default;
const childProcess = require('child_process');
var electronWindow = require('../electron-win.js');
const log = require('electron-log');
let invalidBrowsers = ['MicrosoftEdge.exe', 'iexplore.exe', 'msedge.exe', 'firefox.exe', 'vivaldi.exe', 'brave.exe'];

function start() {
    setInterval(async () => {
        let activeHeroes = heroCache.getActiveHeroes();
        if (activeHeroes.length > 0) {
            let err, runningInvalidBrowsers;
            [ err, runningInvalidBrowsers ] = await to(checkRunningInvalidBrowsers());
            if (!err) {
                let invalidBrowserIsRunning = (runningInvalidBrowsers.length > 0);
                if (invalidBrowserIsRunning) {
                    let killedMicrosoftEdge = false;
                    for (var i = 0; i < runningInvalidBrowsers.length; i++) {
                        let runningInvalidBrowser = runningInvalidBrowsers[i];
                        if (runningInvalidBrowser === 'MicrosoftEdge.exe') {
                            killedMicrosoftEdge = true;
                        }
                        log.info('Killing invalid browser: ', runningInvalidBrowser);
                        childProcess.exec(`taskkill /F /IM ${runningInvalidBrowser}`);
                    }
                    if (!killedMicrosoftEdge) {
                        informUser();
                    } 
                }
            }
        }
    }, 20000);
}

function checkRunningInvalidBrowsers () {
    return new Promise((resolve, reject) => {
        let runningInvalidBrowsers = [];
        childProcess.exec('tasklist', (err, stdout, stderr) => {
            if (err) {
                reject(err);
            }
            for (var i = 0; i < invalidBrowsers.length; i++) {
                let invalidBrowser = invalidBrowsers[i];
                if (stdout.includes(invalidBrowser)) {
                    runningInvalidBrowsers.push(invalidBrowser);
                }
            }
            resolve(runningInvalidBrowsers);
        });
    });   
}

function informUser() {
    ipcService.send('used-invalid-browser');
    if (electronWindow.getWindow() != null) {
        electronWindow.getWindow().show();
    }
}

exports.start = start;