const heroCache = require('../database/hero-cache.js');
const extensionService = require('./extension-service.js');
const ipcService = require('./ipc-service.js');
const childProcess = require('child_process');
const electronWindow = require('../electron-win.js');
const to = require('await-to-js').default;

let graceRemovalBypass = false;
let reinstallingExtension = false;
let removalBypassInterval;
let reinstallExtensionInterval;

function start() {
    console.log('extension bypass service started');
    startRemovalBypassChecker();
}

function startRemovalBypassChecker() {
    removalBypassInterval = setInterval(async () => {
        console.log('checking for removal bypass...');
        let activeHeroes = heroCache.getActiveHeroes();
        let blockIsActive = (activeHeroes.length > 0);
        if (blockIsActive) {
            let err, running;
            [ err, running ] = await to(isChromeRunning());
            let potentialBypass = (!err && running && !extensionService.isExtensionConnected());
            if (potentialBypass) {
                onPotentialRemovalBypass();
            } else if (graceRemovalBypass) {
                // if the first potential bypass wasn't followed by a second, reset the graceBypass flag
                graceRemovalBypass = false;
            }
        }
    }, 10000);
}

function onPotentialRemovalBypass() {
    // if this is the second "potentialBypass" detected in a row, handle the bypass and reset grace flag
    if (graceRemovalBypass) {
        console.log('removal bypass DETECTED');
        onBypassDetected();
        graceRemovalBypass = false;
    } else {
        // if this is the first, mark it as a graceRemovalBypass
        graceRemovalBypass = true;
    }
}

function onIncognitoDisabled() {
    if (!reinstallingExtension) {
        console.log('incognito bypass detected');
        onBypassDetected();
    }
}

function onBypassDetected() {
    if (!reinstallingExtension) {
        console.log('bypass detected');
        reinstallingExtension = true;
        stopRemovalBypassChecker();
        setTimeout(() => {
            killChrome();
            ipcService.send('reinstall-extension');
            electronWindow.show();
            startReinstallExtensionInterval();
        }, 3000);
    }
}

function startReinstallExtensionInterval() {
    console.log('reinstall extension interval started');
    reinstallExtensionInterval = setInterval(async () => {
        let err, running;
        [ err, running ] = await to(isChromeRunning());
        if (!err && running) {
            console.log('reinstall extension interval chrome kill');
            killChrome();
            ipcService.send('reinstall-extension');
        }
    }, 60000);
}

function onExtensionReinstalled() {
    if (reinstallingExtension) {
        reinstallingExtension = false;
        console.log('on extension reinstalled');
        stopReinstallExtensionInterval();
        ipcService.send('extension-reinstalled');
        electronWindow.show();
        startRemovalBypassChecker();
    }
}

function isChromeRunning() {
    return new Promise((resolve, reject) => {
        childProcess.exec('tasklist', (err, stdout, stderr) => {
            if (err) {
                return resolve(err);
            } 
            let running = stdout.includes('chrome.exe');
            if (running) {
                resolve(true);
            } else {
                resolve(false);
            }
        });
    });
}

function killChrome() {
    childProcess.exec('taskkill /F /IM chrome.exe');
    childProcess.exec('taskkill /F /IM chrome.exe*32');
    console.log('killed chrome');
}

function stopRemovalBypassChecker() {
    clearInterval(removalBypassInterval);
}

function stopReinstallExtensionInterval() {
    clearInterval(reinstallExtensionInterval);
}

function isReinstallingExtension() {
    return reinstallingExtension;
}

exports.start = start;
exports.isReinstallingExtension = isReinstallingExtension;
exports.onIncognitoDisabled = onIncognitoDisabled;
exports.onExtensionReinstalled = onExtensionReinstalled;
