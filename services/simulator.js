const moment = require('moment');
const userService = require('./user-service');
const trialService = require('./trial-service');

async function simulateFreeTrialExpired() {
    let now = moment();
    let fiveMinutesAgo = now.add(-5, 'minutes').format('MM/DD/YYYY HH:mm:ss');
    userService.setTrialExpirationDT(fiveMinutesAgo);
}

async function simulateFreeTrialAbuser() {
    trialService.createTrialExpiredFile();
}

exports.simulateFreeTrialExpired = simulateFreeTrialExpired;
exports.simulateFreeTrialAbuser = simulateFreeTrialAbuser;