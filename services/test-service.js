var database = require('../database/database.js');
var timeSavedCache = require('../database/time-saved-cache.js');
var moment = require('moment');

function insertTestHeroesIntoDb(testNumber) {
    return new Promise(function(resolve, reject) {
        var testHeroes;
        if (testNumber == 1) {
            testHeroes = createTest1Heroes();
        } else if (testNumber == 2) {
            testHeroes = createTest2Heroes();
        }
        database.insert(testHeroes, (err, heroDocs) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                resolve("done");
            }
        });
    });
}

function createTest1Heroes() {
    let hero1 = {
        name: 'hero1',
        heroType: 'blocker',
        docType: 'hero',
        blockedSites: ['twitter.com'],
        exceptions: [],
        blockType: 'one-time'
    }
    let now = moment();
    now.seconds(0);
    now.milliseconds(0);
    let future = moment();
    future.seconds(0);
    future.milliseconds(0);
    hero1.startDateTime = now.format('MM/DD/YYYY HH:mm');
    future.add(3, 'hours');
    hero1.endDateTime = future.format('MM/DD/YYYY HH:mm');

    // one time active for two minutes
    let hero2 = {
        name: 'hero2',
        heroType: 'blocker',
        docType: 'hero',
        blockedSites: ['myspace.com'],
        exceptions: [],
        blockType: 'one-time'
    }
    hero2.startDateTime = now.format('MM/DD/YYYY HH:mm');
    future.add(1, 'minutes');
    hero2.endDateTime = future.format('MM/DD/YYYY HH:mm');

    // resting one-time
    let hero3 = {
        name: 'hero3',
        heroType: 'blocker',
        docType: 'hero',
        blockedSites: ['tumblr.com'],
        exceptions: [],
        blockType: 'one-time'
    }
    hero3.startDateTime = '12/01/2018 17:00';
    hero3.endDateTime = '12/01/2018 18:00';

    let dayOfWeekString = now.isoWeekday().toString();
    // ongoing active for 1 minute
    let future4 = moment();
    future4.seconds(0);
    future4.milliseconds(0);
    future4.add(1, 'minutes');
    let hero4 = {
        name: 'hero4',
        heroType: 'blocker',
        docType: 'hero',
        blockedSites: ['instagram.com'],
        exceptions: [],
        blockType: 'ongoing',
        schedule: {
            blockDays: {},
            validSchedule: true
        }
    }
    hero4.schedule.blockDays[dayOfWeekString] = {
        dayOfWeek: dayOfWeekString,
        startTime: now.format('HH:mm'),
        endTime: future4.format('HH:mm'),
        validTimes: true
    };

    // ongoing active for 30 minutes
    let future5 = moment();
    future5.seconds(0);
    future5.milliseconds(0);
    future5.add(30, 'minutes');
    let hero5 = {
        name: 'hero5',
        heroType: 'blocker',
        docType: 'hero',
        blockedSites: ['instagram.com'],
        exceptions: [],
        blockType: 'ongoing',
        schedule: {
            blockDays: {},
            validSchedule: true
        }
    }
    hero5.schedule.blockDays[dayOfWeekString] = {
        dayOfWeek: dayOfWeekString,
        startTime: now.format('HH:mm'),
        endTime: future5.format('HH:mm'),
        validTimes: true
    };
    hero5.schedule.blockDays['7'] = {
        dayOfWeek: '7',
        startTime: '03:00',
        endTime: '16:00',
        validTimes: true
    }

    // ongoing scheduled for this morning
    let hero6 = {
        name: 'hero6',
        heroType: 'blocker',
        docType: 'hero',
        blockedSites: ['instagram.com'],
        exceptions: [],
        blockType: 'ongoing',
        schedule: {
            blockDays: {},
            validSchedule: true
        }
    }
    hero6.schedule.blockDays[dayOfWeekString] = {
        dayOfWeek: dayOfWeekString,
        startTime: '08:00',
        endTime: '09:00',
        validTimes: true
    };
    hero6.schedule.blockDays['3'] = {
        dayOfWeek: '3',
        startTime: '11:00',
        endTime: '15:00',
        validTimes: true
    }
    hero6.schedule.blockDays['5'] = {
        dayOfWeek: '5',
        startTime: '14:00',
        endTime: '23:00',
        validTimes: true
    }

    // ongoing scheduled for today 
    let future7 = moment();
    future7.seconds(0);
    future7.milliseconds(0);
    let futureFuture7 = moment();
    futureFuture7.seconds(0);
    futureFuture7.milliseconds(0);
    future7.add(1, 'minutes');
    futureFuture7.add(2, 'minutes');
    let hero7 = {
        name: 'hero7',
        heroType: 'blocker',
        docType: 'hero',
        blockedSites: ['instagram.com'],
        exceptions: [],
        blockType: 'ongoing',
        schedule: {
            blockDays: {},
            validSchedule: true
        }
    }
    hero7.schedule.blockDays[dayOfWeekString] = {
        dayOfWeek: dayOfWeekString,
        startTime: future7.format('HH:mm'),
        endTime: futureFuture7.format('HH:mm'),
        validTimes: true
    };

    // ongoing scheduled for monday
    future.add(1, 'minutes');
    let hero8 = {
        name: 'hero8',
        heroType: 'blocker',
        docType: 'hero',
        blockedSites: ['instagram.com'],
        exceptions: [],
        blockType: 'ongoing',
        schedule: {
            blockDays: {
                '1': {
                    dayOfWeek: '1',
                    startTime: now.format('HH:mm'),
                    endTime: future.format('HH:mm'),
                    validTimes: true
                }
            },
            validSchedule: true
        }
    }

    var heroes = []
    heroes.push(hero1);
    heroes.push(hero2);
    heroes.push(hero3);
    heroes.push(hero4);
    heroes.push(hero5);
    heroes.push(hero6);
    heroes.push(hero7);
    heroes.push(hero8);
    return heroes;
}

function createTest2Heroes() {
    let heroes = []
    let timeSaved = timeSavedCache.getTimeSaved();
    // TEST: one time hero in the past, 30 mins block duration, last updated was at 15 minute mark
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'one-time'
    // }
    // hero1.startDateTime = '01/17/2019 12:30:15';
    // hero1.endDateTime = '01/17/2019 13:00:15';
    // heroes.push(hero1);
    // timeSaved.minutesSaved = 14;
    // timeSaved.lastUpdated = '01/17/2019 12:45:00';

    // TEST: one time hero still active
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'one-time'
    // }
    // let now = moment();
    // // now.seconds(0);
    // // now.milliseconds(0);
    // let past = now.clone();
    // let future = now.clone();
    // past.subtract('5', 'minutes');
    // future.add('1', 'minutes');

    // let startDateTime = past.format('MM/DD/YYYY HH:mm:ss');
    // let endDateTime = future.format('MM/DD/YYYY HH:mm:ss');
    // hero1.startDateTime = startDateTime;
    // hero1.endDateTime = endDateTime;
    // heroes.push(hero1);
    // timeSaved.minutesSaved = 5;
    // timeSaved.lastUpdated = now.format('MM/DD/YYYY HH:mm:ss');

    // TEST: overlapping one-time heroes, ending on different times, that have become resting 
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'one-time'
    // }
    // hero1.startDateTime = '01/17/2019 12:30';
    // hero1.endDateTime = '01/17/2019 13:00';

    // let hero2 = {
    //     name: 'hero2',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'one-time'
    // }
    // hero2.startDateTime = '01/17/2019 12:40';
    // hero2.endDateTime = '01/17/2019 13:10';

    // heroes.push(hero1);
    // heroes.push(hero2);
    // timeSaved.minutesSaved = 15;
    // timeSaved.lastUpdated = '01/17/2019 12:45';

    // TEST: two onetime heroes that end at the same time, one shorter than the other
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'one-time'
    // }
    // hero1.startDateTime = '01/17/2019 12:30';
    // hero1.endDateTime = '01/17/2019 13:00';

    // let hero2 = {
    //     name: 'hero2',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'one-time'
    // }
    // hero2.startDateTime = '01/17/2019 12:10';
    // hero2.endDateTime = '01/17/2019 13:00';

    // heroes.push(hero1);
    // heroes.push(hero2);
    // timeSaved.minutesSaved = 35;
    // timeSaved.lastUpdated = '01/17/2019 12:45';

    // TEST: two one time heroes now resting, one starts after the last updated time
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'one-time'
    // }
    // hero1.startDateTime = '01/17/2019 12:30';
    // hero1.endDateTime = '01/17/2019 13:30';

    // let hero2 = {
    //     name: 'hero2',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'one-time'
    // }
    // hero2.startDateTime = '01/17/2019 12:55';
    // hero2.endDateTime = '01/17/2019 13:30';

    // heroes.push(hero1);
    // heroes.push(hero2);
    // timeSaved.minutesSaved = 15;
    // timeSaved.lastUpdated = '01/17/2019 12:45';

    // TEST: two one time active, starting at different times, both ending after now
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'one-time'
    // }
    // let now = moment();
    // now.seconds(0);
    // now.milliseconds(0);
    // let past = now.clone();
    // let future = now.clone();
    // past.subtract('5', 'minutes');
    // future.add('2', 'minutes');
    // hero1.startDateTime = past.format('MM/DD/YYYY HH:mm:ss');
    // hero1.endDateTime = future.format('MM/DD/YYYY HH:mm:ss');

    // let hero2 = {
    //     name: 'hero2',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'one-time'
    // }
    // past.subtract('5', 'minutes');
    // hero2.startDateTime = past.format('MM/DD/YYYY HH:mm:ss');
    // hero2.endDateTime = future.format('MM/DD/YYYY HH:mm:ss');

    // heroes.push(hero1);
    // heroes.push(hero2);
    // timeSaved.minutesSaved = 9;
    // timeSaved.lastUpdated = now.subtract('1', 'minutes').format('MM/DD/YYYY HH:mm:ss');


    // TEST to verify that the if block around line 92 in time-saved-service is needed:
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'one-time'
    // }
    // let hero2 = {
    //     name: 'hero2',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'one-time'
    // }
    // let now = moment();
    // now.seconds(0);
    // now.milliseconds(0);
    // let hero1Start = now.clone();
    // let hero1End = now.clone();
    // hero1Start.subtract('30', 'seconds');
    // hero1End.add('3', 'minutes');
    // hero1End.add('30', 'seconds');

    // hero1.startDateTime = hero1Start.format('MM/DD/YYYY HH:mm:ss');
    // hero1.endDateTime = hero1End.format('MM/DD/YYYY HH:mm:ss');

    // heroes.push(hero1);
    // timeSaved.minutesSaved = 0;
    // timeSaved.lastUpdated = now.subtract('15', 'seconds').format('MM/DD/YYYY HH:mm:ss');



    // ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING 
    // ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING 
    // ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING 
    // ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING ONGOING 


    // TEST: ongoing hero that started today before last updated and finished
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'ongoing',
    //     schedule: {
    //         blockDays: {},
    //         validSchedule: true
    //     }
    // }
    // let now = moment();
    // now.seconds(0);
    // now.milliseconds(0);
    // let date = now.clone();
    // date.seconds(0);
    // date.milliseconds();
    // let dayOfWeekString = now.isoWeekday().toString();
    // let past1 = now.clone();
    // past1.subtract('5', 'minutes');
    // let past2 = now.clone();
    // past2.subtract('2', 'minutes');
    // hero1.schedule.blockDays[dayOfWeekString] = {
    //     dayOfWeek: dayOfWeekString,
    //     startTime: past1.format('HH:mm'),
    //     endTime: past2.format('HH:mm'),
    //     validTimes: true
    // };
    // heroes.push(hero1);
    // timeSaved.minutesSaved = 1;
    // timeSaved.lastUpdated = date.subtract('4', 'minutes').format('MM/DD/YYYY HH:mm:ss');

    // TEST: ongoing hero that started today before last updated and is still running
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'ongoing',
    //     schedule: {
    //         blockDays: {},
    //         validSchedule: true
    //     }
    // }
    // let now = moment();
    // now.seconds(0);
    // now.milliseconds(0);
    // let dayOfWeekString = now.isoWeekday().toString();
    // let past = now.clone();
    // past.subtract('5', 'minutes');
    // let future = now.clone();
    // future.add('2', 'minutes');
    // hero1.schedule.blockDays[dayOfWeekString] = {
    //     dayOfWeek: dayOfWeekString,
    //     startTime: past.format('HH:mm'),
    //     endTime: future.format('HH:mm'),
    //     validTimes: true
    // };
    // heroes.push(hero1);
    // timeSaved.minutesSaved = 4
    // timeSaved.lastUpdated = now.subtract('1', 'minutes').format('MM/DD/YYYY HH:mm:ss');

    // TEST: ongoing hero that started after last updated time and finished
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'ongoing',
    //     schedule: {
    //         blockDays: {},
    //         validSchedule: true
    //     }
    // }
    // let now = moment();
    // now.seconds(0);
    // now.milliseconds(0);
    // let date = now.clone();
    // date.seconds(0);
    // date.milliseconds(0);
    // let dayOfWeekString = now.isoWeekday().toString();
    // let past1 = now.clone();
    // let past2 = now.clone();
    // past1.subtract('5', 'minutes');
    // past2.subtract('3', 'minutes');
    // hero1.schedule.blockDays[dayOfWeekString] = {
    //     dayOfWeek: dayOfWeekString,
    //     startTime: past1.format('HH:mm'),
    //     endTime: past2.format('HH:mm'),
    //     validTimes: true
    // };
    // heroes.push(hero1);
    // timeSaved.minutesSaved = 0;
    // date.subtract('30', 'minutes');
    // timeSaved.lastUpdated = date.format('MM/DD/YYYY HH:mm:ss');


    // TEST: ongoing hero that started today after last updated time and is still running
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'ongoing',
    //     schedule: {
    //         blockDays: {},
    //         validSchedule: true
    //     }
    // }
    // let now = moment();
    // now.seconds(0);
    // now.milliseconds(0);
    // let date = now.clone();
    // date.seconds(0);
    // date.milliseconds(0);
    // let dayOfWeekString = now.isoWeekday().toString();
    // let past = now.clone();
    // let future = now.clone();
    // past.subtract('5', 'minutes');
    // future.add('2', 'minutes');
    // hero1.schedule.blockDays[dayOfWeekString] = {
    //     dayOfWeek: dayOfWeekString,
    //     startTime: past.format('HH:mm'),
    //     endTime: future.format('HH:mm'),
    //     validTimes: true
    // };
    // heroes.push(hero1);
    // timeSaved.minutesSaved = 0;
    // date.subtract('30', 'minutes');
    // timeSaved.lastUpdated = date.format('MM/DD/YYYY HH:mm:ss');


    // TEST: blockDay1 started before lastupdated and finished, blockDay2 still running
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'ongoing',
    //     schedule: {
    //         blockDays: {},
    //         validSchedule: true
    //     }
    // }
    // let now = moment();
    // now.seconds(0);
    // now.milliseconds(0);
    // let blockDay1Moment = now.clone();
    // let blockDay2Moment = now.clone();
    // blockDay1Moment.subtract('1', 'days');
    // let blockDay1DayOfWeekString = blockDay1Moment.isoWeekday().toString();
    // let blockDay1MomentStart = blockDay1Moment.clone().subtract('3', 'hours');
    // let date = blockDay1MomentStart.clone();
    // let blockDay1MomentEnd = blockDay1Moment.clone().subtract('2', 'hours');
    // let blockDay2DayOfWeekString = blockDay2Moment.isoWeekday().toString();
    // let blockDay2MomentStart = blockDay2Moment.clone().subtract('1', 'hours');
    // let blockDay2MomentEnd = blockDay2Moment.clone().add('1', 'hours');
    
    // hero1.schedule.blockDays[blockDay1DayOfWeekString] = {
    //     dayOfWeek: blockDay1DayOfWeekString,
    //     startTime: blockDay1MomentStart.format('HH:mm'),
    //     endTime: blockDay1MomentEnd.format('HH:mm'),
    //     validTimes: true
    // };
    // hero1.schedule.blockDays[blockDay2DayOfWeekString] = {
    //     dayOfWeek: blockDay2DayOfWeekString,
    //     startTime: blockDay2MomentStart.format('HH:mm'),
    //     endTime: blockDay2MomentEnd.format('HH:mm'),
    //     validTimes: true
    // };
    // heroes.push(hero1);
    // timeSaved.minutesSaved = 30;
    // date.add('30', 'minutes');
    // timeSaved.lastUpdated = date.format('MM/DD/YYYY HH:mm:ss');


    // TEST: blockDay1 started before lastupdated and finished, blockDay2 finished
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'ongoing',
    //     schedule: {
    //         blockDays: {},
    //         validSchedule: true
    //     }
    // }
    // let now = moment();
    // now.seconds(0);
    // now.milliseconds(0);
    // let blockDay1Moment = now.clone();
    // let blockDay2Moment = now.clone();
    // blockDay1Moment.subtract('1', 'days');
    // let blockDay1DayOfWeekString = blockDay1Moment.isoWeekday().toString();
    // let blockDay1MomentStart = blockDay1Moment.clone().subtract('3', 'hours');
    // let date = blockDay1MomentStart.clone();
    // let blockDay1MomentEnd = blockDay1Moment.clone().subtract('2', 'hours');
    // let blockDay2DayOfWeekString = blockDay2Moment.isoWeekday().toString();
    // let blockDay2MomentStart = blockDay2Moment.clone().subtract('1', 'hours');
    // let blockDay2MomentEnd = blockDay2Moment.clone().subtract('30', 'minutes');
    
    // hero1.schedule.blockDays[blockDay1DayOfWeekString] = {
    //     dayOfWeek: blockDay1DayOfWeekString,
    //     startTime: blockDay1MomentStart.format('HH:mm'),
    //     endTime: blockDay1MomentEnd.format('HH:mm'),
    //     validTimes: true
    // };
    // hero1.schedule.blockDays[blockDay2DayOfWeekString] = {
    //     dayOfWeek: blockDay2DayOfWeekString,
    //     startTime: blockDay2MomentStart.format('HH:mm'),
    //     endTime: blockDay2MomentEnd.format('HH:mm'),
    //     validTimes: true
    // };
    // heroes.push(hero1);
    // timeSaved.minutesSaved = 30;
    // date.add('30', 'minutes');
    // timeSaved.lastUpdated = date.format('MM/DD/YYYY HH:mm:ss');


    // TEST: blockDay1 started after lastUpdated and finished, blockDay2 still running
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'ongoing',
    //     schedule: {
    //         blockDays: {},
    //         validSchedule: true
    //     }
    // }
    // let now = moment();
    // now.seconds(0);
    // now.milliseconds(0);
    // let blockDay1Moment = now.clone();
    // let blockDay2Moment = now.clone();
    // blockDay1Moment.subtract('1', 'days');
    // let blockDay1DayOfWeekString = blockDay1Moment.isoWeekday().toString();
    // let blockDay1MomentStart = blockDay1Moment.clone().subtract('3', 'hours');
    // let date = blockDay1MomentStart.clone();
    // let blockDay1MomentEnd = blockDay1Moment.clone().subtract('2', 'hours');
    // let blockDay2DayOfWeekString = blockDay2Moment.isoWeekday().toString();
    // let blockDay2MomentStart = blockDay2Moment.clone().subtract('1', 'hours');
    // let blockDay2MomentEnd = blockDay2Moment.clone().add('1', 'hours');
    
    // hero1.schedule.blockDays[blockDay1DayOfWeekString] = {
    //     dayOfWeek: blockDay1DayOfWeekString,
    //     startTime: blockDay1MomentStart.format('HH:mm'),
    //     endTime: blockDay1MomentEnd.format('HH:mm'),
    //     validTimes: true
    // };
    // hero1.schedule.blockDays[blockDay2DayOfWeekString] = {
    //     dayOfWeek: blockDay2DayOfWeekString,
    //     startTime: blockDay2MomentStart.format('HH:mm'),
    //     endTime: blockDay2MomentEnd.format('HH:mm'),
    //     validTimes: true
    // };
    // heroes.push(hero1);
    // timeSaved.minutesSaved = 0;
    // date.subtract('30', 'minutes');
    // timeSaved.lastUpdated = date.format('MM/DD/YYYY HH:mm:ss');


    // TEST: blockDay1 started after lastUpdated, blockDay2 finished
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'ongoing',
    //     schedule: {
    //         blockDays: {},
    //         validSchedule: true
    //     }
    // }
    // let now = moment();
    // now.seconds(0);
    // now.milliseconds(0);
    // let blockDay1Moment = now.clone();
    // let blockDay2Moment = now.clone();
    // blockDay1Moment.subtract('1', 'days');
    // let blockDay1DayOfWeekString = blockDay1Moment.isoWeekday().toString();
    // let blockDay1MomentStart = blockDay1Moment.clone().subtract('3', 'hours');
    // let date = blockDay1MomentStart.clone();
    // let blockDay1MomentEnd = blockDay1Moment.clone().subtract('2', 'hours');
    // let blockDay2DayOfWeekString = blockDay2Moment.isoWeekday().toString();
    // let blockDay2MomentStart = blockDay2Moment.clone().subtract('1', 'hours');
    // let blockDay2MomentEnd = blockDay2Moment.clone().subtract('30', 'minutes');
    
    // hero1.schedule.blockDays[blockDay1DayOfWeekString] = {
    //     dayOfWeek: blockDay1DayOfWeekString,
    //     startTime: blockDay1MomentStart.format('HH:mm'),
    //     endTime: blockDay1MomentEnd.format('HH:mm'),
    //     validTimes: true
    // };
    // hero1.schedule.blockDays[blockDay2DayOfWeekString] = {
    //     dayOfWeek: blockDay2DayOfWeekString,
    //     startTime: blockDay2MomentStart.format('HH:mm'),
    //     endTime: blockDay2MomentEnd.format('HH:mm'),
    //     validTimes: true
    // };
    // heroes.push(hero1);
    // timeSaved.minutesSaved = 0;
    // date.subtract('30', 'minutes');
    // timeSaved.lastUpdated = date.format('MM/DD/YYYY HH:mm:ss');


    // TEST: fri: 6pm-10pm, sat whole day, sun whole day | starting january 1st
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'ongoing',
    //     schedule: {
    //         blockDays: {},
    //         validSchedule: true
    //     }
    // }
    // hero1.schedule.blockDays['5'] = {
    //     dayOfWeek: '5',
    //     startTime: '18:00',
    //     endTime: '22:00',
    //     validTimes: true
    // };
    // hero1.schedule.blockDays['6'] = {
    //     dayOfWeek: '6',
    //     startTime: '00:00',
    //     endTime: '00:00',
    //     validTimes: true
    // };
    // hero1.schedule.blockDays['7'] = {
    //     dayOfWeek: '7',
    //     startTime: '00:00',
    //     endTime: '00:00',
    //     validTimes: true
    // };

    // heroes.push(hero1);
    // timeSaved.minutesSaved = 0;
    // timeSaved.lastUpdated = '01/01/2019 00:00:00';

    // TEST: ongoing hero1: fri: 6pm-10pm, sat whole day, sun whole day | starting january 1st
    //       ongoing hero2: fri: 7pm-8pm
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'ongoing',
    //     schedule: {
    //         blockDays: {},
    //         validSchedule: true
    //     }
    // }
    // hero1.schedule.blockDays['5'] = {
    //     dayOfWeek: '5',
    //     startTime: '18:00',
    //     endTime: '22:00',
    //     validTimes: true
    // };
    // hero1.schedule.blockDays['6'] = {
    //     dayOfWeek: '6',
    //     startTime: '00:00',
    //     endTime: '00:00',
    //     validTimes: true
    // };
    // hero1.schedule.blockDays['7'] = {
    //     dayOfWeek: '7',
    //     startTime: '00:00',
    //     endTime: '00:00',
    //     validTimes: true
    // };
    // let hero2 = {
    //     name: 'hero2',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'ongoing',
    //     schedule: {
    //         blockDays: {},
    //         validSchedule: true
    //     }
    // }
    // hero2.schedule.blockDays['5'] = {
    //     dayOfWeek: '5',
    //     startTime: '19:00',
    //     endTime: '20:00',
    //     validTimes: true
    // };
    // heroes.push(hero1);
    // heroes.push(hero2);
    // timeSaved.minutesSaved = 0;
    // timeSaved.lastUpdated = '01/01/2019 00:00:00';
    

    // TEST: ongoing hero1: fri: 6pm-10pm, sat whole day, sun whole day | starting january 1st
    //       one-time hero: ending 4 hours into the new year 
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'ongoing',
    //     schedule: {
    //         blockDays: {},
    //         validSchedule: true
    //     }
    // }
    // hero1.schedule.blockDays['5'] = {
    //     dayOfWeek: '5',
    //     startTime: '18:00',
    //     endTime: '22:00',
    //     validTimes: true
    // };
    // hero1.schedule.blockDays['6'] = {
    //     dayOfWeek: '6',
    //     startTime: '00:00',
    //     endTime: '00:00',
    //     validTimes: true
    // };
    // hero1.schedule.blockDays['7'] = {
    //     dayOfWeek: '7',
    //     startTime: '00:00',
    //     endTime: '00:00',
    //     validTimes: true
    // };
    // let hero2 = {
    //     name: 'hero2',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'one-time',
    //     startDateTime: '12/31/2018 23:00',
    //     endDateTime: '01/01/2019 04:00'
    // }
    // heroes.push(hero1);
    // heroes.push(hero2);
    // timeSaved.minutesSaved = 60;
    // timeSaved.lastUpdated = '01/01/2019 00:00:00';

    // TEST: ongoing active block with lastUpdated after startTime
    // let hero1 = {
    //     name: 'hero1',
    //     docType: 'hero',
    //     blockedSites: ['twitter.com'],
    //     exceptions: [],
    //     blockType: 'ongoing',
    //     schedule: {
    //         blockDays: {},
    //         validSchedule: true
    //     }
    // }
    // let now = moment();
    // let now2 = now.clone();
    // now.seconds(0);
    // now.milliseconds(0);
    // let blockDayStart = now.clone();
    // blockDayStart.subtract('10', 'minutes');
    // let blockDayEnd = now.clone();
    // blockDayEnd.add('2', 'minutes');
    // now2.subtract('35', 'seconds');
    // let lastUpdatedString = now2.format('MM/DD/YYYY HH:mm:ss');
    
    // let dayOfWeekString = now.isoWeekday().toString();
    // hero1.schedule.blockDays[dayOfWeekString] = {
    //     dayOfWeek: dayOfWeekString,
    //     startTime: blockDayStart.format('HH:mm'),
    //     endTime: blockDayEnd.format('HH:mm'),
    //     lastUpdated: lastUpdatedString,
    //     validTimes: true
    // };

    // heroes.push(hero1);
    // timeSaved.minutesSaved = 0;
    // timeSaved.lastUpdated = '01/18/2019 00:00:00';

    timeSavedCache.setTimeSaved(timeSaved);
    return heroes;
}

exports.insertTestHeroesIntoDb = insertTestHeroesIntoDb;