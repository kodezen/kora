const { autoUpdater } = require('electron-updater');
const { ipcMain } = require('electron');
const ipcService = require('./ipc-service.js');
const uninstallService = require('./uninstall-service.js');
var electronWindow = require('../electron-win.js');
const moment = require('moment');
const to = require('await-to-js').default;
const log = require('electron-log');

var lastUpdateCheckDT;
var updateAvailableSender;

ipcMain.on('update-available-received', (event) => {
    clearInterval(updateAvailableSender);
});

ipcMain.on('update', async (event) => {
    log.info('User initiated update');
    let updateFileErr, successful;
    [ updateFileErr, successful ] = await to(uninstallService.createUpdateFileForAutoUpdate());
    if (updateFileErr) {
        ipcService.send('error-downloading-update');
        return;
    }
    let err, path;
    [err, path] = await to(autoUpdater.downloadUpdate());
    if (err) {
        log.error('Update download error: ', err);
        ipcService.send('error-downloading-update');
    } else {
        log.info('Update downloaded successfully');
    }
});

function initialize() {
    configureAutoUpdater();
    setTimeout(() => {
        checkForUpdates();
        setInterval(updateChecker, 60 * 60000);
    }, 2 * 60000)
}

function checkForUpdates() {
    console.log('Checking for updates');
    lastUpdateCheckDT = moment();
    autoUpdater.checkForUpdates();
}

function updateChecker() {
    let now = moment();
    let updateCheckRequired = (now.diff(lastUpdateCheckDT, 'days') > 0);
    if (updateCheckRequired) {
        checkForUpdates();
    }
}

function configureAutoUpdater() {
    autoUpdater.autoDownload = false;
    autoUpdater.autoInstallOnAppQuit = false;

    autoUpdater.on('checking-for-update', () => {
        log.info('Checking for update...');
    });
    autoUpdater.on('update-available', (info) => {
        log.info('Update available');
        electronWindow.show();
        sendUpdateAvailable();
    });
    autoUpdater.on('update-not-available', (info) => {
        log.info('Update not available.');
        uninstallService.deleteUpdateFile();
    });
    autoUpdater.on('update-downloaded', (info) => {
        log.info('Update downloaded');
        safeQuitAndInstall();
    });
    autoUpdater.on('error', (err) => {
        log.error('Error in auto-updater:', err);
    });
}

function sendUpdateAvailable() {
    let channel = 'update-available';
    send(channel);
    updateAvailableSender = setInterval(send, 500, channel);
}

function send(channel) {
    ipcService.send(channel);
}

function safeQuitAndInstall() {
    console.log('Trying to quit and install');
    if (electronWindow.getWindow() != null) {
        electronWindow.getWindow().removeAllListeners('close');
    }
    autoUpdater.quitAndInstall();
}

exports.initialize = initialize;