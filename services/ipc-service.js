var electronWindow = require('../electron-win.js');

const KORA_FRONT_END = 'kora-front-end';
const KORA_BACK_END = 'kora-back-end';

function send(channel, data) {
    if (electronWindow.getWindow() != null) {
        let message = {
            type: KORA_BACK_END,
            channel: channel, 
        };
        if (data != null) {
            message.data = data;
        }
        electronWindow.getWindow().webContents.send(KORA_BACK_END, message);
    }
}

exports.KORA_FRONT_END = KORA_FRONT_END;
exports.KORA_BACK_END = KORA_BACK_END;
exports.send = send;