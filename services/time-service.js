const moment = require('moment');
const got = require('got');
const log = require('electron-log');

async function syncTime() {
    try {
        const response = await got('https://evening-scrubland-10274.herokuapp.com/time');
        let serverTime = response.body;
        let systemTime = Date.now();
        let offset = serverTime - systemTime;
        moment.now = function() {
            let currentTime = Date.now() + offset;
            return currentTime;
        }
        // console.log('server-time: ' + serverTime);
        // console.log('system-time: ' + systemTime);
        // console.log('offset: ' + offset);
    } catch (err) {
        log.error(err);
    }
}

exports.syncTime = syncTime;