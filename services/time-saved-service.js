var database = require('../database/database.js');
var timeSavedCache = require('../database/time-saved-cache.js');
var timeSavedIpcService = require('../ipc/main/time-saved.js');
var moment = require('moment');
const log = require('electron-log');

function getTimeSaved() {
    let timeSaved = timeSavedCache.getTimeSaved();
    return timeSaved;
}

function getTimeSavedFromDb() {
    return new Promise(function(resolve, reject) {
        database.getOne({ docType: 'time-saved' }, (err, timeSavedDoc) => {
            if (err) {
                log.error(err);
            }
            if (timeSavedDoc == null) {
                let now = moment();
                let initialTimeSavedRecord = {
                    minutesSaved: 0,
                    docType: 'time-saved',
                    lastUpdated: now.format('MM/DD/YYYY HH:mm:ss')
                }
                database.insert(initialTimeSavedRecord, (err, insertedTimeSavedDoc) => {
                    if (err) {
                        log.error(err);
                        reject(err);
                    } else {
                        resolve(insertedTimeSavedDoc);
                    }
                });
            }
            else {
                resolve(timeSavedDoc);
            }
        });
    });
}

function clearTimeSaved() {
    return new Promise(function(resolve, reject) {
        database.deleteAll({ docType: 'time-saved' }, (err, numRemoved) => {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                resolve("done");
            }
        });
    });
}

function incrementTimeSaved(now) {
    let timeSaved = timeSavedCache.getTimeSaved();
    timeSaved.minutesSaved++;
    updateTimeSaved(timeSaved, now);
}

function updateTimeSaved(timeSaved, now) {
    timeSaved.lastUpdated = now.format('MM/DD/YYYY HH:mm:ss');
    timeSavedCache.setTimeSaved(timeSaved);
    timeSavedIpcService.sendMinutesSaved(timeSaved.minutesSaved);
    database.update({ docType: 'time-saved' }, timeSaved, (err, numReplaced) => {
        if (err) {
            log.error(err);
        }
    });
}

function timeSavedCatchup(now, heroes) {
    return new Promise(function (resolve, reject) {
        try {
            let timeSaved = timeSavedCache.getTimeSaved();
            if (timeSaved != null) {
                let minutesSavedCaughtUp = timeSaved.minutesSaved;
                let lastUpdated = moment(timeSaved.lastUpdated, 'MM/DD/YYYY HH:mm:ss');
                let oneTimeHeroes = [];
                let ongoingHeroes = [];
                for (var i = 0; i < heroes.length; i++) {
                    let hero = heroes[i];
                    if (hero.blockType === 'one-time') {
                        oneTimeHeroes.push(hero);
                    } else if (hero.blockType === 'ongoing') {
                        ongoingHeroes.push(hero);
                    }
                }
                let blocksToAccountFor = [];
                if (oneTimeHeroes.length > 0) {
                    blocksToAccountFor.push(...oneTimeBlocksToAccountFor(oneTimeHeroes, lastUpdated));
                }
                if (ongoingHeroes.length > 0) {
                    blocksToAccountFor.push(...ongoingBlocksToAccountFor(ongoingHeroes, lastUpdated, now));
                }
                if (blocksToAccountFor.length > 0) {
                    sortBlocksByDateTime(blocksToAccountFor);
                    let date = lastUpdated;
                    var caughtUp = false;
                    while (!caughtUp) {
                        unAccountedForBlock = blocksToAccountFor.shift();
                        let startDateTime = unAccountedForBlock.startDateTime;
                        let endDateTime = unAccountedForBlock.endDateTime;
                        var end;
                        if (now.isBefore(endDateTime)) {
                            end = now.clone();
                            end.seconds(0);
                            end.milliseconds(0);
                        } else {
                            end = endDateTime;
                        }
                        var blockedMinutes;
                        if (date.isBetween(startDateTime, endDateTime, null, '[)')) {
                            let blockedDurationSeconds = end.diff(date, 'seconds');
                            blockedMinutes = Math.floor(blockedDurationSeconds / 60);
                            if (now.isSameOrAfter(endDateTime)) {
                                if ((blockedDurationSeconds % 60) > 0) {
                                    blockedMinutes++;
                                }
                            }
                        } else if (date.isBefore(startDateTime)) {
                            blockedMinutes = end.diff(startDateTime, 'minutes');
                        }
                        if (blockedMinutes > 0) {
                            minutesSavedCaughtUp += blockedMinutes;
                        }
                        removeBlocksEndingBeforeDateTime(blocksToAccountFor, end);
                        date = end;
                        caughtUp = (now.isSameOrBefore(endDateTime) || (blocksToAccountFor.length == 0));
                    }
                    timeSaved.minutesSaved = minutesSavedCaughtUp;
                    updateTimeSaved(timeSaved, now);
                }
            }
            resolve("done");
        } catch (err) {
            log.error(err);
            reject(err);
        }
    });
}

function oneTimeBlocksToAccountFor(heroes, lastUpdated) {
    let blocks = [];
    for (var i = 0; i < heroes.length; i++) {
        let hero = heroes[i];
        let start = moment(hero.startDateTime, 'MM/DD/YYYY HH:mm:ss');
        let end = moment(hero.endDateTime, 'MM/DD/YYYY HH:mm:ss');
        if (lastUpdated.isBetween(start, end, null, '[)')) {
            let block = {
                startDateTime: start,
                endDateTime: end
            }
            blocks.push(block);
        }
    }
    return blocks;
}

function ongoingBlocksToAccountFor(ongoingHeroes, lastUpdated, now) {
    let date = lastUpdated.clone();
    let blocks = [];
    let days = now.diff(date, 'days') + 1;
    for (var i = 0; i <= days; i++) {
        let dayOfWeek = date.isoWeekday().toString();
        let heroesWithBlockDay = getOngoingHeroesWithBlockDay(ongoingHeroes, dayOfWeek);
        for (var j = 0; j < heroesWithBlockDay.length; j++) {
            let hero = heroesWithBlockDay[j];
            let blockDay = hero.schedule.blockDays[dayOfWeek];
            let activeBlock = blockActiveInBetweenDateTimes(blockDay, date, lastUpdated, now);
            if (activeBlock) {
                let startString = date.format('MM/DD/YYYY') + ' ' + blockDay.startTime;
                let start = moment(startString, 'MM/DD/YYYY HH:mm');
                var endDate = date.clone();
                // special case: all day block
                if (blockDay.endTime === '00:00') {
                    endDate.add('1', 'days');
                }
                let endString = endDate.format('MM/DD/YYYY') + ' ' + blockDay.endTime;
                let end = moment(endString, 'MM/DD/YYYY HH:mm');
                let block = {
                    startDateTime: start,
                    endDateTime: end
                }
                blocks.push(block);
            }
        }
        date.add(1, 'days');
    }
    return blocks;
}

function getOngoingHeroesWithBlockDay(heroes, dayOfWeek) {
    let heroesWithBlockDay = [];
    for (var i = 0; i < heroes.length; i++) {
        let hero = heroes[i];
        let blockDays = hero.schedule.blockDays;
        if (blockDays[dayOfWeek] != undefined) {
            heroesWithBlockDay.push(hero);
        }
    }
    return heroesWithBlockDay;
}

function blockActiveInBetweenDateTimes(blockDay, date, lastUpdated, now) {
    let startString = date.format('MM/DD/YYYY') + ' ' + blockDay.startTime;
    let start = moment(startString, 'MM/DD/YYYY HH:mm');
    var endDate = date.clone();
    // special case: all day block
    if (blockDay.endTime === '00:00') {
        endDate.add('1', 'days');
    } 
    let endString = endDate.format('MM/DD/YYYY') + ' ' + blockDay.endTime;
    let end = moment(endString, 'MM/DD/YYYY HH:mm');
    if (end.isBefore(lastUpdated) || start.isAfter(now)) {
        return false;
    } else {
        return true;
    }
}

function sortBlocksByDateTime(blocks) {
    blocks.sort(blockDateTimeComparator);
}

function blockDateTimeComparator(block1, block2) {
    if (block1.startDateTime.isBefore(block2.startDateTime)) {
        return -1;
    } else if (block1.startDateTime.isAfter(block2.startDateTime)) {
        return 1;
    } else {
        if (block1.endDateTime.isBefore(block2.endDateTime)) {
            return -1;
        } else if (block1.endDateTime.isAfter(block2.endDateTime)) {
            return 1;
        }
        return 0;
    }
}

function removeBlocksEndingBeforeDateTime(blocks, dateTime) {
    let blockIndexesToRemove = [];
    for (var i = 0; i < blocks.length; i++) {
        let block = blocks[i];
        let end = block.endDateTime;
        // inclusive
        if (end.isSameOrBefore(dateTime)) {
            blockIndexesToRemove.push(i);
        }
    }
    for (var i = blockIndexesToRemove.length - 1; i >= 0; i--) {
        let index = blockIndexesToRemove[i];
        blocks.splice(index, 1);
    }
}

exports.getTimeSaved = getTimeSaved;
exports.getTimeSavedFromDb = getTimeSavedFromDb;
exports.clearTimeSaved = clearTimeSaved;
exports.timeSavedCatchup = timeSavedCatchup;
exports.incrementTimeSaved = incrementTimeSaved;