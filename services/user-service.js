var database = require('../database/database.js');
var moment = require('moment');
const to = require('await-to-js').default;
const log = require('electron-log');

let user;
let newUser;

async function initialize() {
    let err, userDb;
    [err, userDb] = await to(getUserDb());
    if (err || (userDb === null)) {
        user = genNewUser();
    } else {
        user = userDb;
    }
    console.log(user);
}

function getUser() {
    return user;
}

function setUser(userToSet) {
    user = userToSet;
}

function getTrialExpirationDT() {
    return user.trialExpirationDT;
}

function setTrialExpirationDT(trialExpirationDT) {
    user.trialExpirationDT = trialExpirationDT;
}

function genNewUser() {
    newUser = true;
    let now = moment();
    let trialExpirationDT = moment().add(7, 'days');
    let dateTimeFormat = 'MM/DD/YYYY HH:mm:ss';
    let user = {
        createdDT: now.format(dateTimeFormat),
        trialExpirationDT: trialExpirationDT.format(dateTimeFormat),
        chromeSetupCompleted: false, 
        firstBlockCreated: false
    };
    storeUser(user);
    return user;
};

function getUserDb() {
    return new Promise(function(resolve, reject){
        database.getOne({ docType: 'user' }, (err, user) => {
            if (err) {
                log.error(err);
                reject(err);
            } else {
                resolve(user);
            }
        });
    });
}

function storeUser(userToStore) {
    userToStore.docType = 'user';
    database.insert(userToStore, function (err, newDoc) {
        if (err) {
            log.error(err);
        }
    });
}

function isNewUser() {
    return newUser;
}

function isFreeTrialUser() {
    return !isPaidUser();
}

function getTrialTimeRemaining() {
    let trialExpirationDT = getTrialExpirationDT();
    trialExpirationDT = moment(trialExpirationDT, 'MM/DD/YYYY HH:mm:ss');
    let now = moment();
    let timeRemainingDuration = moment.duration(trialExpirationDT.diff(now)).asSeconds();
    return timeRemainingDuration;
}

function storePremiumCode(code) {
    user.premiumCode = code;
    updateUserDb(user);
}

function isPaidUser() {
    return (user.premiumCode != null);
}

function getPremiumCode() {
    return user.premiumCode;
}

function getMixpanelId() {
    return user.mixpanelId;
}

function storeMixpanelId(mixpanelId) {
    user.mixpanelId = mixpanelId;
    updateUserDb(user);
}

function isChromeSetupCompleted(completed) {
    return user.chromeSetupCompleted;
}

function storeChromeSetupCompleted(completed) {
    user.chromeSetupCompleted = completed;
    updateUserDb(user);
}

function isFirstBlockCreated() {
    return user.firstBlockCreated;   
}

function storeFirstBlockCreated(firstBlockCreated) {
    user.firstBlockCreated = firstBlockCreated;
    updateUserDb(user);
}

function isRestricted() {
    return user.restricted;
}

function storeRestricted(restricted) {
    user.restricted = restricted;
    updateUserDb(user);
}

function updateUserDb(userToUpdate) {
    database.update({ docType: 'user' }, userToUpdate, (err, numReplaced) => {
        if (err) {
            log.error(err);
        }
    });
}

exports.initialize = initialize;
exports.getUser = getUser;
exports.setUser = setUser;
exports.getTrialExpirationDT = getTrialExpirationDT;
exports.setTrialExpirationDT = setTrialExpirationDT;
exports.genNewUser = genNewUser;
exports.getUserDb = getUserDb;
exports.storeUser = storeUser;
exports.isNewUser = isNewUser;
exports.isFreeTrialUser = isFreeTrialUser;
exports.getTrialTimeRemaining = getTrialTimeRemaining;
exports.storePremiumCode = storePremiumCode;
exports.isPaidUser = isPaidUser;
exports.getPremiumCode = getPremiumCode;
exports.getMixpanelId = getMixpanelId
exports.storeMixpanelId = storeMixpanelId;
exports.isChromeSetupCompleted = isChromeSetupCompleted;
exports.storeChromeSetupCompleted = storeChromeSetupCompleted;
exports.isFirstBlockCreated = isFirstBlockCreated;
exports.storeFirstBlockCreated = storeFirstBlockCreated;
exports.isRestricted = isRestricted;
exports.storeRestricted = storeRestricted;