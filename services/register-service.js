const got = require('got');
const to = require('await-to-js').default;
const log = require('electron-log');

function registerUser() {
    return new Promise(async function(resolve, reject) {
        let options = {
            headers: { 'Content-type': 'application/json' },
            body: JSON.stringify({})
        };
        let err, response;
        [ err, response ] = await to(got('https://evening-scrubland-10274.herokuapp.com/user/register', options));
        if (err) {
            return reject(err);
        }
        let message = JSON.parse(response.body);
        if (message.type === 'SUCCESS') {
            let user = message.data;
            if (user) {
                return resolve(user);
            }
        }
        return reject('Error registering user');
    });

}

exports.registerUser = registerUser;