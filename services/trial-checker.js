const { ipcMain } = require('electron');
var moment = require('moment');
const trialService = require('./trial-service.js');
const userService = require('./user-service.js');
const ipcService = require('./ipc-service.js');
const eventService = require('./event-service.js');
const actionService = require('./action-service.js');
const electronWindow = require('../electron-win.js');

var activeTrialChecker;
var showTrialExpiringReminderSender;
var trialExpiredSender;

function start() {
    let freeTrialExpiredFileExists = trialService.checkFreeTrialExpiredFileExists();
    if (freeTrialExpiredFileExists) {
        trialService.handleExpiredTrial();
        stopCheckerAndNotifyUser();
        return;
    }
    if (trialService.isTrialActive()) {
        if (isTrialExpiringSoon()) {
            onTrialExpiringSoon();
        }
        activeTrialChecker = setInterval(checkTrialStillActive, 60 * 60000);
        running = true;
    } else {
        trialService.onTrialExpired();
        stopCheckerAndNotifyUser();
    }
}

function stop() {
    clearInterval(activeTrialChecker);
    running = false;
}

function isTrialExpiringSoon() {
    let trialTimeRemaining = userService.getTrialTimeRemaining();
    trialTimeRemaining = moment.duration(trialTimeRemaining, 'seconds');
    let trialExpiresSoon = (trialTimeRemaining.asDays() < 2);
    return trialExpiresSoon;
}

function onTrialExpiringSoon() {
    let remindedUser = actionService.queryAction(actionService.SHOWED_TRIAL_EXPIRING_REMINDER);
    if (!remindedUser) {
        startShowTrialExpiringReminderSender();
        actionService.registerAction(actionService.SHOWED_TRIAL_EXPIRING_REMINDER);
        eventService.track(eventService.SHOWED_TRIAL_EXPIRING_REMINDER, {});
    }
}

function checkTrialStillActive() {
    let trialExpired = !trialService.isTrialActive();
    if (trialExpired) {
        trialService.onTrialExpired();
        stopCheckerAndNotifyUser();
    }
}

function stopCheckerAndNotifyUser() {
    stop();
    startTrialExpiredSender();
}

function startShowTrialExpiringReminderSender() {
    let channel = 'show-trial-expiring-reminder';
    send(channel);
    showTrialExpiringReminderSender = setInterval(send, 500, channel);
}

function startTrialExpiredSender() {
    let channel = 'trial-expired';
    send(channel);
    trialExpiredSender = setInterval(send, 500, channel);
}

function send(channel) {
    ipcService.send(channel);
}

ipcMain.on('show-trial-expiring-reminder-received', (event) => {
    electronWindow.show();
    clearInterval(showTrialExpiringReminderSender);
});

ipcMain.on('trial-expired-received', (event) => {
    electronWindow.show();
    clearInterval(trialExpiredSender);
});

exports.start = start;
exports.stop = stop;
exports.stopCheckerAndNotifyUser = stopCheckerAndNotifyUser;


