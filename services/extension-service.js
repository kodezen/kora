var version;
var connected;

function getExtensionVersion() {
    return version;
}

function setExtensionVersion(extensionVersion) {
    version = extensionVersion;
}

function isExtensionConnected() {
    return connected;
}

function setExtensionConnected(connectedValue) {
    connected = connectedValue;
}

exports.getExtensionVersion = getExtensionVersion;
exports.setExtensionVersion = setExtensionVersion;
exports.isExtensionConnected = isExtensionConnected;
exports.setExtensionConnected = setExtensionConnected;