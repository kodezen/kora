var database = require('../database/database.js');
const to = require('await-to-js').default;
const log = require('electron-log');

let actions;

async function initialize() {
    let err, actionsDb;
    [err, actionsDb] = await to(getActionsDb());
    if (err || (actionsDb === null)) {
        actions = genNewActions();
    } else {
        actions = actionsDb;
    }
    console.log('actions:');
    console.log(actions);
}

function getActionsDb() {
    return new Promise(function(resolve, reject) {
        database.getOne({ docType: 'actions' }, (err, actionsDoc) => {
            if (err) {
                log.error(err);
                reject(err);
            } else {
                resolve(actionsDoc);
            }
        });
    });
}

function genNewActions() {
    let actions = {}
    storeActions(actions);
    return actions;
}

function storeActions(actionsToStore) {
    actionsToStore.docType = 'actions';
    database.insert(actionsToStore, (err, actionsDoc) => {
        if (err) {
            log.error(err);
        }
    });
}

function registerAction(actionName, actionDetails) {
    if (!actionDetails) {
        actions[actionName] = true;
    } else {
        actions[actionName] = actionDetails;
    }
    updateActionsDb(actions);
}

function queryAction(actionName) {
    return actions[actionName];
}

function updateActionsDb(actionsToUpdate) {
    database.update({ docType: 'actions' }, actionsToUpdate, (err, numReplaced) => {
        if (err) {
            log.error(err);
        }
    });
}

exports.initialize = initialize
exports.registerAction = registerAction;
exports.queryAction = queryAction;

exports.FIRST_BLOCK_CREATED = 'firstBlockCreated';
exports.TRIAL_OFFER_FIRST_CLICK = 'trialOfferFirstClick';
exports.SHOWED_TRIAL_EXPIRING_REMINDER = 'showedTrialExpiringReminder';
exports.INITIATED_TRIAL_EXPIRED_OFFER = 'initiatedTrialExpiredOffer';