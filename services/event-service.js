const got = require('got');
const to = require('await-to-js').default;
const log = require('electron-log');
var Mixpanel = require('mixpanel');
var mixpanel = Mixpanel.init("b286cf27f8592afd87abad7d3758b7c6");
const versionDetailsService = require('./version-details-service.js');

let enabled = false;
let permission = false;
let mixpanelId;

async function initialize(isEnabled, user, newUser) {
    enabled = isEnabled;
    if (enabled) {
        // TODO: handle this more elegantly 
        if (newUser) {
            track('New User', {});
        }
        mixpanelId = user.mixpanelId;
        if (mixpanelId) {
            let notRestricted = !(user.restricted);
            if (notRestricted) {
                await verifyRemoteTrackingPermission(mixpanelId);
            }
        }
    }
}

async function verifyRemoteTrackingPermission(mixpanelId) {
    let options = {
        headers: { 'Content-type': 'application/json' },
        body: JSON.stringify({mixpanelId: mixpanelId})
    };
    let err, response;
    [ err, response ] = await to(got('https://evening-scrubland-10274.herokuapp.com/user/verify-tracking-permission', options));
    if (err) {
        log.error(err);
        return;
    }
    let message = JSON.parse(response.body);
    if (message.type === 'SUCCESS') {
        let havePermission = message.data;
        if (havePermission) {
            setPermission(true);
        } else {
            userService.storeRestricted(true);
        }
    }
}

function setPermission(havePermission) {
    permission = havePermission;
}

function setMixpanelId(id) {
    mixpanelId = id;
}

function track(eventName, event) {
    if (enabled) {
        if (permission) {
            event.distinct_id = mixpanelId;
        }
        event.version = versionDetailsService.version;
        event.codeName = versionDetailsService.codeName;
        mixpanel.track(eventName, event);
    }
}

function trackUserOnlyEvent(eventName, event) {
    if (enabled) {
        if (permission && mixpanelId) {
            event.distinct_id = mixpanelId;
            event.version = versionDetailsService.version;
            event.codeName = versionDetailsService.codeName;
            mixpanel.track(eventName, event);
        }
    }
}

function createPeopleProfile() {
    if (enabled) {
        if (permission) {
            mixpanel.people.set(mixpanelId, {
                plan: 'trial',
                version: versionDetailsService.version,
                codeName: versionDetailsService.codeName
            });
        }
    }
}

function updatePeopleProfile(peopleProfile) {
    if (enabled) {
        if (permission) {
            peopleProfile.version = versionDetailsService.version;
            peopleProfile.codeName = versionDetailsService.codeName;
            mixpanel.people.set(mixpanelId, peopleProfile);
        }
    }
}

exports.initialize = initialize;
exports.setPermission = setPermission;
exports.setMixpanelId = setMixpanelId;
exports.track = track;
exports.trackUserOnlyEvent = trackUserOnlyEvent;
exports.createPeopleProfile = createPeopleProfile;
exports.updatePeopleProfile = updatePeopleProfile;

// USER FLOW EVENTS
exports.NEW_USER = 'New User';
exports.FIRST_BLOCK_FILLED_OUT = 'First Block Filled Out';
exports.CHROME_NOT_INSTALLED = 'Chrome Not Installed';
exports.EXTENSION_SETUP_STARTED = 'Extension Setup Started';
exports.EXTENSION_SETUP_FINISHED = 'Extension Setup Finished';
exports.SETUP_FINISHED = 'Setup Finished';
exports.FIRST_BLOCK_CREATED = 'First Block Created';

exports.UPGRADE_PLANS_VIEWED = 'Upgrade Plans Viewed';
exports.UPGRADE_ACTIVATED_CODE = 'Upgrade Activated Code';

exports.TRIAL_OFFER_CLICKED = 'Trial Offer Clicked';
exports.TRIAL_OFFER_PLANS_VIEWED = 'Trial Offer Plans Viewed';
exports.TRIAL_OFFER_ACTIVATED_CODE = 'Trial Offer Activated Code';

exports.SHOWED_TRIAL_EXPIRING_REMINDER = 'Showed Trial Expiring Reminder';
exports.TRIAL_EXPIRING_REMINDER_CLICKED_CODE = 'Trial Expiring Reminder Clicked Code';
exports.TRIAL_EXPIRING_REMINDER_CLICKED_DASHBOARD = 'Trial Expiring Reminder Clicked Dashboard';

exports.TRIAL_EXPIRED = 'Trial Expired';
exports.TRIAL_EXPIRED_PLANS_VIEWED = 'Trial Expired Plans Viewed';
exports.TRIAL_EXPIRED_ACTIVATED_CODE = 'Trial Expired Activated Code';

// RETENTION EVENTS
exports.WEBSITE_BLOCKED = 'Website Blocked';
exports.BLOCK_ACTIVE = 'Block Active';
exports.BLOCK_CREATED = 'Block Created';
exports.BLOCK_STARTED = 'Block Started';
exports.BLOCK_EDITED = 'Block Edited';
exports.BLOCK_DELETED = 'Block Deleted';