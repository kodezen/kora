var fs = require('fs');
const appDataPaths = require('./app-data-paths.js');
var heroCache = require('../database/hero-cache.js');
const log = require('electron-log');

const filePath = appDataPaths.uninstallFilePath;
const updateFilePath = appDataPaths.updateFilePath;
const uninstallBlockedKey = '56ecc8738deab48';
const uninstallAllowedKey = '173f5126570d361';
var uninstallBlocked;

function initialize() {
    uninstallBlocked = false;
    try {
        let uninstallFileExists = fs.existsSync(filePath);
        if (!uninstallFileExists) {
            fs.writeFile(filePath, uninstallAllowedKey, (err) => {
                if (err) {
                    log.error(err)
                }
            });
        } else {
            var contents = fs.readFileSync(filePath, 'utf8');
            if (contents === uninstallBlockedKey) {
                uninstallBlocked = true;
            }
        }
    } catch (err) {
        log.error(err);
    }

}

function runActiveHeroCheck() {
    let activeHeroes = heroCache.getActiveHeroes();
    let activeHeroesExist = (activeHeroes.length > 0);
    if (activeHeroesExist) {
        if (!uninstallBlocked) {
            blockUninstall();
        }
    } else {
        if (uninstallBlocked) {
            allowUninstall();
        }
    }
}

function blockUninstall() {
    uninstallBlocked = true;
    fs.writeFile(filePath, uninstallBlockedKey, (err) => {
        if (err) {
            log.error(err);
        }
    });  
}

function allowUninstall() {
    uninstallBlocked = false;
    fs.writeFile(filePath, uninstallAllowedKey, (err) => {
        if (err) {
            log.error(err);
        }
    });   
}

function createUpdateFileForAutoUpdate() {
    return new Promise((resolve, reject) => {
        fs.writeFile(updateFilePath, '', (err) => {
            if (err) {
                log.error(err);
                return reject(err);
            }
            log.info('Created update file for update.');
            resolve(true);
        });  
    });
}

function deleteUpdateFile() {
    fs.unlink(updateFilePath, (err) => {
        if (err) {
            log.error(err);
        } else {
            log.info('Deleted update file.');
        }
    });
}

exports.initialize = initialize;
exports.runActiveHeroCheck = runActiveHeroCheck;
exports.blockUninstall = blockUninstall;
exports.allowUninstall = allowUninstall;
exports.createUpdateFileForAutoUpdate = createUpdateFileForAutoUpdate;
exports.deleteUpdateFile = deleteUpdateFile;