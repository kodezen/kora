const fs = require('fs');
const programFiles86 = process.env['ProgramFiles(x86)'];
const chromePath86 = `${programFiles86}\\Google\\Chrome\\Application\\chrome.exe`;

function detectChrome() {
  return new Promise((resolve, reject) => {
      fs.access(chromePath86, (err) => {
        if (!err) {
          resolve(true);
        } else {
          reject(false);
        }
      });
    
  });
}

exports.detectChrome = detectChrome;
