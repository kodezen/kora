var heroCache = require('../database/hero-cache.js');
var heroService = require('./hero-service.js');
var timeService = require('./time-service.js');
var timeSavedService = require('./time-saved-service.js');
var uninstallService = require('./uninstall-service.js');
var heroIpcService = require('../ipc/main/hero.js');
var server = require('../server/server.js');
var moment = require('moment');

var lastUpdated;

function start(cacheUpdatedTime) {
    let now = moment();
    let cacheUpdatedInPreviousMinute = (now.diff(cacheUpdatedTime, 'minutes') > 0);
    if (cacheUpdatedInPreviousMinute) {
        update(now);
    } else {
        // cache was updated in this minute
        now.seconds(0);
        now.milliseconds(0);
        lastUpdated = now;
    }
    setInterval(autoUpdate, 3 * 1000);
}

async function autoUpdate() {
    let now = moment();
    let duration = moment.duration(now.diff(lastUpdated));
    let millisecondsSinceLastUpdate = Math.abs(duration.asMilliseconds());
    // console.log('lastUpdated: ' + lastUpdated.format('HH:mm:ss') + ' milliseconds: ' + millisecondsSinceLastUpdate + ' now: ' + now.format('HH:mm:ss'));
    let minutePassed = (millisecondsSinceLastUpdate >= (60 * 1000));
    if (minutePassed) {
        let updaterAnomaly = (millisecondsSinceLastUpdate > (75 * 1000)); // pc was asleep or system time changed
        if (!updaterAnomaly) {
            update(now);
        }
        else {
            console.log('updated anomoly detected:')
            console.log('before time-sync: ' + now.format('HH:mm:ss'));
            await timeService.syncTime();
            now = moment();
            console.log('after time-sync: ' + now.format('HH:mm:ss'));
            now.seconds(0);
            now.milliseconds(0);
            if (now.isAfter(lastUpdated)) {
                heroUpdaterCatchup(now);
                update(now);
            }
        }
    }
}

function update(now) {
    now.seconds(0);
    now.milliseconds(0);
    console.log('Updating: ' + now.format('MMM DD YYYY HH:mm:ss'));
    let heroesThatWereActive = heroCache.getActiveHeroes();
    let heroesThatRemainedActive = heroService.getActiveHeroes(heroesThatWereActive, now);
    let activeHeroesTurnedScheduled = [];
    let activeHeroesTurnedResting = [];
    let scheduledHeroesTurnedActive = [];
    let restingHeroesTurnedActive = [];
    populateStatusChangeLists(activeHeroesTurnedScheduled, activeHeroesTurnedResting, scheduledHeroesTurnedActive, restingHeroesTurnedActive, now);
    if (heroesThatWereActive.length > 0) {
        updateTimeSaved(heroesThatRemainedActive, activeHeroesTurnedResting,
            activeHeroesTurnedScheduled, now);
    }
    let heroes = heroCache.getHeroes();
    let currentlyActiveHeroes = heroService.getActiveHeroes(heroes, now);
    let allActiveHeroesTurnedInactive = ((heroesThatWereActive.length > 0) && (currentlyActiveHeroes.length == 0));
    if (allActiveHeroesTurnedInactive) {
        uninstallService.allowUninstall();
    }
    let firstHeroToBecomeActive = ((heroesThatWereActive.length == 0) && ((scheduledHeroesTurnedActive.length > 0) || (restingHeroesTurnedActive.length > 0)));
    if (firstHeroToBecomeActive) {
        uninstallService.blockUninstall();
    }
    handleHeroesThatRemainedActive(heroesThatRemainedActive, now);
    handleActiveToScheduled(activeHeroesTurnedScheduled, now);
    handleActiveToResting(activeHeroesTurnedResting);
    handleScheduledToActive(scheduledHeroesTurnedActive, now);
    handleRestingToActive(restingHeroesTurnedActive, now);

    let heroesBecameActive = (scheduledHeroesTurnedActive.length > 0 || restingHeroesTurnedActive.length > 0);
    if (heroesBecameActive) {
        server.initiateBlockQueryAllTabs();
    }

    let updatedOneTimeHeroes = [];
    let updatedOngoingHeroes = [];
    for (var i = 0; i < heroesThatRemainedActive.length; i++) {
        let hero = heroesThatRemainedActive[i];
        if (hero.blockType === 'one-time') {
            updatedOneTimeHeroes.push(hero);
        } else if (hero.blockType === 'ongoing') {
            updatedOngoingHeroes.push(hero);
        }
    }
    for (var i = 0; i < restingHeroesTurnedActive.length; i++) {
        let hero = restingHeroesTurnedActive[i];
        updatedOneTimeHeroes.push(hero);
    }
    for (var i = 0; i < scheduledHeroesTurnedActive.length; i++) {
        let hero = scheduledHeroesTurnedActive[i];
        updatedOngoingHeroes.push(hero);
    }
    for (var i = 0; i < activeHeroesTurnedScheduled.length; i++) {
        let hero = activeHeroesTurnedScheduled[i];
        updatedOngoingHeroes.push(hero);
    }
    for (var i = 0; i < activeHeroesTurnedResting.length; i++) {
        let hero = activeHeroesTurnedResting[i];
        updatedOneTimeHeroes.push(hero);
    }
    heroIpcService.sendOneTimeHeroes(updatedOneTimeHeroes);
    heroIpcService.sendOngoingHeroes(updatedOngoingHeroes);
    lastUpdated = now;
}

function heroUpdaterCatchup(now) {
    let previousMinute = now.clone();
    previousMinute.subtract(1, 'minutes');
    previousMinute.seconds(0);
    previousMinute.milliseconds(0);
    heroCache.reSortHeroes(previousMinute);
    uninstallService.runActiveHeroCheck();
    let heroes = heroCache.getHeroes();
    timeSavedService.timeSavedCatchup(previousMinute, heroes);
    heroIpcService.sendHeroes();
}

function updateTimeSaved(heroesThatRemainedActive, activeHeroesTurnedResting, activeHeroesTurnedScheduled, now) {
    if (activeHeroesTurnedResting.length > 0) {
        timeSavedService.incrementTimeSaved(now);
    } else if (activeHeroesTurnedScheduled.length > 0) {
        timeSavedService.incrementTimeSaved(now);
    } else if (heroesThatRemainedActive.length > 0) {
        let containsOngoingHero = heroService.containsOngoingHero(heroesThatRemainedActive);
        if (containsOngoingHero) {
            timeSavedService.incrementTimeSaved(now);
        } else {
            let containsHeroActiveForAMinute = heroService.containsHeroActiveForAMinute(heroesThatRemainedActive, now);
            if (containsHeroActiveForAMinute) {
                timeSavedService.incrementTimeSaved(now);
            }
        }    
    }
}

function handleHeroesThatRemainedActive(heroesThatRemainedActive, now) {
    for (var i = 0; i < heroesThatRemainedActive.length; i++) {
        let hero = heroesThatRemainedActive[i];
        heroService.updateTimeRemaining(hero, now);
    }
}

function populateStatusChangeLists(activeHeroesTurnedScheduled, activeHeroesTurnedResting, scheduledHeroesTurnedActive, restingHeroesTurnedActive, now) {
    let heroesThatWereActive = heroCache.getActiveHeroes();
    let nonActiveHeroes = heroService.getNonActiveHeroes(heroesThatWereActive, now);
    for (var i = 0; i < nonActiveHeroes.length; i++) {
        let hero = nonActiveHeroes[i];
        if (hero.blockType === 'one-time') {
            console.log('active hero to resting: ' + hero.name);
            activeHeroesTurnedResting.push(hero);
        } else if (hero.blockType === 'ongoing') {
            console.log('active hero to scheduled: ' + hero.name);
            activeHeroesTurnedScheduled.push(hero);
        }
    }
    let heroesThatWereScheduled = heroCache.getScheduledHeroes();
    scheduledHeroesTurnedActive.push(...heroService.getActiveHeroes(heroesThatWereScheduled, now));
    for (var i = 0; i < scheduledHeroesTurnedActive.length; i++) {
        let hero = scheduledHeroesTurnedActive[i];
        console.log('scheduled hero to active: ' + hero.name);
    }
    let heroesThatWereResting = heroCache.getRestingHeroes();
    for (var i = 0; i < heroesThatWereResting.length; i++) {
        let hero = heroesThatWereResting[i];
        let heroIsActive = heroService.isHeroActive(hero, now);
        if (heroIsActive) {
            console.log('resting hero to active: ' + hero.name);
            restingHeroesTurnedActive.push(hero);
        }
    }
}

function handleActiveToScheduled(activeHeroesTurnedScheduled, now) {
    for (var i = 0; i < activeHeroesTurnedScheduled.length; i++) {
        let hero = activeHeroesTurnedScheduled[i];
        heroCache.removeActiveHero(hero);
        heroCache.addScheduledHero(hero, now);
    }
}

function handleActiveToResting(activeHeroesTurnedResting) {
    for (var i = 0; i < activeHeroesTurnedResting.length; i++) {
        let hero = activeHeroesTurnedResting[i];
        heroCache.removeActiveHero(hero);
        heroCache.addRestingHero(hero);
    }
}

function handleScheduledToActive(scheduledHeroesTurnedActive, now) {
    for (var i = 0; i < scheduledHeroesTurnedActive.length; i++) {
        let hero = scheduledHeroesTurnedActive[i];
        heroCache.removeScheduledHero(hero);
        heroCache.addActiveHero(hero, now);
    }
}

function handleRestingToActive(restingHeroesTurnedActive, now) {
    for (var i = 0; i < restingHeroesTurnedActive.length; i++) {
        let hero = restingHeroesTurnedActive[i];
        heroCache.removeRestingHero(hero);
        heroCache.addActiveHero(hero, now);
    }
}

exports.start = start;