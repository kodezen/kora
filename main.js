const electron = require('electron');
const { app, BrowserWindow, Tray, nativeImage } = electron;
const path = require('path');
var electronWindow = require('./electron-win.js');
var serverModule = require('./server/server.js');
var ipc = require('./ipc/main/main.js');
var database = require('./database/database.js');
var timeSavedCache = require('./database/time-saved-cache.js');
var heroCache = require('./database/hero-cache.js');
var heroService = require('./services/hero-service.js');
var timeService = require('./services/time-service.js');
var timeSavedService = require('./services/time-saved-service.js');
var heroUpdaterService = require('./services/hero-updater-service.js');
var uninstallService = require('./services/uninstall-service.js');
var extensionBypassService = require('./services/extension-bypass-service.js');
var browserBypassService = require('./services/browser-bypass-service.js');
var autoUpdater = require('./services/auto-updater.js');
var userService = require('./services/user-service.js');
var actionService = require('./services/action-service.js');
var eventService = require('./services/event-service.js');
var trialChecker = require('./services/trial-checker.js');
var trialService = require('./services/trial-service.js');
var premiumService = require('./services/premium-service.js');
const log = require('electron-log');
const moment = require('moment');
const momentDurationFormatSetup = require('moment-duration-format'); // do not remove (import does all the work)
const url = require('url');
const to = require('await-to-js').default;

var simulator = require('./services/simulator');
let win = null;
//require('electron-reload')(__dirname, { electron: require(`${__dirname}/node_modules/electron`) });

let productionMode = false;

process.on('uncaughtException', (err, origin) => {
  log.error(err);
  if (!productionMode) {
    console.log(err);
  }
});

const obtainedLock = app.requestSingleInstanceLock();
if (!obtainedLock) {
  app.quit();
} else {
  app.on('second-instance', (event, commandLineArguments, workingDirectory) => {
    if (win) {
      win.show();
    }
  });

  startup().then((startupBundle) => {
    ipc.setup.sendSetupComplete(startupBundle);
  });

  app.on('ready', function () {
    electronReady = true;
    createWindow();
    autoUpdater.initialize();
    log.info('Kora startup successful');
  });

  app.on('web-contents-created', (event, contents) => {
    contents.on('did-finish-load', () => {
      contents.setZoomFactor(1);
      contents.setVisualZoomLevelLimits(1, 1);
      contents.setLayoutZoomLevelLimits(0, 0);
    });
    contents.on('new-window', (event, navigationUrl) => {
      event.preventDefault();
    }); 
    contents.on('will-navigate', (event, navigationUrl) => {
      event.preventDefault();
    });
    contents.on('will-attach-webview', (event, webPreferences, params) => {
      event.preventDefault();
    });
  });
}

async function startup() {
  uninstallService.initialize();
  await timeService.syncTime();
  database.connect();
  // TODO: only uncomment after fixing extension bypass user experience
  // extensionBypassService.start();
  await userService.initialize();
  await eventService.initialize(productionMode, userService.getUser(), userService.isNewUser());

  let err, timeSaved;
  [err, timeSaved] = await to(timeSavedService.getTimeSavedFromDb());
  if (!err) {
    timeSavedCache.populateCache(timeSaved);
  } else {
    err = null;
  }

  let heroes;
  [err, heroes] = await to(heroService.getHeroesFromDb());
  if (!err) {
    var now = moment();
    heroCache.populateCache(heroes, now);
    uninstallService.runActiveHeroCheck();
    browserBypassService.start();
    await timeSavedService.timeSavedCatchup(now, heroes); 
    heroUpdaterService.start(now);
  } else {
    // if err retrieving blocks from database, automatically allow uninstall
    uninstallService.allowUninstall();
    err = null;
  }
  
  await actionService.initialize();

  let isPaidUser = userService.isPaidUser();
  if (isPaidUser) {
    premiumService.startCodeChecker();
  } else {
    trialChecker.start();
  }

  launchServer();

  let startupBundle = genStartupBundle();
  return startupBundle;
}

function genStartupBundle() {
  let startupBundle = {
    newUser: userService.isNewUser(),
    firstBlockCreated: actionService.queryAction(actionService.FIRST_BLOCK_CREATED),
    chromeSetupCompleted: userService.isChromeSetupCompleted(),
    freeTrialExpired: (userService.isFreeTrialUser() && !trialService.isTrialActive())
  };
  return startupBundle;
}

function createWindow() {
  const {width, height} = electron.screen.getPrimaryDisplay().workAreaSize;
  win = new BrowserWindow({ 
    width: width, 
    height: height, 
    icon: path.join(__dirname, 'build/icon.ico'),
    webPreferences: {
      nodeIntegration: false,
      contextIsolation: true,
      enableRemoteModule: false,
      sandbox: true,
      disableBlinkFeatures: "Auxclick",
      preload: path.join(app.getAppPath(), '/preload/ipc.js')
    }
  });
  win.setMenu(null);
  win.maximize();

  const trayIconPath = path.join(__dirname, 'assets/tray-small.png');
  const tray = new Tray(nativeImage.createFromPath(trayIconPath));
  tray.on('click', () => {
        win.show();
  });
  win.Tray = tray;
  let session = win.webContents.session;
  setPermissionRequestHandler(session);

  if (productionMode) {
    // win.loadURL(`file://${__dirname}/distAngular/index.html`);
    win.loadURL(
      url.format({
        pathname: path.join(__dirname, `/distAngular/index.html`),
        protocol: "file:",
        slashes: true
      })
    );
  } else {
    win.loadURL('http://localhost:4200');
    win.webContents.openDevTools();
    // setDevCSP(session); // commented because requires ng serve --prod (slow displaying of changes)
  }

  win.on('unmaximize', function (event) {
    win.maximize();
  });

 // Window to system tray on close
  win.on('close', function (event) {
    event.preventDefault();
    win.hide();
  });

  win.on('closed', function(event) {
    electronWindow.setWindow(null);
  });

  electronWindow.setWindow(win);
}

function setPermissionRequestHandler(session) {
  session.setPermissionRequestHandler((webContents, permission, callback) => {
    console.log('we got a permission request: ' + permission);
    return callback(false);
  });
}

function setDevCSP(session) {
  session.webRequest.onHeadersReceived((details, callback) => {
    callback({
      responseHeaders: Object.assign({
        "Content-Security-Policy": ["default-src 'self'; style-src 'self' 'unsafe-inline'"]
      }, details.responseHeaders)
    })
  });
}

function launchServer() {
  // Start HTTP server
  var server = serverModule.server;
  var port = serverModule.port;
  server.listen(port, () => log.info(`Socket listening on port ${port}`));
}

exports.win = win;
