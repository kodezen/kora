Function .onInstSuccess
  	SetOutPath "$INSTDIR"
    Exec "$INSTDIR\schedTaskFiles\createScheduledTask-dev-win.exe"
FunctionEnd

!macro customUnInit
  !system "echo '' > ${BUILD_RESOURCES_DIR}/customUnInit"
  SetShellVarContext current
  IfFileExists "$APPDATA\kora-dev\update-in-progress" update_file_found update_file_not_found
    
    update_file_found:
        SetShellVarContext all
        Exec 'schtasks /delete /tn "KoraMonitorDev" /F'
        goto allow_uninstall_for_update
    
    update_file_not_found:
        IfFileExists "$APPDATA\kora-dev\Versioning-binary" file_found
            
            file_found:
                FileOpen $0 "$APPDATA\kora-dev\Versioning-binary" r
                FileRead $0 $1
                StrCmp $1 "173f5126570d361" allow_uninstall do_not_allow_uninstall

                    allow_uninstall:
                        SetShellVarContext all
                        Exec 'schtasks /delete /tn "KoraMonitorDev" /F'
                        goto end
                
                    do_not_allow_uninstall:
                        MessageBox MB_OK "A block is active. Please wait until it ends to uninstall. Contact korablocker@gmail.com if you need technical support."
                        Quit
                end:
    allow_uninstall_for_update:

!macroend

